/*
 * audio.h
 *
 *  Created on: 11 Feb 2013
 *      Author: st
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include <stdint.h>

typedef struct {
	uint32_t sampleRate;
	uint32_t nFrames;
	uint32_t actualSampleRate;
	uint32_t actualNFrames;
} AudioSettings;

// buffer will never be smaller than this number of samples per channel
// buffer will always be a multiple of this
#define MIN_FRAMES				   	0x0010
// buffer will never be larger than this number of samples per channel
#define MAX_FRAMES                 	0x0100


/*
 * This is the function you need to call in an application
 * that wants to use audio.
 */
void Audio_Init(AudioSettings *audioSettings);

// then after you have everything set up ready for Audio_Process calls...
void Audio_Start(uint32_t nframes);

/*
 * These are the functions you need to implement in an application
 * that wants to use audio. It is a VST-like interface with the same
 * semantics.
 */
void Audio_Process(float **in, float **out, uint32_t nFrames);
void Audio_SetParameter(int32_t index, float value);

/*
 * This function may be called to print data to stdout concerning
 * the number of audio input and output completions and errors.
 */
void Audio_PrintData(void);

#endif /* AUDIO_H_ */
