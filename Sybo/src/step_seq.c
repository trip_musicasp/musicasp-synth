/*
 * step_seq.c
 *
 * Handle Sequencer Clock ticks and Audio DMA interrupts.
 * It is important that these share the same interrupt preemption priority
 * such that neither can interrupt the other! Given that this is true of
 * the codebase as supplied, the two functions SeqClock_HandleTick and
 * StepSeq_Process can simply communicate using the same data structure with
 * no concurrency problems.
 *
 *  Created on: 22 Mar 2013
 *      Author: st
 */

#include "step_seq.h"
#include <seq_clock.h>
#include <midi.h>

#define SIXTEENTHS 	(SEQ_CLOCK_PPQN >> 2)
#define MAX_CONSUME	10
#define MAX_LANES	16

static volatile int playing = 0;
static int teenth = 0;
static int bar = 0;
static int shuffle = 0;				// 0 .. 6
static int shuffle48ths = 0;
static int nLanes = 0;

static StepSeqLane *lanes[MAX_LANES], *recordLane = 0;

/*
 * Called by the Sequencer Clock. The number returned controls how many
 * ticks at 48 PPQN will pass before we are called again.
 * Returning 1 (or less) causes us to be called at 48 PPQN.
 * Returning 48 causes us to be called on quarter notes.
 * Returning 12 causes us to be called on sixteenth notes.
 */
int SeqClock_HandleTick(void) {
	if (++teenth > 15) {
		teenth = 0;
		bar++;
	}
	if ((teenth & 3) == 0) {
		DebugLed_set(0, 1);			// on downbeat turn led on
		shuffle48ths = shuffle;
	} else if ((teenth & 3) == 2) {
		DebugLed_set(0, 0);			// on (shuffled) upbeat turn led off
		shuffle48ths = -shuffle;
	}
	if (playing) {
		StepSeqLane *l;
		int i;
		for (i = 0; i < nLanes; i++) {
			l = lanes[i];
			l->used = l->prepare(bar, teenth, l);
		}
	}
	return SIXTEENTHS + shuffle48ths;
}

/**
 * Called synchronously with Audio_Process so that it may safely create
 * voices etc.
 */
int StepSeq_Process(void) {
	if (playing) {
		StepSeqLane *l;
		int i, b;
		for (i = 0; i < nLanes; i++) {
			l = lanes[i];
			if (!l->used) continue;		// typical short circuit
			b = l->burst;
			while (l->used && b--) {
				Midi_HandleMessage(l->messages[--l->used]);
			}
		}
	}
	// consume after playback so we don't accidently echo messages
	int consumed = 0;
	while (Midi_Available() && consumed < MAX_CONSUME) {
		MidiMessage msg = Midi_Consume();
		if (playing && recordLane) {
			recordLane->record(bar, teenth, recordLane, msg);
		}
		Midi_HandleMessage(msg);
		consumed += 1;
	}
	return consumed;
}

void StepSeq_Play(void) {
	if (playing) return;
	teenth = -1;
	bar = 0;
	playing = 1;
}

void StepSeq_Stop(void) {
	if (!playing) return;
	playing = 0;
	Midi_HandleAllNotesOff();
}

void StepSeq_Record(int state) {

}

int StepSeq_isPlaying(void) {
	return playing;
}

void StepSeq_setShuffle(uint8_t amount) {
	if (amount < 1) amount = 1;
	if (amount > 7) amount = 7;
	shuffle = --amount;
}

void StepSeq_addLane(StepSeqLane *l) {
	lanes[nLanes++] = l;
}

