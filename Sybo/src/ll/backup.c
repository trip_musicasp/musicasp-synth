/*
 * backup.c
 *
 *  Created on: 10 Mar 2013
 *      Author: st
 */

/*
Access to the Backup Domain (SRAM and RTC)

Enable the power interface clock by setting the PWREN bits in the RCC_APB1ENR
register

Set the DBP bit in the PWR power control register (PWR_CR)

Enable the backup SRAM clock by setting BKPSRAMEN bit in the RCC AHB1
peripheral clock enable register (RCC_AHB1ENR).

When the backup domain is supplied by VBAT (analog switch connected to VBAT
because VDD is not present), the backup SRAM is powered by a dedicated low
power regulator. This regulator can be ON or OFF depending whether the
application needs the backup SRAM function in Standby and VBAT modes or not.
The power-down of this regulator is controlled by a dedicated bit, the BRE
control bit of the PWR_CSR register.

   (+) Select the RTC clock source using the RCC_RTCCLKConfig() function.
   (+) Enable RTC Clock using the RCC_RTCCLKCmd() function.
   (+) Configure the RTC Prescaler (Asynchronous and Synchronous) and RTC hour
       format using the RTC_Init() function.

 */

#include <stm32f4xx_pwr.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_rtc.h>

/**
 * Initialise access to the battery backed up SRAM and RTC
 */
void Backup_Init(void) {
	// RCC->APB1ENR |= 0x10000000;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	// CR_DBP_BB = 1;
	PWR_BackupAccessCmd(ENABLE);

	// RCC->AHB1ENR |= 0x00040000;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);
	// CSR_BRE_BB = 1;
	PWR_BackupRegulatorCmd(ENABLE);

	RCC_LSEConfig(RCC_LSE_ON);
	// Wait till LSE is ready
	while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);
	// RCC->BDCR |= (0x00000100 & 0x00000FFF)
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	// BDCR_RTCEN_BB = 1;
	RCC_RTCCLKCmd(ENABLE);
	// Wait for RTC APB registers synchronisation
	RTC_WaitForSynchro();

	RTC_InitTypeDef RTC_InitStruct;
	RTC_StructInit(&RTC_InitStruct); // 24h, prescaler divisors for LSE clock
	RTC_Init(&RTC_InitStruct);
}


