/*
 * backup.h
 *
 * This header declares the initialisation function required to use
 * battery backed SRAM and the macro used to mark data items to be
 * placed in the battery backed up SRAM rather than the normal data
 * section.
 *
 *  Created on: 10 Mar 2013
 *      Author: st
 */

#ifndef BACKUP_H_
#define BACKUP_H_

#define NON_VOLATILE __attribute__ ((section (".bkpram")))

void Backup_Init(void);

#endif /* BACKUP_H_ */
