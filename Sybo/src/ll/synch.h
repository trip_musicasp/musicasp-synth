/*
 * synch.h
 *
 *  Created on: 13 Feb 2013
 *      Author: st
 */

#ifndef SYNCH_H_
#define SYNCH_H_

/*
 * define our own data memory barrier since the one in cmsis has illegal syntax
 * for current arm-none-eabi-gcc
 */
#define DMB() __asm__ __volatile__ ("dmb")

#define LOCKED 1
#define UNLOCKED 0

typedef unsigned SYNC_OBJECT, *SYNC_OBJECT_PTR;

extern void lock_mutex(void *mutex);
extern void unlock_mutex(void *mutex);

#endif /* SYNCH_H_ */
