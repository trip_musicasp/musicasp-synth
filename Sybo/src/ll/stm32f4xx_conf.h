/*
 * This file is included by CMSIS stm32f4xx.h if USE_STDPERIPH_DRIVER is
 * defined. If USE_FULL_ASSERT is defined parameter checking is enabled
 * in the standard peripheral libraries. Both of these are defined in
 * the makefile for Sybo.
 */
#ifndef __STM32F4xx_CONF_H
#define __STM32F4xx_CONF_H

#include "assertp.h"

#endif /* __STM32F4xx_CONF_H */
