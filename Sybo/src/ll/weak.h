/*
 * weak.h
 *
 *  Created on: 28 Apr 2013
 *      Author: st
 */

#ifndef WEAK_H_
#define WEAK_H_

#define WEAK __attribute__((weak))

#endif /* WEAK_H_ */
