/*
 * assert.h
 *
 *  Created on: 25 Feb 2013
 *      Author: st
 */

#ifndef ASSERT_H_
#define ASSERT_H_

#include <stdint.h>

#if  USE_FULL_ASSERT
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
  void assert_failed(uint8_t* file, uint32_t line);
#else
  #define assert_param(expr) ((void)0)
#endif /* USE_FULL_ASSERT */

#endif /* ASSERT_H_ */
