/*
 * exhndlr.c
 *
 * Cortex-M4 Processor Exceptions Handlers
 *
 *  Created on: 15 Feb 2013
 *      Author: st
 */

#include <stdio.h>
#include <stm32f4xx_rcc.h>
#include <debug_led.h>
#include "backup.h"

/* The prototype shows it is a naked function - in effect this is just an
assembly function. */
void HardFault_Handler(void) __attribute__( ( naked ) );
void MemManage_Handler(void) __attribute__( ( naked ) );
void BusFault_Handler(void) __attribute__( ( naked ) );
void UsageFault_Handler(void) __attribute__( ( naked ) );

struct {
	uint32_t fault;
	uint32_t r0, r1, r2, r3;
	uint32_t r12, lr, pc, psr;
	uint32_t stackPC;
	uint32_t SCB_CFSR;
	uint32_t SCB_HFSR;
	uint32_t SCB_DFSR;
	uint32_t SCB_MMFAR;
	uint32_t SCB_BFAR;
	uint32_t SCB_AFSR;
} FaultBackup NON_VOLATILE;

void Fault_printData(void) {
	if (!FaultBackup.fault) return;
	printf("MCU: r0=%08X, r1=%08X, r2=%08X, r3=%08X\n",
			(unsigned)FaultBackup.r0, (unsigned)FaultBackup.r1,
			(unsigned)FaultBackup.r2, (unsigned)FaultBackup.r3);
	printf("MCU: r12=%08X, lr=%08X, pc=%08X, psr=%08X\n",
			(unsigned)FaultBackup.r12, (unsigned)FaultBackup.lr,
			(unsigned)FaultBackup.pc, (unsigned)FaultBackup.psr);
	printf("SCB: CFSR=%08X, HFSR=%08X, DFSR=%08X, AFSR=%08X\n",
			(unsigned)FaultBackup.SCB_CFSR, (unsigned)FaultBackup.SCB_HFSR,
			(unsigned)FaultBackup.SCB_DFSR, (unsigned)FaultBackup.SCB_AFSR);
	uint32_t cfsr = FaultBackup.SCB_CFSR;
// http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0553a/Cihdjcfc.html
	if (cfsr & 0x80) {
		printf("SCB: MMFAR=%08X\n",	(unsigned)FaultBackup.SCB_MMFAR);
	}
	if (cfsr & 0x8000) {
		printf("SCB: BFAR=%08X\n", (unsigned)FaultBackup.SCB_BFAR);
	}
	FaultBackup.fault = 0;
}

void faultHandler(uint32_t* stackFrame) {
	FaultBackup.SCB_MMFAR = SCB->MMFAR;
	FaultBackup.SCB_BFAR = SCB->BFAR;
	FaultBackup.SCB_CFSR = SCB->CFSR;
	FaultBackup.SCB_HFSR = SCB->HFSR;
	FaultBackup.SCB_DFSR = SCB->DFSR;
	FaultBackup.SCB_AFSR = SCB->AFSR;

	FaultBackup.r0 =  stackFrame[0];
	FaultBackup.r1 =  stackFrame[1];
	FaultBackup.r2 =  stackFrame[2];
	FaultBackup.r3 =  stackFrame[3];
	FaultBackup.r12 = stackFrame[4];
	FaultBackup.lr =  stackFrame[5];
	FaultBackup.pc =  stackFrame[6];
	FaultBackup.psr = stackFrame[7];

	FaultBackup.fault = 1;

	DebugLed_set(0, 1);

	Fault_printData();

	while (1);
}

#define ASM_HANDLER() \
	__asm volatile ( \
    " tst lr, #4                                                \n" \
    " ite eq                                                    \n" \
    " mrseq r0, msp                                             \n" \
    " mrsne r0, psp                                             \n" \
    " ldr r1, [r0, #24]                                         \n" \
    " ldr r2, 1f                            					\n" \
    " bx r2                                                     \n" \
    " 1: .word faultHandler    									\n" \
);

/**
  * @brief   This function handles NMI exception.
  */
void NMI_Handler(void) {
}

/**
  * @brief  This function handles Hard Fault exception.
  */
void HardFault_Handler(void) {
	ASM_HANDLER();
}

/**
  * @brief  This function handles Memory Manage exception.
  */
void MemManage_Handler(void) {
	ASM_HANDLER();
}

/* The fault handler implementation calls a function called
prvGetRegistersFromStack(). */
void BusFault_Handler(void) {
	ASM_HANDLER();
}

/**
  * @brief  This function handles Usage Fault exception.
  */
void UsageFault_Handler(void) {
	ASM_HANDLER();
}

/**
  * @brief  This function handles SVCall exception.
  */
void SVC_Handler(void) {
}

/**
  * @brief  This function handles Debug Monitor exception.
  */
void DebugMon_Handler(void) {
}

/**
  * @brief  This function handles PendSVC exception.
  */
void PendSV_Handler(void) {
}

