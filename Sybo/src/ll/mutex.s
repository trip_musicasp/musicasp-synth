    .syntax unified
    .arch armv7-m

	.equ locked, 1
	.equ unlocked, 0

	.text

    .thumb
    .thumb_func
	.align 2
# lock_mutex
# Declare for use from C as extern void lock_mutex(void * mutex)
    .global lock_mutex
    .func lock_mutex
lock_mutex:
    ldr r1, =locked
.1: ldrex r2, [r0]
    cmp r2, r1 				@ Test if mutex is locked or unlocked
    beq .2 					@ If locked - wait for it to be released, from 2
    itt ne
    strexne r2, r1, [r0] 	@ Not locked, attempt to lock it
    cmpne r2, #1			@ Check if Store-Exclusive failed
    beq .1 					@ Failed - retry from 1

    @ Lock acquired
    dmb						@ Required before accessing protected resource
    bx lr

.2:   @ Take appropriate action while waiting for mutex to become unlocked
    b .1 					@ Retry from 1
    .endfunc

    .thumb
    .thumb_func
	.align 2
# unlock_mutex
# Declare for use from C as extern void unlock_mutex(void * mutex);
    .global unlock_mutex
    .func unlock_mutex
unlock_mutex:
    ldr r1, =unlocked
    dmb 					@ Required before releasing protected resource
    str r1, [r0] 			@ Unlock mutex
    bx lr
    .endfunc

    .end
 
