/*
 * param.c
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#include "param.h"
#include <weak.h>

static void linInit(ControlLaw *law);
static float linRawToUser(ControlLaw *law, float rawVal);
static float linUserToRaw(ControlLaw *law, float userVal);

static void logInit(ControlLaw *law);
static float logRawToUser(ControlLaw *law, float rawVal);
static float logUserToRaw(ControlLaw *law, float userVal);

static float defaultUserToUser(ControlLaw *law, float userVal);
static void discreteInit(ControlLaw *law);
static float discreteUserToUser(ControlLaw *law, float userVal);

ParamConverter Param_LinConverter =
	{ linInit, linRawToUser, linUserToRaw, defaultUserToUser };
ParamConverter Param_LogConverter =
	{ logInit, logRawToUser, logUserToRaw, defaultUserToUser };
ParamConverter Param_DiscreteConverter =
	{ discreteInit, linRawToUser, linUserToRaw, discreteUserToUser };

static ControlLaw unityLaw = { 0.f, 1.f, "", &Param_LinConverter };

static Parameter *paramArray;
static uint32_t nparams;

/*
 * Return a Parameter's dsp value by converting the user value.
 */
static float userToDsp(ControlLaw *law, float userVal) {
	assert_param(law);
	if (law->userToDsp) {
		return law->userToDsp(userVal);
	}
	return userVal;
}

/*
 * Update a Parameter by range enforcing the raw value, converting it to
 * the user value, then converting the user value to the dsp value.
 * This is typically called by the application Audio_SetParameter() function.
 */
void Param_Update(Parameter *p, float rawVal) {
	assert_param(p && p->law);
	ControlLaw *law = p->law;
	// store range limited raw value;
	if (rawVal < 0) rawVal = 0.f;
	if (rawVal > 1) rawVal = 1.f;
	p->rawVal = rawVal;
	// convert raw to user, using lin, log or whatever ControlLaw
	p->userVal = law->convert->userToUser(law,
				 law->convert->rawToUser(law, rawVal));
	// convert user to dsp
	p->dspVal = userToDsp(law, p->userVal);
}

/*
 * Update a Parameter by range enforcing the user value, converting it to
 * the raw value, then converting the user value to the dsp value.
 */
static void updateFromUser(Parameter *p, float userVal) {
	assert_param(p && p->law);
	ControlLaw *law = p->law;
	userVal = law->convert->userToUser(law, userVal);
	// store range limited user value
	if (userVal < law->userMin) userVal = law->userMin;
	if (userVal > law->userMax) userVal = law->userMax;
    p->userVal = userVal;
	// convert user to raw, using lin, log or whatever ControlLaw
	p->rawVal = law->convert->userToRaw(law, userVal);
	// convert user to dsp
	p->dspVal = userToDsp(law, p->userVal);
}

/*
 * Initialise a Parameter by setting its abbreviation and its ControlLaw (if
 * supplied, otherwise we use a default unityLaw). We also initialise the
 * ControlLaw, and finally call updateFromUser() to update the raw and dsp
 * values from the user value.
 */
void Param_Init(Parameter *p, char *abbrev, float userVal, ControlLaw *law) {
	assert_param(p && abbrev);
	p->abbrev = abbrev;
	if (!law) law = &unityLaw;
    p->law = law;
	law->convert->init(law);
	updateFromUser(p, userVal);
}

/*
 * Link the application Parameters so that we can search them later
 */
void Param_Link(Parameter *p, uint32_t np) {
	paramArray = p;
	nparams = np;
}

Parameter *Param_fromAbbreviation(char *s) {
	int i;
	for (i = 0; i < nparams; i++) {
		if (strcmp(s, paramArray[i].abbrev) == 0) {
			return &paramArray[i];
		}
	}
	return 0;
}

void WEAK Param_Name(Parameter *p, char *buf, unsigned len) {}

static void printParam(Parameter *p) {
	int precision = p->law->units &&
			strlen(p->law->units) < 1 && fabsf(p->userVal) < 10.f ? 4 : 2;
	char name[80];
	const char *annotation = p->law->units;
	if (annotation == 0) {
		precision = 0;
		if (p->law->annotation) {
			annotation = p->law->annotation((unsigned)p->userVal);
		} else if (p->userVal < 100.f){
			annotation = "\t";
		} else {
			annotation = "";
		}
	}
	Param_Name(p, name, 80);
	printf("%s %.*f %s\t(%s)\n", p->abbrev, precision, p->userVal, annotation, name);
}

static void printAnnotationList(Parameter *p) {
	int i;
	for (i = 0; i <= p->law->userMax; i++) {
		printf("%d\t\%s\n", i, p->law->annotation(i));
	}
}

/*
 * Parse parameter abbreviation commands
 */
void Param_ParseUserVal(char *s) {
	Parameter *p;
	char *e = strchr(s, '='); 	// non-null if mutator
	char *q = strchr(s, '?'); 	// non-null if annotation list requested
	if (e) {					// <paramAbbreviation>=<value>
		*e++ = '\0';
		p = Param_fromAbbreviation(s);
		if (p) {
			float v;
			sscanf(e, "%f", &v);
			updateFromUser(p, v);
			printParam(p);
			return;
		}
	} else if (q) {				// <paramAbbreviation>?
		*q = '\0';
		p = Param_fromAbbreviation(s);
		if (p && p->law->annotation) {
			printAnnotationList(p);
		}
		return;
	} else {					// <paramAbbreviation>|<partialParamAbbreviation>
		p = Param_fromAbbreviation(s);	// full abbreviation, print value
		if (p) {
			printParam(p);
		} else {				// partial abbreviaton, list all matching
			int i;
			char *c;
			for (i = 0; i < nparams; i++) {
				c = paramArray[i].abbrev;
				if (c == strstr(c, s)) {
					printParam(&paramArray[i]);
				}
			}
		}
		return;
	}
	printf("Parameter '%s' not found\n", s);
}

void Param_PrintAll(void) {
	int i;
	for (i = 0; i < nparams; i++) {
		printParam(&paramArray[i]);
	}
}

static void paramWrite(FIL *fp, Parameter *p) {
	char value[20];
	sprintf(value, "%.5f", p->userVal);
	f_printf(fp, "%s=%s\n", p->abbrev, value);
}

FRESULT Param_Save(char *s, char *path) {
	FIL file;
	// open file
	FRESULT res = f_open(&file, path, FA_CREATE_ALWAYS | FA_WRITE);
	if (res) return res;
	Parameter *p = Param_fromAbbreviation(s);
	int n = 0;
	if (p) {						// full abbreviation, save value
		paramWrite(&file, p);
		n++;
	} else {				// partial abbreviation, save all matching
		int i;
		char *c;
		for (i = 0; i < nparams; i++) {
			c = paramArray[i].abbrev;
			if (c == strstr(c, s)) {
				paramWrite(&file, &paramArray[i]);
				n++;
			}
		}
	}
	printf("%d parameters saved to '%s'\n", n, path);
	return f_close(&file);
}

FRESULT Param_Load(char *path) {
	char line[256];
	FIL file;
	// open file
	FRESULT res = f_open(&file, path, FA_OPEN_EXISTING | FA_READ);
	if (res) return res;
	// parse each line to set parameter
	while (f_gets(line, 256, &file) == line) {
		char *last = &line[strlen(line)-1];
		if (*last == '\n') {
			*last = '\0';	// remove trailing \n
		}
		Param_ParseUserVal(line);
		line[0] = '\0';
	}
	// close file
	return f_close(&file);
}


static float defaultUserToUser(ControlLaw *law, float userVal) {
	return userVal;
}

// --- linear law handling -------------------------------------------------

static void linInit(ControlLaw *law) {
	assert_param(law);
}

static float linRawToUser(ControlLaw *law, float rawVal) {
	assert_param(law);
	return law->userMin + rawVal*(law->userMax - law->userMin);
}

static float linUserToRaw(ControlLaw *law, float userVal) {
	assert_param(law);
	return (userVal - law->userMin) / (law->userMax - law->userMin);
}

// ---- log law handling ---------------------------------------------------

static void logInit(ControlLaw *law) {
	assert_param(law && law->userMin && law->userMax);
	// precalculate some values for Param_ConvertLog
    law->logMin = log10(law->userMin); // error on zero min
    float logMax = log10(law->userMax); // error zero max
    law->logSpan = logMax - law->logMin;
}

static float logRawToUser(ControlLaw *law, float rawVal) {
	assert_param(law);
    float power = law->logMin + rawVal * law->logSpan;
    return (float)pow(10, power);
}

static float logUserToRaw(ControlLaw *law, float userVal) {
	assert_param(law);
	return (log10(userVal)-law->logMin) / law->logSpan;
}

// ---- discrete law handling ----------------------------------------------

static void discreteInit(ControlLaw *law) {
	assert_param(law);
	law->userMin = discreteUserToUser(law, law->userMin);
	law->userMax = discreteUserToUser(law, law->userMax);
}

static float discreteUserToUser(ControlLaw *law, float userVal) {
	return floorf(userVal);
}

