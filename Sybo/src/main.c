#include "app.h"

/***************************************************************************/
/*  Generic application                                                            */
/***************************************************************************/
int main (void)
{
	/*
	 * Initialise your application
	 */
	App_Init();

	/*
	 * Run your application background code indefinitely
	 */
	while ( 1 ) {
		App_Background();
	}

	/*
	 * This return will never be reached.
	 * But to prevent the compiler warning:
	 * "return type of 'main' is not 'int'
	 * we use an int as return :-)
	 */
	return(0);
}

/*** EOF ***/
