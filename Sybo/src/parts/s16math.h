/*
 * s16math.h
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#ifndef S16MATH_H_
#define S16MATH_H_

static inline int16_t s16mul(int16_t a, int16_t b) {
	return (int16_t)(((int32_t)a * (int32_t)b) >> 15);
}

#endif /* S16MATH_H_ */
