/*
 * convert.c
 *
 *  Created on: 30 Mar 2013
 *      Author: st
 */

#include "convert.h"
#include <stdio.h>

void testConverts(void) {
	float a = 0.5f;
	int32_t w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
	a = S16toF32((int16_t)w);
	printf("%X -> %f\n", (int)(int16_t)w, a);
	a = 1.0f;
	w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
	a = S16toF32((int16_t)w);
	printf("%X -> %f\n", (int)(int16_t)w, a);
	a = 1.1f;
	w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
	a = -0.5f;
	w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
	a = S16toF32((int16_t)w);
	printf("%X -> %f\n", (int)(int16_t)w, a);
	a = -1.0f;
	w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
	a = S16toF32((int16_t)w);
	printf("%X -> %f\n", (int)(int16_t)w, a);
	a = -1.1f;
	w = F32toS16(a);
	printf("%f -> %X\n", a, (int)w);
}
