/*
 * decimate.h
 *
 *  Created on: 8 Apr 2013
 *      Author: st
 */

#ifndef RESAMPLE_H
#define RESAMPLE_H_

#include <stdint.h>

typedef struct {
	  float y1, y2, y3, y4, y5; //, y6, y7, y8, y9;
} HQDecimator;

typedef struct {
	float y1, y2, y3, y4, y5;
} HQInterpolator;

uint32_t HQDecimator_Process(HQDecimator *d, float *output, float *input, uint32_t nframes);
uint32_t HQInterpolator_Process(HQInterpolator *d, float *output, float *input, uint32_t nframes);

#endif /* RESAMPLE_H_ */
