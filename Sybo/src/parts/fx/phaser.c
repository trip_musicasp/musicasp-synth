/*
 * phaser.c
 *
 *  Created on: 2 Apr 2013
 *      Author: st
 */

#include "phaser.h"
#include <fastmath.h>

static AllPassFilter ap[8];

static float dmin, drange;
static float sampleRate;

void Phaser_Init(uint32_t aSampleRate) {
	sampleRate = (float)aSampleRate;
	dmin = 440.f / (sampleRate / 2.f);  // !! actually min norm freq
	float dmax = 1600.f / (sampleRate / 2.f); // !! actually max norm freq
	drange = 0.5f * (dmax - dmin);
}

/**
 * A mono Phaser.
 */
void Phaser_Process(Phaser *ph, float *samples, uint32_t nframes) {
	float _lfoInc = 2.f * (float)M_PI * (ph->rate / sampleRate);
	float a1, d, y;
	float fb = ph->feedback * 0.95f; // safety limit
	float depth = ph->depth;
	float phase = ph->phase;
	float zm1 = ph->zm1;
	int n = ph->n;
	int i, a;
	for (i = 0; i < nframes; i++) {
		// calculate and update sweep lfo...
		d = dmin + drange * (fastsin(phase) + 1.f);
		phase += _lfoInc;
		if ( phase >= (float)M_PI )
			phase -= (float)M_PI * 2.f;

		// calculate allpass output
		a1 = (1.f - d) / (1.f + d);
		y = samples[i] + zm1 * fb;
		for (a = 0; a < n; a++ ) {
			y = allpass(&ap[a], y, a1);
		}
		zm1 = y;
		samples[i] += zm1 * depth;
	}
	ph->phase = phase;
	ph->zm1 = zm1;
}
