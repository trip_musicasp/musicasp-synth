/*
 * phaser.h
 *
 *  Created on: 2 Apr 2013
 *      Author: st
 */

#ifndef PHASER_H_
#define PHASER_H_

#include <stdint.h>
#include <math.h>
#include <fastmath.h>

typedef struct {
	uint32_t n;
	float rate;
	float depth;
	float feedback;
	float phase;
	float zm1;
} Phaser;

typedef struct {
	float zm1;
} AllPassFilter;

static inline float allpass(AllPassFilter *a, float in, float a1) {
	float y = in * -a1 + a->zm1;
	a->zm1 = y * a1 + in;
	return y;
}

void Phaser_Init(uint32_t aSampleRate);
void Phaser_Process(Phaser *ph, float *samples, uint32_t nframes);

#endif /* PHASER_H_ */
