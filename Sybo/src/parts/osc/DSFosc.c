/*
 * DSFosc.c
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#include "DSFosc.h"

static void update(DSFoscillator *o, float a);

static float *sine1, *sine2, *sine3, *cosine;

void DSFoscillatorW_Init(uint32_t nframes) {
	sine1 = (float *)malloc(nframes * sizeof(float));
	sine2 = (float *)malloc(nframes * sizeof(float));
	sine3 = (float *)malloc(nframes * sizeof(float));
	cosine = (float *)malloc(nframes * sizeof(float));
}

void DSFoscillatorW_init(DSFoscillator *o, float wn, int np, float a) {
	o->np = np;
	wn *= 2.f; // normalise to nyquist
	// ensure the highest partial is below nyquist
	if (wn * np >= 1.0f)
		np = (int) (1.0f / wn);
	Phasor_initSin(&o->sine1, (wn * (np + 1)));
	Phasor_initSin(&o->sine2,  wn * np);
	Phasor_initSin(&o->sine3,  wn);
	Phasor_initCos(&o->cosine, wn);
	update(o, a);
}

static void update(DSFoscillator *o, float a) {
	o->a = a;
	o->aN = powf(a, o->np);
}

static float out(float a, float aN, int i) {
	float denom = (1.f - 2.f * a * cosine[i] + a * a);
	if (denom == 0.)
		return 0.f; // ??
	return ((((a * sine2[i] - sine1[i])	* aN + sine3[i]) * a) / denom);
}

void DSFoscillatorW_process(DSFoscillator *o, float *samples, int nframes, float a) {
//	update(o, a);
	Phasor_process(&o->sine1, sine1, nframes);
	Phasor_process(&o->sine2, sine2, nframes);
	Phasor_process(&o->sine3, sine3, nframes);
	Phasor_process(&o->cosine, cosine, nframes);
	int i;
	float aN = o->aN;
	for (i = 0; i < nframes; i++) {
		*samples++ = out(a, aN, i) * 0.7f;	// try to normalise to polyblep saw
	}
}

