/*
 * polyBLEP.h
 *
 *  Created on: 24 Mar 2013
 *      Author: st
 */

#ifndef POLYBLEP_H_
#define POLYBLEP_H_

#include <stdint.h>

typedef struct {
	float t;
	float dt;
	float dtInv;
	float prevSample;
	uint8_t shape;
} PolyBLEPoscillator;

void PolyBLEPoscillator_init(PolyBLEPoscillator *o, float wn, uint8_t shape, int reset);
void PolyBLEPoscillator_process(PolyBLEPoscillator *o, float *samples, int nframes);

#endif /* POLYBLEP_H_ */
