/*
 * DSFosc.h
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef DSFOSC_H_
#define DSFOSC_H_

#include "phasor.h"
#include <audio.h>
#include <math.h>
#include <assertp.h>
#include <stdlib.h>

typedef struct {
	float a;
	float aN;
	Phasor sine1, sine2, sine3, cosine;
	Phasor sine4; // only used by DSFoscillatorSS (Stilson-Smith version)
	int np;
} DSFoscillator;

void DSFoscillatorW_Init(uint32_t nframes);

void DSFoscillatorW_init(DSFoscillator *o, float wn, int np, float a);
void DSFoscillatorW_process(DSFoscillator *o, float *samples, int nframes, float a);

#endif /* DSFOSC_H_ */
