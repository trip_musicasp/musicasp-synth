/*
 * polyblep.c
 *
 *  Created on: 24 Mar 2013
 *      Author: st
 *
 *  PolyBLEP works by adding the polynomial	band-limited step function to
 *  two samples: the samples before and after a discontinuity in the signal.
 *  To achieve this, the implementation delays its output by one sample.
 *  At each frame, the phase is inspected. If the phase passed a
 *  discontinuity in the waveform, the PolyBLEP function is evaluated and
 *	added (to rising wave) or subtracted (falling wave) for the previous sample
 *	and the current sample.
 *
 */

#include "polyBLEP.h"

#define NAIVE_SAW			(2.f * t - 1.f)

#define POLYBLEP_BEFORE(t) 	(t * t + t + t + 1.f)
#define POLYBLEP_AFTER(t)	(t + t - t * t - 1.f)

void PolyBLEPoscillator_init(PolyBLEPoscillator *o, float wn,
		uint8_t shape, int reset) {
	o->dt = wn;
	o->dtInv = 1.f / o->dt;	// a reciprocal to avoid real-time division
	// freerunning, only reset if illegal or requested
	if (o->t > 1.f || o->t < 0.f || reset) {
		o->t = 0.5f;			// inital saw phase, so it starts from zero
		o->prevSample = 0.f;	// the 1 sample delay memory initial value
		o->shape = shape;
		if (shape == 1) o->prevSample = -1.f;
	}
}

// a version that is logical and easy to understand :)
// but sounds crap :(
void PolyBLEPoscillator_processSquare(PolyBLEPoscillator *o,
		float *samples, int nframes, float pw) {
	float t = o->t;
	float dt = o->dt;
	float dtInv = o->dtInv;
	float prevSample = o->prevSample;	// last sample of previous call
	while (nframes--) {
		t += dt;
		if (t >= 1.f) {
			t -= 1.f;					// discontinuity 1 -> -1
			float qa = t * dtInv;		// proportion of sample period after
			float qb = qa - 1.0f;		// proportion of sample period before
			*samples++ = prevSample - POLYBLEP_BEFORE(qb);
			prevSample = -1.f 		- POLYBLEP_AFTER(qa);
		} else if (prevSample < 0 && t >= pw) { // discontinuity -1 -> 1
			float qa = (t - pw) * dtInv;// proportion of sample period after
			float qb = qa - 1.0f;		// proportion of sample period before
			*samples++ = prevSample + POLYBLEP_BEFORE(qb);
			prevSample = 1.f 		+ POLYBLEP_AFTER(qa);
		} else {
			*samples++ = prevSample;
			prevSample = prevSample < 0 ? -1.f : 1.f;
		}
	}
	o->t = t;
	o->prevSample = prevSample;			// store for next call
}

// a version that is logical and easy to understand :)
void PolyBLEPoscillator_processSaw1(PolyBLEPoscillator *o,
		float *samples, int nframes) {
	float t = o->t;
	float dt = o->dt;
	float dtInv = o->dtInv;
	float prevSample = o->prevSample;	// last sample of previous call
	while (nframes--) {
		t += dt;
		if (t >= 1.f) {
			t -= 1.f;					// discontinuity 1 -> -1
			float qa = t * dtInv;		// proportion of sample period after
			float qb = qa - 1.0f;		// proportion of sample period before
			*samples++ = prevSample - POLYBLEP_BEFORE(qb);
			prevSample = NAIVE_SAW	- POLYBLEP_AFTER(qa);
		} else {
			*samples++ = prevSample;
			prevSample = NAIVE_SAW;		// ready for next iteration
		}
	}
	o->t = t;
	o->prevSample = prevSample;			// store for next call
}

// a version with the condition outside the inner loop for efficiency I hope
void PolyBLEPoscillator_processSaw(PolyBLEPoscillator *o,
		float *samples, int nframes) {
	float t = o->t;
	float dt = o->dt;
	float dtInv = o->dtInv;
	float prevSample = o->prevSample;	// last sample of previous call
	do {
		int nf = (1.f - t) * dtInv;		// frames until discontinuity
		while (nf-- && nframes--) {		// until discontinuity or end of block
			t += dt;
			*samples++ = prevSample;
			prevSample = NAIVE_SAW;		// ready for next iteration
		}
		if (nframes > 0) {
			t += dt;
			t -= 1.f;					// discontinuity
			float qa = t * dtInv;		// proportion of sample period after
			float qb = qa - 1.0f;		// proportion of sample period before
			*samples++ = prevSample - POLYBLEP_BEFORE(qb);
			prevSample = NAIVE_SAW  - POLYBLEP_AFTER(qa);
			nframes--;
		}
	} while (nframes > 0);
	o->t = t;
	o->prevSample = prevSample;			// store for next call
}

void PolyBLEPoscillator_process(PolyBLEPoscillator *o,
		float *samples, int nframes) {
	switch (o->shape) {
	case 0: // saw
		PolyBLEPoscillator_processSaw(o, samples, nframes);
		break;
	case 1: // square
		PolyBLEPoscillator_processSquare(o, samples, nframes, 0.5f);
		break;
	}
}
