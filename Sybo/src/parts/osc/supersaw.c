/*
 * supersaw.c
 *
 * Thanks to Adam Szabo for the following information.
 *
 * The ratios of the 7 saws at maximum detune are:
 * - 0.11002313
 * - 0.06288439
 * - 0.01952356
 * + 0
 * + 0.01991221
 * + 0.06216538
 * + 0.10745242
 *
 * Detune is non-linear, approximately 3 linear segments
 * 0	0
 * 0.5	0.1
 * 0.75 0.25
 * 0.94 0.39
 * 1	1
 *
 * For the mix control
 * the centre saw amplitude y = -0.55366*x + 0.99785
 * the outer 6 saws amplitude y = -0.73764*x^2 + 1.2841*x + 0.044372
 *
 *  Created on: 5 Apr 2013
 *      Author: st
 */

#include "supersaw.h"
#include <stdlib.h>
#include <math.h>

#define HPQ 0.707f
#define KF2S (1 << 31)
#define KS2F (1.f / KF2S)

void SuperSaw_init(SuperSawOscillator *o, float wn) {
	o->dt[0] = wn * KF2S;
	int i;
	for (i = 0; i < 7; i++) {
		o->t[i] = (int32_t)rand();
	}
	o->prevDetune = -1.f;		// force initialisation
	o->prevMix = -1.f;
	o->low = o->high = o->band; // initialise filter state
	o->freq = 2.0f * sinf((float)M_PI * 1.25f * wn);
}

void SuperSaw_process(SuperSawOscillator *o, float *samples, int nframes,
		float detune, float mix) {
	if (detune != o->prevDetune) {
		float dt0 = o->dt[0];
		float dtm = dt0 * detune;
		o->dt[1] = dt0 - 0.11002313f * dtm;
		o->dt[2] = dt0 - 0.06288439f * dtm;
		o->dt[3] = dt0 - 0.01952356f * dtm;
		o->dt[4] = dt0 + 0.01991221f * dtm;
		o->dt[5] = dt0 + 0.06216538f * dtm;
		o->dt[6] = dt0 + 0.10745242f * dtm;
		o->prevDetune = detune;
	}
	if (mix != o->prevMix) {
		o->a0 = -0.55366f * mix + 0.99785f;
		o->an = -0.73764f * mix * mix + 1.2841f * mix + 0.044372f;
		o->prevMix = mix;
	}
	float s0;
	float sn;
	int32_t *t = o->t;
	int32_t *dt = o->dt;
	while (nframes--) {
		// centre saw
		t[0] += dt[0];
		s0 = t[0] * KS2F * o->a0;
		// sum detuned saws
		sn = 0.f;
		t[1] += dt[1];
		sn += t[1] * KS2F;
		t[2] += dt[2];
		sn += t[2] * KS2F;
		t[3] += dt[3];
		sn += t[3] * KS2F;
		t[4] += dt[4];
		sn += t[4] * KS2F;
		t[5] += dt[5];
		sn += t[5] * KS2F;
		t[6] += dt[6];
		sn += t[6] * KS2F;
		// mix detuned saws
		s0 += sn * o->an;
		// hpf
		o->low += o->band * o->freq;
		o->high = s0 - o->low - HPQ * o->band;
		o->band += o->high * o->freq;
		*samples++ = o->high;
	}
}
