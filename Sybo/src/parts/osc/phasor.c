/*
 * Phasor.c
 *
 * http://musicdsp.org/showArchiveComment.php?ArchiveID=9
 * A 2nd order harmonic oscillator, as described by RBJ
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#include "phasor.h"
#include <math.h>

void Phasor_init(Phasor *p, float w, float theta) {
    p->b1 = 2.f * cosf(w);
    p->y1 = sinf(theta - w);
    p->y2 = sinf(theta - 2.f * w);
}

void Phasor_initSin(Phasor *p, float w) {
	Phasor_init(p, w, 0.f);
}

void Phasor_initCos(Phasor *p, float w) {
	Phasor_init(p, w, 0.5f * (float)M_PI);
}

inline float Phasor_out(Phasor *p) {
    p->y0 = p->b1 * p->y1 - p->y2;
    p->y2 = p->y1;
    p->y1 = p->y0;
    return p->y0;
}

void Phasor_process(Phasor *p, float *out, int nframes) {
	float y0 = p->y0;
	float y1 = p->y1;
	float y2 = p->y2;
	float b1 = p->b1;
	int nb = nframes >> 2u;
	while (nb > 0u) {
	    y0 = b1 * y1 - y2;
	    y2 = y1;
	    y1 = y0;
	    *out++ = y0;
	    y0 = b1 * y1 - y2;
	    y2 = y1;
	    y1 = y0;
	    *out++ = y0;
	    y0 = b1 * y1 - y2;
	    y2 = y1;
	    y1 = y0;
	    *out++ = y0;
	    y0 = b1 * y1 - y2;
	    y2 = y1;
	    y1 = y0;
	    *out++ = y0;
	    nb--;
	}
	p->y0 = y0;
	p->y1 = y1;
	p->y2 = y2;
	p->b1 = b1;
}
