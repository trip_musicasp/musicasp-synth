/*
 * Phasor.h
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef PHASOR_H_
#define PHASOR_H_

typedef struct {
	float y0, y1, y2;
	float b1;
} Phasor;

void Phasor_init(Phasor *p, float w, float theta);
void Phasor_initSin(Phasor *p, float w);
void Phasor_initCos(Phasor *p, float w);
void Phasor_process(Phasor *p, float *out, int nframes);
float Phasor_out(Phasor *p);

#endif /* PHASOR_H_ */
