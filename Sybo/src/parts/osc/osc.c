/*
 * osc.c
 *
 *  Created on: 18 Apr 2013
 *      Author: st
 */

#include "osc.h"

const char *Osc_TypeName(unsigned n) {
	switch (n) {
	case OSC_TRIPLE: return "Triple";
	case OSC_DSF: return "DSF";
	case OSC_SUPERSAW: return "Supersaw";
	default: return "?";
	}
}

const char *Osc_ShapeName(unsigned n) {
	switch (n) {
	case SHAPE_SAW: return "Saw";
	case SHAPE_SQUARE: return "Square";
	default: return "?";
	}
}


