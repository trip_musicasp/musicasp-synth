/*
 * supersaw.h
 *
 *  Created on: 5 Apr 2013
 *      Author: st
 */

#ifndef SUPERSAW_H_
#define SUPERSAW_H_

#include <stdint.h>

typedef struct {
	int32_t t[7];
	int32_t dt[7];
	float prevDetune;
	float prevMix;
	float a0, an;					// mix levels
	float freq, low, high, band; 	// the HP SVF
} SuperSawOscillator;

void SuperSaw_init(SuperSawOscillator *o, float wn);
void SuperSaw_process(SuperSawOscillator *o, float *samples, int nframes,
		float detune, float mix);

#endif


