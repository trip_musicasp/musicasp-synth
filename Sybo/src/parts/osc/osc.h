/*
 * osc.h
 *
 *  Created on: 18 Apr 2013
 *      Author: st
 */

#ifndef OSC_H_
#define OSC_H_

enum {
	OSC_TRIPLE,
	OSC_DSF,
	OSC_SUPERSAW,
	N_OSC_TYPES		// must be last
};

enum {
	SHAPE_SAW,
	SHAPE_SQUARE,
	N_OSC_SHAPES
};

const char *Osc_TypeName(unsigned n);
const char *Osc_ShapeName(unsigned n);

#endif /* OSC_H_ */
