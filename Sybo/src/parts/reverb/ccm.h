/*
 * ccm.h
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#ifndef CCM_H_
#define CCM_H_

#include <stdint.h>

#define CCM_BASE 	0x10000000
#define CCM_END		(CCM_BASE + 0xffff)

void CCM_reset(void);
int16_t *CCM_alloc(uint16_t nwords);
uint16_t CCM_available(void);

#endif /* CCM_H_ */
