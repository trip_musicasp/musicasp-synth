/*
 * reverb.h
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#ifndef REVERB_H_
#define REVERB_H_

#include <stdint.h>
#include <stdlib.h>
#include <unrolled.h>
#include <s16math.h>

enum {
	ALGO_GPLATE,
	ALGO_KBVERB,
	N_ALGOS
};

typedef struct {
	void    (*build)(void);
	int16_t (*diffuseInput)(int16_t sample);
	int16_t (*tankProcessLeft)(int16_t sample, int16_t dampingCoeff);
	int16_t (*tankProcessRight)(int16_t sample, int16_t dampingCoeff);
	int16_t (*tankOutputLeft)(void);
	int16_t (*tankOutputRight)(void);
} ReverbAlgorithm;

typedef struct {
	int16_t zm1;
} Filter;

static inline int16_t filter(Filter *f, int16_t k, int16_t s) {
	return f->zm1 = s16mul(k, (f->zm1 - s)) + s;
}

void Reverb_Init(uint32_t nframes);
void Reverb_Process(float **output, float *input, uint32_t nframes,
		unsigned algorithm,
		float preDelay, float bandwidth, float loopGain, float dampingCoeff);

const char *Reverb_AlgorithmName(unsigned n);
void Reverb_printLevels(void);

#endif /* REVERB_H_ */
