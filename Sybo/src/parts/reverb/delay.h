/*
 * delay.h
 *
 * Signed 16 bit delay and allpass delay for reverbs.
 *
 *  Created on: 31 Mar 2013
 *      Author: st
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <stdint.h>
#include "s16math.h"

typedef struct {
	uint16_t size;			// the maximum that can be used
	uint16_t used;			// how much of the size is used
	uint16_t index;			// current index into circular buffer
	int16_t *buffer;		// circular buffer
	int16_t  k;				// for allpass only
} Delay;

/**
 * Delay
 */
static inline int16_t delay(Delay *d, int16_t input) {
	uint16_t i = d->index;
	int16_t *sample = &d->buffer[i];
	d->index = (++i >= d->used) ? 0 : i;

	int16_t output = *sample;
	*sample = input;

	return output;
}

/**
 * Allpass Delay
 */
static inline int16_t allpass(Delay *d, int16_t input) {
	uint16_t i = d->index;
	int16_t *sample = &d->buffer[i];
	d->index = (++i >= d->used) ? 0 : i;

	int16_t bufout = *sample;
	*sample = input + s16mul(bufout, d->k);
	int16_t output = bufout - s16mul(*sample, d->k);

	return output;
}

/**
 * Tap
 */
static inline int16_t tap(Delay *d, uint16_t z) {
	int16_t *b = d->buffer;
	int16_t *p = b + d->index - z;
	if (p < b) p += d->used;
	return *p;
}

void Delay_Init(Delay *d, uint16_t size);
void Delay_ResetAll(void);

#endif /* DELAY_H_ */
