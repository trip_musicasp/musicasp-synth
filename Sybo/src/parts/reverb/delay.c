/*
 * delay.c
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#include "delay.h"
#include "ccm.h"

void Delay_Init(Delay *d, uint16_t size) {
	if (!size) size = CCM_available();
	d->buffer = CCM_alloc(size);
	if (!d->buffer) return;
	d->size = d->used = size;
}

void Delay_ResetAll(void) {
	CCM_reset();
}

