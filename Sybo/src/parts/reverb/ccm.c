/*
 * ccm.c
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#include "ccm.h"
#include <string.h>

static int16_t *ccmptr = 0;

void CCM_reset(void) {
	ccmptr = (int16_t *)CCM_BASE;
}

int16_t *CCM_alloc(uint16_t nwords) {
	int16_t *ptr = ccmptr;
	if (ccmptr + nwords > (int16_t *)CCM_END) return 0;
	memset(ccmptr, 0, nwords << 1);
	ccmptr += nwords;
	return ptr;
}

uint16_t CCM_available(void) {
	return (int16_t *)CCM_END - ccmptr;
}
