/*
 * reverb.c
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#include "reverb.h"
#include <convert.h>
#include "s16math.h"
#include <gplate.h>	// for default algorithm
#include <kbverb.h>	// for default algorithm
#include "resample.h"
#include "ccm.h"
#include <block.h>
#include <stdio.h>

static int16_t *inWords;
static int16_t *outWords[2];
static unsigned nalgo = 999;			// curent algo index (bogus init)
static ReverbAlgorithm *algos[N_ALGOS]; // array of pointers to algos
static ReverbAlgorithm *a;				// pointer to current algo
static Delay dpre;
static Filter fbw;
static HQDecimator decimator;
static HQInterpolator interpolator0, interpolator1;

#if USE_PERF
	float reverbInMax, reverbOutLMax, reverbOutRMax;
#endif

static void buildAlgoArray(void) {
	algos[ALGO_GPLATE] = &GPlate;
	algos[ALGO_KBVERB] = &KBverb;
}

static void changeAlgo(int n) {
	if (n == nalgo) return;
	nalgo = n;
	a = algos[nalgo];
	a->build();
	//	printf("CCM available after Reverb allocation: %u words\n", CCM_available());
}

void Reverb_Init(uint32_t nframes) {
	inWords = (int16_t *)calloc(nframes, sizeof(int16_t));
	outWords[0] = (int16_t *)calloc(nframes, sizeof(int16_t));
	outWords[1] = (int16_t *)calloc(nframes, sizeof(int16_t));
	buildAlgoArray();
	changeAlgo(ALGO_GPLATE);

	// we build a pre delay here because its common to all reverbs
	// and it is easier to handle here
	dpre.buffer = CCM_alloc(CCM_available());
}

/**
 * The 16 bit reverb engine.
 * We abstract the algorithm implementation details to 5 function pointers
 */
void Reverb_ProcessS16(int16_t *input, int16_t **output, uint32_t nframes,
		int16_t preDelay, int16_t bw, int16_t loopGain, int16_t dampingCoeff) {
	static int16_t tankOutputL = 0, tankOutputR = 0;
	if (!a) return;
	int16_t *outL = output[0], *outR = output[1];
	int16_t diffusedInput;
	dpre.used = s16mul(dpre.size, preDelay);
	int i;
	for (i = 0; i < nframes; i++) {
		diffusedInput = a->diffuseInput(delay(&dpre,
											filter(&fbw, bw, *input++)));
		tankOutputL = a->tankProcessLeft(diffusedInput +
											s16mul(tankOutputR, loopGain),
											dampingCoeff);
		tankOutputR = a->tankProcessRight(diffusedInput +
											s16mul(tankOutputL, loopGain),
											dampingCoeff);
		*outL++ = a->tankOutputLeft();
		*outR++ = a->tankOutputRight();
	}
}

/**
 * Wrap the 16 bit reverb engine in float/s16 conversions
 * input/output buffer is a little counter-intuitive now decimation
 * and interpolation are involved.
 */
void Reverb_Process(float **output, float *input, uint32_t nframes,
		unsigned na,
		float preDelay, float bandwidth, float loopGain, float dampingCoeff) {
	changeAlgo(na);
	if (!a) return;
	nframes = HQDecimator_Process(&decimator, output[0], input, nframes); // note buffer use
#if USE_PERF
	float max = max_process(output[0], nframes);
	if (max > reverbInMax) reverbInMax = max;
#endif
	Unrolled_F32toS16(inWords, output[0], nframes);
	Reverb_ProcessS16(inWords, outWords, nframes,
			F32toS16(preDelay), F32toS16(1.f - bandwidth),
			F32toS16(loopGain), F32toS16(dampingCoeff));
	Unrolled_S16toF32(input, outWords[0], nframes);
#if USE_PERF
	max = max_process(input, nframes);
	if (max > reverbOutLMax) reverbOutLMax = max;
#endif
	HQInterpolator_Process(&interpolator0, output[0], input, nframes); // note buffer use
	Unrolled_S16toF32(input, outWords[1], nframes);
#if USE_PERF
	max = max_process(input, nframes);
	if (max > reverbOutRMax) reverbOutRMax = max;
#endif
	HQInterpolator_Process(&interpolator1, output[1], input, nframes); // note buffer use
}

const char *Reverb_AlgorithmName(unsigned n) {
	switch (n) {
	case ALGO_GPLATE: return "GPlate";
	case ALGO_KBVERB: return "KBverb";
	default: return "?";
	}
}


void Reverb_printLevels(void) {
	printf("Reverb: input %.2fdB, output left %.2fdB, output right %.2fdB\n",
			20.f*log10f(reverbInMax),
			20.f*log10f(reverbOutLMax),
			20.f*log10f(reverbOutRMax));
	reverbInMax = reverbOutLMax = reverbOutRMax = 0.f;
}

