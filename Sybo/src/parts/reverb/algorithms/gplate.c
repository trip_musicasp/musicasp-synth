/*
 * gplate.c
 *
 * The Griesinger plate reverb algorithm as presented by D'Attorro
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#include "gplate.h"

static GPlate_InputDiffuser id;
static GPlate_TankStage ts1[2], ts2[2]; // [LEFT/RIGHT]
static Filter f[2];

static const int16_t S16_50 = 0x3FFF;
static const int16_t S16_625 = 0x4FFF;
static const int16_t S16_70m = 0xA668;
static const int16_t S16_75 = 0x5FFF;

static void build(void) {
	// reset the delay memory
	Delay_ResetAll();

	// build input diffuser;
	Delay_Init(&id.a1, 114);		id.a1.k = S16_75;
	Delay_Init(&id.a2, 87);			id.a2.k = S16_75;
	Delay_Init(&id.a3, 306);		id.a3.k = S16_625;
	Delay_Init(&id.a4, 223);		id.a4.k = S16_625;

	// build tank left
	Delay_Init(&ts1[0].a, 542);		id.a1.k = S16_70m;
	Delay_Init(&ts1[0].d, 3590);
	Delay_Init(&ts2[0].a, 1451);	id.a1.k = S16_50;
	Delay_Init(&ts2[0].d, 2999);

	// build tank right
	Delay_Init(&ts1[1].a, 732);		id.a1.k = S16_70m;
	Delay_Init(&ts1[1].d, 3400);
	Delay_Init(&ts2[1].a, 2141);	id.a1.k = S16_50;
	Delay_Init(&ts2[1].d, 2550);
}

static inline int16_t tankOutputLeft(void) {
	return(-(int32_t)tap(&ts1[0].d, 1604) -
			(int32_t)tap(&ts2[0].a, 151) -
			(int32_t)tap(&ts2[0].d, 859) +
			(int32_t)tap(&ts1[1].d, 214) +
			(int32_t)tap(&ts1[1].d, 2398) -
			(int32_t)tap(&ts2[1].a, 1542) +
			(int32_t)tap(&ts2[1].d, 1609) ) >> 2;
}

static inline int16_t tankOutputRight(void) {
	return(-(int32_t)tap(&ts1[1].d, 2111) -
			(int32_t)tap(&ts2[1].a, 270) -
			(int32_t)tap(&ts2[1].d, 98) +
			(int32_t)tap(&ts1[0].d, 285) +
			(int32_t)tap(&ts1[0].d, 2924) -
			(int32_t)tap(&ts2[0].a, 990) +
			(int32_t)tap(&ts2[0].d, 2155) ) >> 2;
}

static int16_t diffuseInput(int16_t s) {
	return
		allpass(&id.a4,
			allpass(&id.a3,
				allpass(&id.a2,
					allpass(&id.a1, s))));
}

static int16_t tankProcessLeft(int16_t s, int16_t k) {
	return
		delay(&ts2[0].d,
			allpass(&ts2[0].a,
				filter(&f[0], k,
					delay(&ts1[0].d,
						allpass(&ts1[0].a, s)))));
}

static int16_t tankProcessRight(int16_t s, int16_t k) {
	return
		delay(&ts2[1].d,
			allpass(&ts2[1].a,
				filter(&f[1], k,
					delay(&ts1[1].d,
						allpass(&ts1[1].a, s)))));
}


ReverbAlgorithm GPlate = {
		&build,
		&diffuseInput,
		&tankProcessLeft,
		&tankProcessRight,
		&tankOutputLeft,
		&tankOutputRight
};
