/*
 * gplate.h
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#ifndef KBVERB_H_
#define KBVERB_H_

#include <stdint.h>
#include <reverb.h>
#include <delay.h>

typedef struct {
	Delay a1, a2, a3, a4;
} KBverb_InputDiffuser;

typedef struct {
	Delay a1, a2;
	Delay d;
} KBverb_TankStage;

extern ReverbAlgorithm KBverb;

#endif /* KBVERB_H_ */
