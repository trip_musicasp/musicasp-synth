/*
 * kbverb.c
 *
 * A Keith Barr (RIP) reverb algorithm
 * http://www.spinsemi.com/knowledge_base/effects.html#Reverberation
 *
 *  Created on: 18 Apr 2013
 *      Author: st
 */

#include "kbverb.h"

static KBverb_InputDiffuser id;
static KBverb_TankStage ts1[2], ts2[2]; // [LEFT/RIGHT]
static Filter f[2];

static const int16_t S16_50 = 0x3FFF;
static const int16_t S16_625 = 0x4FFF;
static const int16_t S16_70m = 0xA668;
static const int16_t S16_75 = 0x5FFF;

static void build(void) {
	// reset the delay memory
	Delay_ResetAll();

	// build input diffuser;
	Delay_Init(&id.a1, 114);		id.a1.k = S16_75;
	Delay_Init(&id.a2, 87);			id.a2.k = S16_75;
	Delay_Init(&id.a3, 306);		id.a3.k = S16_625;
	Delay_Init(&id.a4, 223);		id.a4.k = S16_625;

	// build tank left
	Delay_Init(&ts1[0].a1,  542);	id.a1.k = S16_70m;
	Delay_Init(&ts1[0].a2, 1451);	id.a1.k = S16_50;
	Delay_Init(&ts1[0].d,  3591);
	Delay_Init(&ts2[0].a1,  967);	id.a1.k = S16_50;
	Delay_Init(&ts2[0].a2, 1948);	id.a1.k = S16_50;
	Delay_Init(&ts2[0].d,  2550);

	// build tank right
	Delay_Init(&ts1[1].a1,  732);	id.a1.k = S16_70m;
	Delay_Init(&ts1[1].a2, 2142);	id.a1.k = S16_50;
	Delay_Init(&ts1[1].d,  3401);
	Delay_Init(&ts2[1].a1,  638);	id.a1.k = S16_50;
	Delay_Init(&ts2[1].a2, 2268);	id.a1.k = S16_50;
	Delay_Init(&ts2[1].d,  3000);
}

static inline int16_t tankOutputLeft(void) {
	return( (int32_t)tap(&ts1[0].d,  138) +
			(int32_t)tap(&ts1[0].d, 2836) -
			(int32_t)tap(&ts2[0].d,  950) +
			(int32_t)tap(&ts2[0].d, 2237) -
			(int32_t)tap(&ts1[1].d,  345) -
			(int32_t)tap(&ts1[1].d, 3196) +
			(int32_t)tap(&ts2[1].d,  774) +
			(int32_t)tap(&ts2[1].d, 2815) ) >> 2;
}

static inline int16_t tankOutputRight(void) {
//	return tap(&ts1[0].d, 605);
	return( (int32_t)tap(&ts1[0].d,  605) -
			(int32_t)tap(&ts1[0].d, 1946) +
			(int32_t)tap(&ts2[0].d,  309) +
			(int32_t)tap(&ts2[0].d, 2539) +
			(int32_t)tap(&ts1[1].d, 1069) +
			(int32_t)tap(&ts1[1].d, 2305) -
			(int32_t)tap(&ts2[1].d,   26) -
			(int32_t)tap(&ts2[1].d, 1922) ) >> 2;
}

static int16_t diffuseInput(int16_t s) {
	return
		allpass(&id.a4,
			allpass(&id.a3,
				allpass(&id.a2,
					allpass(&id.a1, s))));
}

static int16_t tankProcessLeft(int16_t s, int16_t k) {
	return
		delay(&ts2[0].d,
			allpass(&ts2[0].a2,
				allpass(&ts2[0].a1,
					filter(&f[0], k,
						delay(&ts1[0].d,
							allpass(&ts1[0].a2,
								allpass(&ts1[0].a1, s)))))));
}

static int16_t tankProcessRight(int16_t s, int16_t k) {
	return
		delay(&ts2[1].d,
			allpass(&ts2[1].a2,
				allpass(&ts2[1].a1,
					filter(&f[1], k,
						delay(&ts1[1].d,
							allpass(&ts1[1].a2,
								allpass(&ts1[1].a1, s)))))));
}


ReverbAlgorithm KBverb = {
		&build,
		&diffuseInput,
		&tankProcessLeft,
		&tankProcessRight,
		&tankOutputLeft,
		&tankOutputRight
};
