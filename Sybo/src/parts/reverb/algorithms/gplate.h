/*
 * gplate.h
 *
 *  Created on: 1 Apr 2013
 *      Author: st
 */

#ifndef GPLATE_H_
#define GPLATE_H_

#include <stdint.h>
#include "reverb.h"
#include "delay.h"

typedef struct {
	Delay a1, a2, a3, a4;
} GPlate_InputDiffuser;

typedef struct {
	Delay a;
	Delay d;
} GPlate_TankStage;

extern ReverbAlgorithm GPlate;

#endif /* GPLATE_H_ */
