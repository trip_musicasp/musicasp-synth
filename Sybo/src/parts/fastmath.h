/*
 * fast_math.h
 *
 *  Created on: 17 Mar 2013
 *      Author: st
 */

#ifndef FAST_MATH_H_
#define FAST_MATH_H_

#include <math.h>

typedef union {
	float f;
	int i;
} FloatIntUnion;

// http://www.music.columbia.edu/pipermail/music-dsp/2002-October/017790.html
static inline float fastpow2(float x) {
	long *px = (long*) (&x); // store address of float as long pointer
	const float tx = (x - 0.5f) + (3 << 22); // temporary value for truncation
	const long lx = *((long*) &tx) - 0x4b400000; // integer power of 2
	const float dx = x - (float) (lx); // float remainder of power of 2
	x = 1.0f + dx * (0.6960656421638072f + // cubic apporoximation of 2^x
			dx * (0.224494337302845f + // for x in the range [0, 1]
					dx * (0.07944023841053369f)));
	*px += (lx << 23); // add integer power of 2 to exponent
	return x;
}

// http://www.devmaster.net/forums/showthread.php?t=5784
static const float S_B = (float) (4 / M_PI);
static const float S_C = (float) (-4 / (M_PI * M_PI));
// -PI < x < PI, a crude sin only useful for modulation
static inline float fastsin(const float x) {
	return S_B * x + S_C * x * fabsf(x);
}

static const float TWODIVPI = (float) (2 / M_PI);
// thanks scoofy[AT]inf[DOT]elte[DOT]hu
// for musicdsp.org pseudo-code improvement
// -PI < x < PI
static inline float fasttri(float x) {
	x += M_PI; 		//  0 < x < 2*PI
	x *= TWODIVPI; 	//  0 < x < 4
	x -= 1; 		// -1 < x < 3
	if (x > 1)
		x -= 4.f;
	return fabsf(-(fabsf(x) - 2.f)) - 1.f;
}

/**
 * pade-approximation to tanh.
 */
static inline float fasttanh(float x) {
	// clamping is advisable because |x| > 5 does cause very high output
	// we clamp at 3 because that's where output is unity and C1/C2 continuous
	if ( fabsf(x) > 3.f ) return 1;
	float x2 = x * x;
	return x * (27.f + x2) / (27.f + 9.f * x2);
}

// http://cbloomrants.blogspot.co.uk/2009/06/06-21-09-fast-exp-log.html
static inline float fastexpf(float x) {
    FloatIntUnion u;
    // 12102203.16156f = (2^23)/ln(2), 361007 = (0.08607133/2)*(2^23)
    u.i = (int)(x * 12102203.16156f) + (127 << 23) - 361007;
    return u.f;
}

#endif /* FAST_MATH_H_ */

