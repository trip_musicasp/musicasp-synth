/*
 * unrolled.c
 *
 *  Created on: 18 Feb 2013
 *      Author: st
 */

#include "unrolled.h"
#include "fastmath.h"
#include <math.h>

void Unrolled_mult(float *d, float *s1, float *s2, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s1++ * *s2++;
		*d++ = *s1++ * *s2++;
		*d++ = *s1++ * *s2++;
		*d++ = *s1++ * *s2++;
		nb--;
	}
}

void Unrolled_add(float *d, float *s1, float *s2, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s1++ + *s2++;
		*d++ = *s1++ + *s2++;
		*d++ = *s1++ + *s2++;
		*d++ = *s1++ + *s2++;
		nb--;
	}
}

void Unrolled_sub(float *d, float *s1, float *s2, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s1++ - *s2++;
		*d++ = *s1++ - *s2++;
		*d++ = *s1++ - *s2++;
		*d++ = *s1++ - *s2++;
		nb--;
	}
}

void Unrolled_scale(float *d, float *s, float K, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s++ * K;
		*d++ = *s++ * K;
		*d++ = *s++ * K;
		*d++ = *s++ * K;
		nb--;
	}
}

void Unrolled_offset(float *d, float *s, float K, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s++ + K;
		*d++ = *s++ + K;
		*d++ = *s++ + K;
		*d++ = *s++ + K;
		nb--;
	}
}

// ---- compound functions ---------------------------------------------------
// d = s1 + s2 * K
void Unrolled_scale_add(float *d, float *s1, float *s2, float K, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s1++ + *s2++ * K;
		*d++ = *s1++ + *s2++ * K;
		*d++ = *s1++ + *s2++ * K;
		*d++ = *s1++ + *s2++ * K;
		nb--;
	}
}

// d = s1 * s2 * K
void Unrolled_scale_mult(float *d, float *s1, float *s2, float K, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s1++ * *s2++ * K;
		*d++ = *s1++ * *s2++ * K;
		*d++ = *s1++ * *s2++ * K;
		*d++ = *s1++ * *s2++ * K;
		nb--;
	}
}

// ---- unary functions ----------------------------------------------------

void Unrolled_abs(float *d, float *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = fabsf(*s++);
		*d++ = fabsf(*s++);
		*d++ = fabsf(*s++);
		*d++ = fabsf(*s++);
		nb--;
	}
}

void Unrolled_fill(float *d, float K, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = K;
		*d++ = K;
		*d++ = K;
		*d++ = K;
		nb--;
	}
}

void Unrolled_copy(float *d, float *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = *s++;
		*d++ = *s++;
		*d++ = *s++;
		*d++ = *s++;
		nb--;
	}
}

void Unrolled_pow2(float *d, float *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = fastpow2(*s++);
		*d++ = fastpow2(*s++);
		*d++ = fastpow2(*s++);
		*d++ = fastpow2(*s++);
		nb--;
	}
}


// ---- conversion functions -------------------------------------------------

void Unrolled_S16toF32(float *d, int16_t *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = S16toF32(*s++);
		*d++ = S16toF32(*s++);
		*d++ = S16toF32(*s++);
		*d++ = S16toF32(*s++);
		nb--;
	}
}

void Unrolled_F32toS16(int16_t *d, float *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;

	while (nb > 0u) {
		*d++ = F32toS16(*s++);
		*d++ = F32toS16(*s++);
		*d++ = F32toS16(*s++);
		*d++ = F32toS16(*s++);
		nb--;
	}
}

// ---- deprecated functions -----------------------------------------------

// clamps 0..1 but measures slower than 2 ifs per samples in Moog :(
// the assembler output with -Ofast is insane too
// http://stackoverflow.com/questions/427477/fastest-way-to-clamp-a-real-fixed-floating-point-value
#if 0
#define CLAMP(x) (0.5f*(fabsf(x) - fabsf(x-1.0f) + 1.0f))

void Unrolled_clamp(float *d, float *s, uint32_t nf) {
	uint32_t nb = nf >> 2u;
	float x;

	while (nb > 0u) {
		x = *s++;
		*d++ = CLAMP(x);
		x = *s++;
		*d++ = CLAMP(x);
		x = *s++;
		*d++ = CLAMP(x);
		x = *s++;
		*d++ = CLAMP(x);
		nb--;
	}
}
#endif
