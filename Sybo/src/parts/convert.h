/*
 * convert.h
 *
 *  Created on: 30 Mar 2013
 *      Author: st
 */

#ifndef CONVERT_H_
#define CONVERT_H_

#include <stdint.h>

static inline float S16toF32(const int16_t value) {
	float result;
	asm("fmsr %[result], %[value]\n\t"
		"vcvt.f32.s16 %[result], %[result], #15" :
			[result] "=t" (result) :
			[value] "r" (value));
	return result;
}

static inline int16_t F32toS16(const float value) {
	int16_t result;
	asm("vcvt.s16.f32 %[value], %[value], #15\n\t"
		"fmrs %[result], %[value]" :
			[result] "=r" (result) :
			[value] "t" (value));
	return result;
}

void testConverts(void);

#endif /* CONVERT_H_ */
