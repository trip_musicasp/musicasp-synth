/*
 * moogmode.c
 *
 *  Created on: 10 Apr 2013
 *      Author: st
 */

#include "svfmode.h"

		// outputs (the same as the analog circuit):
		// band = v1;
		// low = v2;
		// high = v0 - k*v1 - v2;
		// notch = high + low;
		// peak = high - low;

static const char *_name[] = {
		"Low", "Band", "High", "Notch", "Peak"
};

const char *SVF_ModeName(unsigned i) {
	if (i > N_SVF_MODES) return "?";
	return _name[i];
}



