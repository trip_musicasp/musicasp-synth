/*
 * moog.c
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#include "filter.h"
#include "moog.h"
#include <math.h>
#include <stdio.h>

// http://www.music.mcgill.ca/~ich/research/misc/papers/cr1071.pdf
// 0 is orginal 12dB loss, 0.5 is 6dB loss (equalish loudness), 1 is no loss
static float comp = 0.5f;

void Moog_init(Moog *m) {
	m->in1 = m->in2 = m->in3 = m->in4 = 0.f;
	m->out1 = m->out2 = m->out3 = m->out4 = 0.f;
}

/*
 * passed 2 equals length buffers, samples (to be replaced) and
 * fc (0..1), also fixed resonance (0..1).
 */
void Moog_process(Moog *m, float *samples, int nframes, float *wc, float res) {
	float input;
	float fb;
	float w, wsquared;
	float in1 = m->in1;
	float in2 = m->in2;
	float in3 = m->in3;
	float in4 = m->in4;
	float out1 = m->out1;
	float out2 = m->out2;
	float out3 = m->out3;
	float out4 = m->out4;
	res *= 4.f;
	while (nframes--) {
		w = *wc++ * 2.f;
		CLAMP_W(w);
		w *= 1.16f;
		wsquared = w * w;
		fb = res * (1.0f - 0.15f * wsquared);
		input = *samples;
		input -= (out4 - comp*input) * fb;
		input *= 0.35013f * wsquared * wsquared;
		out1 = input + 0.3f * in1 + (1.f - w) * out1; // Pole 1
		in1 = input;
		out2 = out1 + 0.3f * in2 + (1.f - w) * out2; // Pole 2
		in2 = out1;
		out3 = out2 + 0.3f * in3 + (1.f - w) * out3; // Pole 3
		in3 = out2;
		out4 = out3 + 0.3f * in4 + (1.f - w) * out4; // Pole 4
		in4 = out3;
		*samples++ = out4;
	}
	m->in1 = in1;
	m->in2 = in2;
	m->in3 = in3;
	m->in4 = in4;
	m->out1 = out1;
	m->out2 = out2;
	m->out3 = out3;
	m->out4 = out4;
}

