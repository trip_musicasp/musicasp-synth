/*
 * assvf.c
 *
 *  Created on: 11 Apr 2013
 *      Author: st
 */

#include "filter.h"
#include "assvf.h"
#include <math.h>

void ASSVF_init(ASSVF *f, int mode) {
	f->v1 = f->v2 = 0.f;
	f->v0z = f->v1z = f->v2z = 0;
	f->mode = mode;
}

void ASSVF_process(ASSVF *f, float *samples, int nframes, float *wc, float res) {
	float k = 2.f*(1.f - res); // (typically in the range 2 to 0);
	float v1 = f->v1;
	float v2 = f->v2;
	float v0z = f->v0z;
	float v1z = f->v1z;
	float v2z = f->v2z;
	int mode = f->mode;
	float w, g, v0;
	while (nframes--) {
		w = *wc++;
		CLAMP_W(w);

		g = tanf((float)M_PI * w);

		v1z = v1;
		v2z = v2;
		v0 = *samples;
		v1 = v1z + g * (v0 + v0z - 2.f*(g + k)*v1z - 2.f*v2z) / (1.f + g*(g + k));
		v2 = v2z + g * (v1 + v1z);
		v0z = v0;

		// outputs (the same as the analog circuit):
		switch (mode) {
		case SVF_LOW:
			// low = v2;
			*samples++ = v2;
			break;
		case SVF_BAND:
			// band = v1;
			*samples++ = v1;
			break;
		case SVF_HIGH:
			// high = v0 - k*v1 - v2;
			*samples++ = v0 - k*v1 - v2;
			break;
		case SVF_NOTCH:
			// notch = high + low;
			*samples++ = v0 - k*v1;
			break;
		case SVF_PEAK:
			// peak = high - low;
			*samples++ = v0 - k*v1 - 2.f*v2;
			break;
		}
	}
	f->v1 = v1;
	f->v2 = v2;
	f->v0z = v0z;
	f->v1z = v1z;
	f->v2z = v2z;
}
