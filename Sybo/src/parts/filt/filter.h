/*
 * filter.h
 *
 *  Created on: 11 Apr 2013
 *      Author: st
 */

#ifndef FILTER_H_
#define FILTER_H_

#include "moog.h"
#include "tladder.h"
#include "assvf.h"
#include "moogmode.h"
#include "svfmode.h"

enum {
	FILTER_NONE,
	FILTER_MOOG,				// cheap and cheerful linear
	FILTER_TEEMU_TR_LADDER_0,	// cheapish ZDF linear
	FILTER_TEEMU_TR_LADDER,		// expensive ZDF non-linear
	FILTER_ANDY_SVF,			// cheapish linear
	N_FILTER_TYPES
};

#define CLAMP_W(w) if (w < wmin) w = wmin; else if (w > wmax) w = wmax

extern float wmin, wmax;

void Filter_Setup(float winm, float wmax);
const char *Filter_TypeName(unsigned n);

#endif /* FILTER_H_ */
