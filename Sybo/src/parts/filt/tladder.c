//// LICENSE TERMS: Copyright 2012 Teemu Voipio
//
// You can use this however you like for pretty much any purpose,
// as long as you don't claim you wrote it. There is no warranty.
//
// Distribution of substantial portions of this code in source form
// must include this copyright notice and list of conditions.
//

/*
 * tladder.c
 *
 * http://www.kvraudio.com/forum/viewtopic.php?t=349859&postdays=0&postorder=asc
 * &start=0&sid=637fd02db124b24de94d2b5a48d02327
 *
 * You can trivially remove any saturator by setting the corresponding gain
 * t0,...,t1 to 1. Also, you can simply scale any saturator (i.e. change
 * clipping threshold) to 1/a*tanh(a*x) by writing
 * 		float t1 = tanhXdX(a*s[0]);
 *
 *  Created on: 11 Apr 2013
 */

#include "tladder.h"
#include "filter.h"
#include <math.h>

// http://www.music.mcgill.ca/~ich/research/misc/papers/cr1071.pdf
// 0 is orginal 12dB loss, 0.5 is 6dB loss (equalish loudness), 1 is no loss
static float comp = 0.5f;

// input delay and state for member variables
void Tladder_init(Tladder *l, int mode) {
	l->s[0] = l->s[1] = l->s[2] = l->s[3] = 0.f;
	MoogMode m = MoogMode_mode[mode];
	l->a = m.a;
	l->b = m.b;
	l->c = m.c;
	l->d = m.d;
	l->e = m.e;
}

// tanh(x)/x approximation, flatline at very high inputs
// so might not be safe for very large feedback gains
// [limit is 1/15 so very large means ~15 or +23dB]
static inline float tanhXdX(float x) {
	float a = x*x;
	// IIRC I got this as Pade-approx for tanh(sqrt(x))/sqrt(x)
	return ((a + 105.f)*a + 945.f) / ((15.f*a + 420.f)*a + 945.f);
}

// wc as normalized frequency (eg 0.5 = Nyquist)
// resonance from 0 to 1, self-oscillates at settings over 0.9
void Tladder_process(Tladder *l, float *samples, int nframes,
		float *wc, float resonance) {
	// tuning and feedback
	float r = (40.0/9.0) * resonance;

	float *s = &l->s[0];
	float zi = l->z1;
	while (nframes--) {
		float wi = *wc++; 	// 0 .. 1
		CLAMP_W(wi);
		float w = tanf((float)M_PI * wi);

		// input with half delay, for non-linearities
		float in = *samples;
		float ih = 0.5f * (in + zi); zi = in;

		// evaluate the non-linear gains
		float t0 = tanhXdX(ih - r * (s[3] - comp*ih));
		float t1 = tanhXdX(s[0]);
		float t2 = tanhXdX(s[1]);
		float t3 = tanhXdX(s[2]);
		float t4 = tanhXdX(s[3]);

		// g# the denominators for solutions of individual stages
		float g0 = 1.f / (1.f + w*t1), g1 = 1.f / (1.f + w*t2);
		float g2 = 1.f / (1.f + w*t3), g3 = 1.f / (1.f + w*t4);

		// f# are just factored out of the feedback solution
		float f3 = w*t3*g3, f2 = w*t2*g2*f3, f1 = w*t1*g1*f2, f0 = w*t0*g0*f1;

		// solve feedback
		float y3 = (g3*s[3] + f3*g2*s[2] + f2*g1*s[1] + f1*g0*s[0] + f0*in) / (1.f + r*f0);

		// then solve the remaining outputs (with the non-linear gains here)
		float xx = t0*(in - r * (y3 - comp*in));
		float y0 = t1*g0*(s[0] + w*xx);
		float y1 = t2*g1*(s[1] + w*y0);
		float y2 = t3*g2*(s[2] + w*y1);

		// update state
		s[0] += 2.f*w * (xx - y0);
		s[1] += 2.f*w * (y0 - y1);
		s[2] += 2.f*w * (y1 - y2);
		s[3] += 2.f*w * (y2 - t4*y3);

		*samples++ =
				l->a * xx +
				l->b * y0 +
				l->c * y1 +
				l->d * y2 +
				l->e * y3 * 0.94f; // average of 92% .. 96%
	}
	l->z1 = zi;
}

// simplified for no saturation, fully linear model
// wc as normalized frequency (eg 0.5 = Nyquist)
// resonance from 0 to 1, self-oscillates at settings over 0.9
void Tladder_process0(Tladder *l, float *samples, int nframes,
		float *wc, float resonance) {
	// tuning and feedback
	float r = (40.0/9.0) * resonance;

	float *s = &l->s[0];
	float zi = l->z1;
	while (nframes--) {
		float wi = *wc++; 	// 0 .. 1
		CLAMP_W(wi);
		float w = tanf((float)M_PI * wi);

		// input with half delay, for non-linearities
		float in = *samples;
//		float ih = 0.5f * (in + zi); zi = in;

		// g# the denominators for solutions of individual stages
		float g = 1.f / (1.f + w);

		// f# are just factored out of the feedback solution
		float f3 = w*g, f2 = w*g*f3, f1 = w*g*f2, f0 = w*g*f1;

		// solve feedback
		float y3 = (g*s[3] + f3*g*s[2] + f2*g*s[1] + f1*g*s[0] + f0*in) / (1.f + r*f0);

		// then solve the remaining outputs (with the non-linear gains here)
		float xx = (in - r * (y3 - comp*in));
		float y0 = g*(s[0] + w*xx);
		float y1 = g*(s[1] + w*y0);
		float y2 = g*(s[2] + w*y1);

		// update state
		s[0] += 2.f*w * (xx - y0);
		s[1] += 2.f*w * (y0 - y1);
		s[2] += 2.f*w * (y1 - y2);
		s[3] += 2.f*w * (y2 - y3);

		*samples++ =
				l->a * xx +
				l->b * y0 +
				l->c * y1 +
				l->d * y2 +
				l->e * y3 * 0.94f; // average of 92% .. 96%
	}
	l->z1 = zi;
}
