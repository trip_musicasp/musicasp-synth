/*
 * moogmode.h
 *
 *  Created on: 10 Apr 2013
 *      Author: st
 */

#ifndef SVFMODE_H_
#define SVFMODE_H_

#define	N_SVF_MODES 5

enum {
	SVF_LOW,
	SVF_BAND,
	SVF_HIGH,
	SVF_NOTCH,
	SVF_PEAK
};

typedef struct {
	const char *name;
} SVFMode;

extern SVFMode SVFMode_mode[];

const char *SVF_ModeName(unsigned n);

#endif /* SVFMODE_H_ */
