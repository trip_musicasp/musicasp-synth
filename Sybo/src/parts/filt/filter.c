/*
 * filter.c
 *
 *  Created on: 11 Apr 2013
 *      Author: st
 */

#include "filter.h"
#include <stdio.h>

float wmin = 0.f;
float wmax = 0.f;

void measureFilterDCGains(void);

void Filter_Setup(float wmn, float wmx) {
	wmin = wmn;
	wmax = wmx;
//	measureFilterDCGains();
}

const char *Filter_TypeName(unsigned n) {
	switch (n) {
	case FILTER_NONE: return "None";
	case FILTER_MOOG: return "Moog";
	case FILTER_TEEMU_TR_LADDER_0: return "Moog 1";
	case FILTER_TEEMU_TR_LADDER: return "Moog 2";
	case FILTER_ANDY_SVF: return "SVF";
	default: return "?";
	}
}

Moog m;
Tladder t;
ASSVF s;

void measureFilterDCGains(void) {
	const int N = 12;
	float res = 0.f;
	float samples[N];
	float wc[12];
	int f;
	int i;
	for (i = 0; i < N; i++) {
		wc[i] = 0.1f; // 4800Hz?
	}
	Moog_init(&m);
	Tladder_init(&t, 3);
	ASSVF_init(&s, 0);

	for (f = 1; f < N_FILTER_TYPES; f++) {
		printf("%d: ", f);
		int r;
		for (r = 0; r < 10; r++) {
			res = (float)r / 10.f;
			int j;
			for (j = 0; j < 5; j++) {
				for (i = 0; i < N; i++) {
					samples[i] = 0.5f;
				}
				switch (f) {
				case FILTER_MOOG:
					Moog_process(&m, &samples[0], N, &wc[0], res);
					break;
				case FILTER_TEEMU_TR_LADDER_0:
					Tladder_process(&t, &samples[0], N, &wc[0], res);
					break;
				case FILTER_TEEMU_TR_LADDER:
					Tladder_process(&t, &samples[0], N, &wc[0], res);
					break;
				case FILTER_ANDY_SVF:
					ASSVF_process(&s, &samples[0], N, &wc[0], res);
					break;
				default:
					printf("Bogus: ");
					break;
				}
			}
			printf("%.1f ", res);
			printf("%.2f, ", samples[N-1]/0.5f);
		}
		printf("\n");
	}
}


