/*
 * moogmode.c
 *
 *  Created on: 10 Apr 2013
 *      Author: st
 */

#include "moogmode.h"

/*
 * from http://www.kvraudio.com/forum/viewtopic.php?t=207647
 *
 * 4 cascaded single pole LP in a typical moog ladder
 *
 * A = 1
 * B = 1/(1+s)
 * C = (1/(1+s))^2
 * D = (1/(1+s))^3
 * E = (1/(1+s))^4
 *
 * Output mix from start and after every stage
 *
 * a*A + b*B + c*C + d*D + e*E
 *
 * When factored to powers of s
 *
 * as^4 + (4a+b)s^3 + (6a+3b+c)s^2 + (4a+3b+2c+d)s + a+b+c+d+e
 * -----------------------------------------------------------
 *                       (1+s)^4
 *
 * then equate to desired transfer function, get (1+s)^4 as bottom of both sides
 * and solve for a b c d e
 *
 * Also see http://vellocet.com/dsp/CascadedFilterResponses/
 */
const MoogMode MoogMode_mode[] = {
		//				a		b		c		d		e
		{ "L4", 		0.f, 	0.f, 	0.f, 	0.f, 	1.f 	},
		{ "L3", 		0.f, 	0.f, 	0.f, 	1.f, 	0.f 	},
		{ "L2", 		0.f, 	0.f, 	1.f, 	0.f, 	0.f 	},
		{ "L1", 		0.f, 	1.f, 	0.f, 	0.f, 	0.f 	},
		{ "H4", 		1.f,   -4.f, 	6.f,   -4.f, 	1.f 	},
		{ "H3", 		1.f,   -3.f, 	3.f,   -1.f, 	0.f 	},
		{ "H2", 		1.f,   -2.f, 	1.f, 	0.f, 	0.f 	},
		{ "H1", 		1.f,   -1.f, 	0.f, 	0.f, 	0.f 	},
		{ "H3L1", 		0.f, 	1.f,   -3.f, 	3.f,   -1.f 	},
		{ "H2L1", 		0.f, 	1.f,   -2.f, 	1.f, 	0.f 	},
		{ "H1L1", 		0.f, 	1.f,   -1.f, 	0.f, 	0.f 	},
		{ "H1L2", 		0.f, 	0.f, 	1.f,   -1.f, 	0.f 	},
		{ "H1L3", 		0.f, 	0.f, 	0.f, 	1.f,   -1.f 	},
		{ "B2", 		0.f, 	1.f,   -1.f, 	0.f, 	0.f 	},
		{ "B4", 		0.f, 	0.f, 	1.f,   -2.f, 	1.f 	},
		{ "A3", 		1.f,   -6.f,   12.f,   -8.f, 	0.f 	},
		{ "A3L1", 		0.f, 	1.f,   -6.f,   12.f,   -8.f 	},
		{ "N2", 		1.f,   -2.f, 	2.f, 	0.f, 	0.f 	},
		{ "N4",			1.f,   -4.f,	8.f,   -8.f,   -2.f 	},
		{ "N2L2",		0.f,	0.f,	1.f,   -2.f,	2.f 	},
		{ "H2N2",		1.f,   -4.f,	7.f,	6.f,  -10.f 	},
		{ "H1N2L1",		0.f,	1.f,   -3.f,	4.f,	2.f 	},
		{ "N2L2T",		0.f,	0.f,	1.f,   -2.f,	1.25f 	},
		{ "N2H1L1T",	0.f,	1.f,   -3.f,	3.25f, -1.25f 	},
		{ "H2N2T",		1.f,   -4.f,   10.f,  -12.f,	5.f 	},
		{ "H1L1N2T",	0.f,	1.f,   -3.f,	7.f,   -5.f 	},
		{ "N2N2AT", 	1.f,   -4.f,   11.f,  -14.f,   10.f		},
		{ "N2N2BT",		1.f,   -4.f,	7.25f, -6.5f,	2.5f 	}
};

const char *Moog_ModeName(unsigned i) {
	if (i > N_MOOG_MODES) return "?";
	return MoogMode_mode[i].name;
}



