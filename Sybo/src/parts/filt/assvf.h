/*
 * ASSVF.h
 *
 *  Created on: 11 Apr 2013
 *      Author: st
 */

#ifndef ASSVF_H_
#define ASSVF_H_

typedef struct {
	float v1, v2;
	float v0z, v1z, v2z;
	int mode;
} ASSVF;

void ASSVF_init(ASSVF *f, int mode);
void ASSVF_process(ASSVF *f, float *samples, int nframes, float *wc, float res);

#endif /* ASSVF_H_ */
