/*
 * moogmode.h
 *
 *  Created on: 10 Apr 2013
 *      Author: st
 */

#ifndef MOOGMODE_H_
#define MOOGMODE_H_

#define	N_MOOG_MODES 28

typedef struct {
	const char *name;
	const float a, b, c, d, e;
} MoogMode;

extern const MoogMode MoogMode_mode[];

const char *Moog_ModeName(unsigned n);

#endif /* MOOGMODE_H_ */
