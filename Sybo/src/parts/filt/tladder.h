/*
 * tladder.h
 *
 *  Created on: 11 Apr 2013
 *      Author: st
 */

#ifndef TLADDER_H_
#define TLADDER_H_

#include "moogmode.h"

typedef struct {
	float z1;
	float s[4];
	float a, b, c, d, e;	// the mode stage weights
} Tladder;

void Tladder_init(Tladder *l, int mode);
void Tladder_process(Tladder *l, float *samples, int nframes,
		float *wc, float resonance);
void Tladder_process0(Tladder *l, float *samples, int nframes,
		float *wc, float resonance);


#endif /* TLADDER_H_ */
