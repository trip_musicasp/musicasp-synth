/*
 * moog.h
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef MOOG_H_
#define MOOG_H_

typedef struct {
	float in1, in2, in3, in4, out1, out2, out3, out4;
} Moog;

void Moog_Setup(float wm);
void Moog_init(Moog *m);
void Moog_process(Moog *m, float *samples, int nframes, float *fc, float res);

#endif /* MOOG_H_ */
