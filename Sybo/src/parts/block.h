/*
 * block.h
 *
 *  Created on: 19 Mar 2013
 *      Author: st
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include <math.h>
#include <stdint.h>

float max_process(float *samples, uint32_t nf);

#endif /* BLOCK_H_ */
