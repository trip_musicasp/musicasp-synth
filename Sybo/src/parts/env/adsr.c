/*
 * adsr.c
 *
 * Exponential and Linear ADSR envelope generators.
 *
 * Typically the exponential version will be used for the amplitude
 * envelope, the exponential decay and release phases will sound linear
 * due us hearing sound levels logarithmically, as if in dB.
 *
 * Filter cutoffs should ideally be exponential to allow simple summing
 * and scaling of modulation in musically relevant semitones. In this case
 * the linear version should be used
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#include "adsr.h"

/*
 * produce nframes envelope samples into the out buffer
 * only change state on a new block for efficiency
 * exponential states never reach target so never overshoot
 */
void ADSR_process(ADSR *adsr, float *out, int nframes,
		float attackCoeff, float decayCoeff,
		float sustainLevel, float releaseCoeff) {
	float envelope = adsr->value;
	ADSR_State state = adsr->state;
	// work out how to cope with this state
	float coeff = 0.f, target = 0.f;
	switch (state) {
		case ATTACK:	coeff = attackCoeff;	target = 1.f;			break;
		case DECAY:		coeff = decayCoeff;		target = sustainLevel;	break;
		case RELEASE:	coeff = releaseCoeff;	target = 0.f;			break;
		case SUSTAIN:	/* we use default values of	zero */				break;
		case COMPLETE:	return;
	}
	int shape = (state == ATTACK) ? ADSR_LIN : ADSR_EXP;
	// process this phase
	switch (shape) {
	case ADSR_LIN: // may overshoot so deal with it, TODO not just for attack
		while (nframes && envelope < (target - coeff)) {
			envelope += coeff;
			*out++ = envelope;
			nframes--;
		}
		if (nframes > 0) {
			envelope = target;
			while (nframes--) {
				*out++ = envelope;
			}
		}
		break;
	case ADSR_EXP: // never reaches target so we can do 'too many' frames
		while (nframes--) {
			envelope += coeff * (target - envelope);
			*out++ = envelope;
		}
		break;
	}
	// work out whether this state transitions to another
	switch (state) {
	case ATTACK:
        if (envelope > 0.99f) {
             state = DECAY;
        }
		break;
	case DECAY:
        if (envelope <= sustainLevel + 0.01f) {
            state = SUSTAIN;
        }
        break;
	case RELEASE:
		if (envelope < 0.0001f) { // -80dB cutoff !!!
			envelope = 0.f;
			state = COMPLETE;
		}
		break;
	case SUSTAIN:
		break;
	case COMPLETE:	// keep compiler happy even though we can't reach here
		return;
	}
	adsr->value = envelope;
	adsr->state = state;
}

