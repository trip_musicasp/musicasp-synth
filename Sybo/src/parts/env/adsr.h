/*
 * adsr.h
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef ADSR_H_
#define ADSR_H_

#include <assertp.h>

typedef enum {
	COMPLETE = 0,
	ATTACK = 1,
	DECAY = 2,
	SUSTAIN = 3,
	RELEASE = 4
} ADSR_State;

typedef enum {
	ADSR_LIN,
	ADSR_EXP
} ADSR_Shape;

typedef struct {
	ADSR_State state;
	float value;
} ADSR ;

#define ADSR_STATE(x) ("0ADSR"[x])

void ADSR_process(ADSR *adsr, float *out, int nframes,
		float attackCoeff, float decayCoeff,
		float sustainLevel, float releaseCoeff);

#endif /* ADSR_H_ */
