/*
 * resample.c
 *
 * High quality decimation and interpolation, 2x, out of place.
 *
 *  Created on: 8 Apr 2013
 *      Author: st
 */

#include "resample.h"

/*
 * http://www.musicdsp.org/archive.php?classid=3#231
 *
 * Thanks to ld0d for recommending this example.
 */
uint32_t HQDecimator_Process(HQDecimator *d, float *output, float *input, uint32_t nframes) {
	float h5y0;
	float h3y0;
	float h1y0;
	float y0;
	float y1 = d->y1;
	float y2 = d->y2;
	float y3 = d->y3;
	float y4 = d->y4;
	float y5 = d->y5;
	float y6;
	nframes >>= 1u;
	uint32_t nf = nframes;
	while (nf--) {
		y0 = *input++;
		h5y0 = (9/692.0f) * y0;
		h3y0 = (-44/692.0f) * y0;
		h1y0 = (208/692.0f) * y0;
		y6 = y5 + h5y0;
		y5 = y4 + h3y0;
		y4 = y3 + h1y0;
		y3 = y2 + h1y0 + *input++ * (346/692.0f);
		y2 = y1 + h3y0;
		y1 = h5y0;
		*output++ = y6;
	}
	d->y1 = y1;
	d->y2 = y2;
	d->y3 = y3;
	d->y4 = y4;
	d->y5 = y5;
	return nframes;
}

/*
 * http://yehar.com/blog/?p=197
 *
 * Thanks to Olli Niemitalo for his paper.
 *
 * Optimal 2x (6-point, 5th-order) (z-form)
 */
uint32_t HQInterpolator_Process(HQInterpolator *i, float *output, float *input, uint32_t nframes) {
	float y0;
	float y1 = i->y1;
	float y2 = i->y2;
	float y3 = i->y3;
	float y4 = i->y4;
	float y5 = i->y5;
	uint32_t nf = nframes;
	while(nf--) {
		y0 = *input++;
		*output++ = (y2+y3) * 0.40513396007145713f +
					(y1+y4) * 0.09251794438424393f +
					(y0+y5) * 0.00234806603570670f;
		*output++ = y2;
		y5 = y4;
		y4 = y3;
		y3 = y2;
		y2 = y1;
		y1 = y0;
	}
	i->y1 = y1;
	i->y2 = y2;
	i->y3 = y3;
	i->y4 = y4;
	i->y5 = y5;
	return nframes << 1u;
}
