/*
 * block.c
 *
 *  Created on: 19 Mar 2013
 *      Author: st
 */

#include "block.h"

float max_process(float *samples, uint32_t nf) {
	float max = 0.f;
	uint32_t nb = nf;
	while (nb > 0u) {
		max = fmaxf(max, fabs(*samples++));
		nb--;
	}
	return max;
}


