/*
 * unrolled.h
 *
 * Out of place vector and constant operations on vectors
 *
 *  Created on: 18 Feb 2013
 *      Author: st
 */

#ifndef UNROLLED_H_
#define UNROLLED_H_

#include <stdint.h>
#include "convert.h"

// d = s1 * s2
void Unrolled_mult(float *d, float *s1, float *s2, uint32_t nf);

// d = s1 + s2
void Unrolled_add(float *d, float *s1, float *s2, uint32_t nf);

// d = s1 - s2
void Unrolled_sub(float *d, float *s1, float *s2, uint32_t nf);

// d = s1 * K
void Unrolled_scale(float *d, float *s, float K, uint32_t nf);

// d = s1 + K
void Unrolled_offset(float *d, float *s, float K, uint32_t nf);

// d = s1 + s2 * K
void Unrolled_scale_add(float *d, float *s1, float *s2, float K, uint32_t nf);

// d = s1 * s2 * K
void Unrolled_scale_mult(float *d, float *s1, float *s2, float K, uint32_t nf);

// d = |s1|
void Unrolled_abs(float *d, float *s, uint32_t nf);

// d = K
void Unrolled_fill(float *d, float K, uint32_t nf);

// d = s
void Unrolled_copy(float *d, float *s, uint32_t nf);

// d = 2^s
void Unrolled_pow2(float *d, float *s, uint32_t nf);

void Unrolled_S16toF32(float *d, int16_t *s, uint32_t nf);
void Unrolled_F32toS16(int16_t *d, float *s, uint32_t nf);

// d = s < 0 ? 0 : s > 1 ? 1 : s
void Unrolled_clamp(float *d, float *s, uint32_t nf);

#endif /* UNROLLED_H_ */
