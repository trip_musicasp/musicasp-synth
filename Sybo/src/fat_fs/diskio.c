/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2013        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "sd.h"			/* SD/MMC/SDC control */
#include <usb_core.h>   /* USB */
#include <usbh_core.h>
#include <usbh_msc_scsi.h>
#include <usbh_msc_bot.h>
#include <stm32f4xx_rtc.h>

/* Definitions of physical drive number for each media */
#define SD		0
#define USB		1

static volatile DSTATUS StatSD = STA_NOINIT; /* USB Disk status */

#if USE_USBH_MSC
static volatile DSTATUS StatUSB = STA_NOINIT; /* USB Disk status */

extern USB_OTG_CORE_HANDLE USB_OTG_host;
extern USBH_HOST USB_Host;
#endif

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv /* Physical drive nmuber (0..) */) {
	switch (pdrv) {
	case SD: {
		SD_Error state = SD_Init();
		if (SD_OK == state) {
			StatSD &= ~STA_NOINIT;
		}
		return StatSD;
	}
#if USE_USBH_MSC
	case USB:
		if (HCD_IsDeviceConnected(&USB_OTG_host)) {
			StatUSB &= ~STA_NOINIT;
		}
		return StatUSB;
#endif
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv /* Physical drive nmuber (0..) */) {
	SDCardState state;

	switch (pdrv) {
	case SD:
		state = SD_GetState();
		if (SD_CARD_ERROR == state)
			return STA_NOINIT;
		if (SD_CARD_DISCONNECTED == state)
			return STA_NOINIT;
		return RES_OK;

#if USE_USBH_MSC
	case USB:
		return StatUSB;
#endif
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv,    /* Physical drive nmuber (0..) */
				  BYTE *buff,   /* Data buffer to store read data */
				  DWORD sector, /* Sector address (LBA) */
				  BYTE count    /* Number of sectors to read (1..128) */
) {
#if USE_USBH_MSC
	BYTE status = USBH_MSC_OK;
#endif

	if (!count)
		return RES_PARERR;

	switch (pdrv) {
	case SD:
		if (StatSD & STA_NOINIT) return RES_NOTRDY;
		if (SD_OK != SD_ReadMultiBlocks(buff, ((uint64_t) sector) << 9, 512, count)) {
			return RES_ERROR;
		}
		if (SD_OK != SD_WaitReadOperation()) {
			return RES_ERROR;
		}
		return (SD_TRANSFER_OK == SD_GetStatus()) ? RES_OK : RES_ERROR;
#if USE_USBH_MSC
	case USB:
		if (StatUSB & STA_NOINIT) return RES_NOTRDY;

		if (HCD_IsDeviceConnected(&USB_OTG_host)) {
			do {
				status = USBH_MSC_Read10(&USB_OTG_host, buff, sector,
						512 * count);
				USBH_MSC_HandleBOTXfer(&USB_OTG_host, &USB_Host);

				if (!HCD_IsDeviceConnected(&USB_OTG_host)) {
					return RES_ERROR;
				}
			} while (status == USBH_MSC_BUSY);
		}

		if (status == USBH_MSC_OK)
			return RES_OK;
		return RES_ERROR;
#endif
	}
	return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write(BYTE pdrv, 		 /* Physical drive nmuber (0..) */
				   const BYTE *buff, /* Data to be written */
				   DWORD sector, 	 /* Sector address (LBA) */
				   BYTE count 		 /* Number of sectors to write (1..128) */
) {
#if USE_USBH_MSC
	BYTE status = USBH_MSC_OK;
#endif

	if (!count)
		return RES_PARERR;

	switch (pdrv) {
	case SD:
		if (StatSD & STA_NOINIT) return RES_NOTRDY;
		if (StatSD & STA_PROTECT) return RES_WRPRT;

		if (SD_OK != SD_WriteMultiBlocks((uint8_t *) buff, ((uint64_t) sector) << 9, 512, count))
			return RES_ERROR;
		if (SD_OK != SD_WaitWriteOperation())
			return RES_ERROR;
		while (SD_GetStatus() == SD_TRANSFER_BUSY);
		return (SD_TRANSFER_OK == SD_GetStatus()) ? RES_OK : RES_ERROR;

#if USE_USBH_MSC
	case USB:
		if (StatUSB & STA_NOINIT) return RES_NOTRDY;
		if (StatUSB & STA_PROTECT) return RES_WRPRT;

		if (HCD_IsDeviceConnected(&USB_OTG_host)) {
			do {
				status = USBH_MSC_Write10(&USB_OTG_host, (BYTE*) buff, sector,
						512 * count);
				USBH_MSC_HandleBOTXfer(&USB_OTG_host, &USB_Host);

				if (!HCD_IsDeviceConnected(&USB_OTG_host)) {
					return RES_ERROR;
				}
			}
			while (status == USBH_MSC_BUSY);
		}

		if (status == USBH_MSC_OK)
			return RES_OK;
		return RES_ERROR;
#endif
	}
	return RES_PARERR;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(BYTE pdrv, /* Physical drive nmuber (0..) */
				   BYTE cmd, /* Control code */
				   void *buff /* Buffer to send/receive control data */) {
	SD_CardInfo cardinfo;

	switch (pdrv) {
	case SD:
		if (StatSD & STA_NOINIT) return RES_NOTRDY;

		switch (cmd) {
		case CTRL_SYNC:
			break;
		case GET_SECTOR_SIZE:
			*(WORD *) buff = 512;
			break;
		case GET_SECTOR_COUNT:
			if (SD_OK != SD_GetCardInfo(&cardinfo))
				return RES_PARERR;
			*(DWORD *) buff = cardinfo.CardCapacity / 512;
			break;
		case GET_BLOCK_SIZE:
			if (SD_OK != SD_GetCardInfo(&cardinfo))
				return RES_PARERR;
			*(DWORD *) buff = cardinfo.CardBlockSize;
			break;
		default:
			return RES_PARERR;
		}
		return RES_OK;

#if USE_USBH_MSC
	case USB:
		if (StatUSB & STA_NOINIT) return RES_NOTRDY;

		switch (cmd) {
		case CTRL_SYNC: /* Make sure that no pending write process */
			break;
		case GET_SECTOR_SIZE: /* Get R/W sector size (WORD) */
			*(WORD*) buff = 512;
			break;
		case GET_SECTOR_COUNT: /* Get number of sectors on the disk (DWORD) */
			*(DWORD*) buff = (DWORD) USBH_MSC_Param.MSCapacity;
			break;
		case GET_BLOCK_SIZE: /* Get erase block size in unit of sector (DWORD) */
			*(DWORD*) buff = 512;
			break;
		default:
			return RES_PARERR;
		}
		return RES_OK;
#endif
	}
	return RES_PARERR;
}
#endif

/*-----------------------------------------------------------------------*/
/* Get current time                                                      */
/*-----------------------------------------------------------------------*/

DWORD get_fattime (void)
{
	RTC_DateTypeDef d;
	RTC_TimeTypeDef t;
	RTC_GetDate(RTC_Format_BIN, &d);
	RTC_GetTime(RTC_Format_BIN, &t);

	return	((20+d.RTC_Year) << 25) 	// Year since 1980
			| (d.RTC_Month << 21)
			| (d.RTC_Date << 16)
			| (t.RTC_Hours << 11)
			| (t.RTC_Minutes << 5)
			| (t.RTC_Seconds >> 1)
			;
}


