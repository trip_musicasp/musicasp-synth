/*
 * audio_codec.h
 *
 * Define Audio CODEC, I2S, DMA and IRQ allocation
 */

#ifndef __AUDIOCODEC_H
#define __AUDIOCODEC_H

#include <audio.h>
#include <stm32f4xx.h>
#include <arm_math.h>
#include <core_cmFunc.h>
#include <misc.h>
#include <stm32f4xx_dma.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_gpio.h>
#include <stdio.h>


// ---- Hardware Configuration defines parameters -----------------------------
// we USE I2S3 (aka SPI3) for SD(out) (PB5), SCK (PB3), WS (PA4) and MCK (PC7)
// we use I2S3ext for SD(in) (PB4)

// I2S peripheral configuration defines
#define CODEC_I2S                   SPI3
#define CODEC_I2S_EXT               I2S3ext
#define CODEC_I2S_CLK               RCC_APB1Periph_SPI3
#define CODEC_I2S_OUT_DR	      	&(((SPI_TypeDef *)SPI3_BASE)->DR)
#define CODEC_I2S_IN_DR        		&(((SPI_TypeDef *)I2S3ext_BASE)->DR)
#define CODEC_I2S_WS_PIN            GPIO_Pin_4
#define CODEC_I2S_SCK_PIN           GPIO_Pin_3
#define CODEC_I2S_SD_OUT_PIN        GPIO_Pin_5
#define CODEC_I2S_SD_IN_PIN			GPIO_Pin_4
#define CODEC_I2S_MCK_PIN           GPIO_Pin_7
#define CODEC_I2S_WS_PINSRC         GPIO_PinSource4
#define CODEC_I2S_SCK_PINSRC        GPIO_PinSource3
#define CODEC_I2S_SD_OUT_PINSRC     GPIO_PinSource5
#define CODEC_I2S_SD_IN_PINSRC      GPIO_PinSource4
#define CODEC_I2S_MCK_PINSRC        GPIO_PinSource7
#define CODEC_I2S_SD_SCK_GPIO       GPIOB
#define CODEC_I2S_MCK_GPIO          GPIOC
#define CODEC_I2S_WS_GPIO			GPIOA
#define CODEC_I2S_GPIO_CLOCK        (RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOB|RCC_AHB1Periph_GPIOC)

// direct codec control lines
#define CODEC_CONTROL_PDN_PIN		GPIO_Pin_7
#define CODEC_CONTROL_HPF_PIN		GPIO_Pin_6
#define CODEC_CONTROL_CKS0_PIN		GPIO_Pin_4
#define CODEC_CONTROL_MASTER_PIN	GPIO_Pin_3
#define CODEC_CONTROL_GPIO			GPIOD
#define CODEC_CONTROL_GPIO_CLOCK    RCC_AHB1Periph_GPIOD

// I2S DMA Stream definitions
#define AUDIO_DMA_CLOCK            	RCC_AHB1Periph_DMA1
#define AUDIO_DMA_PERIPH_DATA_SIZE 	DMA_PeripheralDataSize_HalfWord
#define AUDIO_DMA_MEM_DATA_SIZE    	DMA_MemoryDataSize_Word

#define AUDIO_OUT_IRQHandler		DMA1_Stream5_IRQHandler
#define AUDIO_OUT_DMA_IRQ           DMA1_Stream5_IRQn
#define AUDIO_OUT_DMA_STREAM        DMA1_Stream5
#define AUDIO_OUT_DMA_CHANNEL       DMA_Channel_0
#define AUDIO_OUT_DMA_FLAG_TC       DMA_FLAG_TCIF5
#define AUDIO_OUT_DMA_FLAG_HT       DMA_FLAG_HTIF5
#define AUDIO_OUT_DMA_FLAG_FE       DMA_FLAG_FEIF5
#define AUDIO_OUT_DMA_FLAG_TE       DMA_FLAG_TEIF5
#define AUDIO_OUT_DMA_FLAG_DME      DMA_FLAG_DMEIF5

#define AUDIO_IN_IRQHandler         DMA1_Stream2_IRQHandler
#define AUDIO_IN_DMA_IRQ            DMA1_Stream2_IRQn
#define AUDIO_IN_DMA_STREAM 		DMA1_Stream2
#define AUDIO_IN_DMA_CHANNEL        DMA_Channel_2
#define AUDIO_IN_DMA_FLAG_TC       	DMA_FLAG_TCIF2
#define AUDIO_IN_DMA_FLAG_HT       	DMA_FLAG_HTIF2
#define AUDIO_IN_DMA_FLAG_FE       	DMA_FLAG_FEIF2
#define AUDIO_IN_DMA_FLAG_TE       	DMA_FLAG_TEIF2
#define AUDIO_IN_DMA_FLAG_DME      	DMA_FLAG_DMEIF2

#define AUDIO_SPI_IRQHandler		SPI3_IRQHandler
#define AUDIO_SPI_IRQ				SPI3_IRQn

#define AUDIO_IRQ_ERROR_HANDLING	1

typedef struct {
	uint32_t nDMATC;
#if AUDIO_IRQ_ERROR_HANDLING
	uint32_t nDMATE;
	uint32_t nDMAFE;
#endif
} AudioDMAIrqData;

typedef struct {
#if AUDIO_IRQ_ERROR_HANDLING
	uint32_t nI2SOVR;
	uint32_t nI2SUDR;
	uint32_t nI2STIFRFE;
#endif
} AudioI2SIrqData;

#endif /* __AUDIOCODEC_H */

