#include "adc.h"
#include <stm32f4xx_adc.h>
#include <stm32f4xx_dma.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <misc.h>
#include <stdio.h>

static volatile uint16_t convertedValue[N_ANALOG_IN];
#if USE_ANALOG_IN
extern void AnalogIn_Callback(float *raw, int nain);

static float rawValue[N_ANALOG_IN];
static float newness = 0.1f;
static const float RAW_FACTOR = 1.f/4096;
#endif
static int nAin = 0;

#if USE_PERF
static uint32_t nIRQ;		// ADC IRQ
static uint32_t nOVR;		// ADC overrun count
#if USE_ADC_DMA
static uint32_t nDMA_TC;	// DMA transfer complete count
static uint32_t nDMA_TE;	// DMA transfer error count
static uint32_t nDMA_FE;	// DMA FIFO error count
#endif // USE_ADC_DMA
#endif

#if USE_ADC_DMA
static 	DMA_InitTypeDef DMA_InitStructure;
#else
static int adcChannel;
#endif

/**
 * Initialise analog inputs using ADC1 with DMA configuration
 *  - Enable peripheral clocks
 *  - DMA Stream and channel configuration
 *  - Configure ADC pins as analog input
 *  - Configure ADC Channels
 */
void AnalogIn_Init(AnalogInput *ain, int nain) {
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	if (nain > N_ANALOG_IN) nain = N_ANALOG_IN;
	nAin = nain;

	// Enable ADC1, DMA and GPIO clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); // TODO
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE); // only if
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); // used
#if USE_ADC_DMA
	RCC_AHB1PeriphClockCmd(ADC_DMA_CLOCK, ENABLE);

	// DMA Stream and channel configuration
	DMA_InitStructure.DMA_Channel = ADC_DMA_CHANNEL;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &ADC1->DR;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &convertedValue[0];
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = nAin;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(ADC_DMA_STREAM, &DMA_InitStructure);

	// Enable the selected DMA interrupts
	DMA_ITConfig(ADC_DMA_STREAM, DMA_IT_TC, ENABLE);

	// I2S DMA IRQ Channel configuration
	NVIC_InitStructure.NVIC_IRQChannel = ADC_DMA_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

	// Configure ADC1 pins as analog input
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	int i;
	for (i = 0; i < nAin; i++) {
		GPIO_InitStructure.GPIO_Pin = AIN_PIN(ain[i]);
		GPIO_Init(AIN_PORT(ain[i]), &GPIO_InitStructure);
	}

	// ADC Common Init
	ADC_CommonStructInit(&ADC_CommonInitStructure);
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInit(&ADC_CommonInitStructure);

	// ADC1 Init
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = nAin;
	ADC_Init(ADC1, &ADC_InitStructure);

	// ADC1 regular channel configuration
	for (i = 0; i < nAin; i++) {
		ADC_RegularChannelConfig(ADC1, AIN_AIN(ain[i]), i + 1,
				ADC_SampleTime_15Cycles);
	}

	// enable ADC overrun interrupt
	ADC_ITConfig(ADC1, ADC_IT_OVR, ENABLE);
#if !USE_ADC_DMA
	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);
	ADC_EOCOnEachRegularChannelCmd(ADC1, ENABLE);
#endif

	// ADC IRQ Channel configuration
	NVIC_InitStructure.NVIC_IRQChannel = ADC_IRQn;
#if USE_ADC_DMA
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
#else
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
#endif
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

#if USE_ADC_DMA
	DMA_Cmd(ADC_DMA_STREAM, ENABLE);

	/* Disable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC1, DISABLE);

	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);
#endif

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
}

/*
 * This function should typically be called in the Audio ISR such that
 * it does its DMA while the Audio is not doing DMA.
 * Our ISR will then be delayed until the Audio ISR returns but we will
 * be ideally synced to it, so not too many or too few scans.
 */
void AnalogIn_StartScan(void) {
#if !USE_ADC_DMA
	adcChannel = 0;
#endif
	ADC_SoftwareStartConv(ADC1);
}

static inline void average(int i) {
	rawValue[i] = newness * RAW_FACTOR * (float)convertedValue[i] +
			(1.f - newness) * rawValue[i];
}

// override the weak ISR
#if USE_ANALOG_IN
#if USE_ADC_DMA
void ADC_DMA_IRQHandler(void) {
	// Transfer complete interrupt
	if (DMA_GetFlagStatus(ADC_DMA_STREAM, ADC_DMA_FLAG_TC) != RESET) {
#if USE_PERF
		nDMA_TC++;
#endif
		int i;
		for (i = 0; i < nAin; i++) {
			average(i);
		}
		AnalogIn_Callback(rawValue, nAin);
		// Clear the Interrupt flag
		DMA_ClearFlag(ADC_DMA_STREAM, ADC_DMA_FLAG_TC);
		/*
		 * At the end of the last DMA transfer (number of transfers configured
		 * in the DMA controller’s DMA_SxRTR register):
 	 	 * No new DMA request is issued to the DMA controller if the DDS bit
 	 	 * is cleared to 0 in the ADC_CR2 register (this avoids generating an
 	 	 * overrun error). However the DMA bit is not cleared by hardware.
 	 	 * It must be written to 0, then to 1 to start a new transfer.
		 * tech ref 11.8.1 p.274 ?????
		 */
		ADC_DMACmd(ADC1, DISABLE);
		ADC_DMACmd(ADC1, ENABLE);
	}
	if (DMA_GetFlagStatus(ADC_DMA_STREAM, ADC_DMA_FLAG_TE) != RESET) {
#if USE_PERF
		nDMA_TE++;
#endif
		DMA_ClearFlag(ADC_DMA_STREAM, ADC_DMA_FLAG_TE);
	}
	if (DMA_GetFlagStatus(ADC_DMA_STREAM, ADC_DMA_FLAG_FE) != RESET) {
#if USE_PERF
		nDMA_FE++;
#endif
		DMA_ClearFlag(ADC_DMA_STREAM, ADC_DMA_FLAG_FE);
	}
}
#endif // USE_ADC_DMA

void ADC_IRQHandler(void) {
#if USE_PERF
		nIRQ++;
#endif
	if (ADC_GetITStatus(ADC1, ADC_IT_OVR) != RESET) {
#if USE_PERF
		nOVR++;
#endif
		/*
		 * To recover the ADC from OVR state when the DMA is used, follow the
		 * steps below:
		 * 1. Reinitialize the DMA (adjust destination address and NDTR counter)
		 * 2. Clear the ADC OVR bit in ADC_SR register
		 * 3. Trigger the ADC to start the conversion.
		 */
#if USE_ADC_DMA
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &convertedValue[0];
		DMA_InitStructure.DMA_BufferSize = nAin;
		DMA_Init(ADC_DMA_STREAM, &DMA_InitStructure);
#endif
		ADC_ClearITPendingBit(ADC1, ADC_IT_OVR);
	}

#if !USE_ADC_DMA
	if (ADC_GetITStatus(ADC1, ADC_IT_EOC) != RESET) {
		convertedValue[adcChannel] = ADC1->DR;
		average(adcChannel);
		if (++adcChannel == nAin) {
			AnalogIn_Callback(rawValue, nAin);
			adcChannel = 0; // just to be safe
		}
		ADC_ClearITPendingBit(ADC1, ADC_IT_EOC);
	}
#endif
}

void ADC_printData(void) {
#if USE_PERF
	printf("ADC: %u interrupts, %u overruns\n",	(unsigned)nIRQ, (unsigned)nOVR);
#if USE_ADC_DMA
	printf("ADC: %u completions, %u transfer errors, %u FIFO errors\n",
			(unsigned)nDMA_TC, (unsigned)nDMA_TE, (unsigned)nDMA_TE);
#endif
#endif // USE_PERF
/*	printf("ADC: SR=%X, CR1=%X, CR2=%X, SQR1=%X, SQR2=%X, SQR3=%X\n",
			(unsigned)ADC1->SR, (unsigned)ADC1->CR1, (unsigned)ADC1->CR2,
			(unsigned)ADC1->SQR1, (unsigned)ADC1->SQR2, (unsigned)ADC1->SQR3); */
	printf("ADC: 12bit: %d, %d, %d, %d\n",
			convertedValue[0], convertedValue[1],
			convertedValue[2], convertedValue[3]);
	printf("ADC: raw: %.3f, %.3f, %.3f, %.3f\n",
			rawValue[0], rawValue[1],
			rawValue[2], rawValue[3]);
}
#endif // USE_ANALOG_IN
