/**
 ******************************************************************************
 * @file    usbh_usr.c
 * @author  MCD Application Team
 * @version V1.3.0
 * @date    14-January-2013
 * @brief   This file includes the usb host library user callbacks
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "usbh_usr.h"
#include "ff.h"
#include "usbh_msc_core.h"
#include "usbh_msc_scsi.h"
#include "usbh_msc_bot.h"
#include "ff.h"
#include <stdio.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/*  Points to the DEVICE_PROP structure of current device */
/*  The purpose of this register is to speed up the execution */

USBH_Usr_cb_TypeDef USBH_USR_cb = {
		USBH_USR_Init,
		USBH_USR_DeInit,
		USBH_USR_DeviceAttached,
		USBH_USR_ResetDevice,
		USBH_USR_DeviceDisconnected,
		USBH_USR_OverCurrentDetected,
		USBH_USR_DeviceSpeedDetected,
		USBH_USR_Device_DescAvailable,
		USBH_USR_DeviceAddressAssigned,
		USBH_USR_Configuration_DescAvailable,
		USBH_USR_Manufacturer_String,
		USBH_USR_Product_String,
		USBH_USR_SerialNum_String,
		USBH_USR_EnumerationDone,
		USBH_USR_UserInput,
		USBH_USR_MSC_Application,
		USBH_USR_DeviceNotSupported,
		USBH_USR_UnrecoveredError
};

FATFS USBH_fatfs;
uint8_t USB_Host_Application_Ready;

int silent = 0;

/*--------------- Messages ---------------*/
const char MSG_HOST_INIT[] = "[USBH] Host Initialized\n";
const char MSG_DEV_ATTACHED[] = "[USBH] Device Attached\n";
const char MSG_DEV_DISCONNECTED[] = "[USBH] Device Disconnected\n";
const char MSG_DEV_ENUMERATED[] = "[USBH] Enumeration completed\n";
const char MSG_DEV_HIGHSPEED[] = "[USBH] High speed device detected\n";
const char MSG_DEV_FULLSPEED[] = "[USBH] Full speed device detected\n";
const char MSG_DEV_LOWSPEED[] = "[USBH] Low speed device detected\n";
const char MSG_DEV_ERROR[] = "[USBH] Device fault\n";
const char MSG_DEV_NOT_SUPPORTED[] = "[USBH] Device not supported\n";
const char MSG_USER_INPUT[] = "[USBH] User Input\n";
const char MSG_MSC_CLASS[] = "[USBH] Mass storage device connected\n";
const char MSG_HID_CLASS[] = "[USBH] HID device connected\n";
const char MSG_DISK_SIZE[] = "[USBH] Size of the disk in MBytes: ";
//const char MSG_LUN[] = "[USBH] LUN Available in the device: ";
//const char MSG_ROOT_CONT[] = "[USBH] Exploring disk flash ...";
//const char MSG_WR_PROTECT[] = "[USBH] The disk is write protected\n";
const char MSG_UNREC_ERROR[] = "[USBH] UNRECOVERED ERROR STATE ";
const char MSG_OVERCURRENT[] = "[USBH] Overcurrent detected!\n";
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Add the host lib initialization message to the console
 * @param  None
 * @retval None
 */
void USBH_USR_Init(void) {
	printf(MSG_HOST_INIT);
	USB_Host_Application_Ready = 0;
	silent = 0;
}

/**
 * @brief  De-init User state and associated variables
 * @param  None
 * @retval None
 */
void USBH_USR_DeInit(void) {
	if (USB_Host_Application_Ready == 1) {
		USB_Host_Application_Ready = 0;
		f_mount(1, NULL);
		printf("[FS] FS on drive 1 unmounted\n");
	}
}

/**
 * @brief  Add the device attachement message to the console
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAttached(void) {
	if (silent) return;
	printf(MSG_DEV_ATTACHED);
}

/**
 * @brief  Add the unrecovered error message to the console
 * @param  None
 * @retval None
 */
void USBH_USR_UnrecoveredError(void) {
	if (silent) {
		printf(".");
		return;
	}
	printf(MSG_UNREC_ERROR);
	silent = 1;
}

/**
 * @brief Add the device disconnection message to the console and free 
 *        USB associated resources
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceDisconnected(void) {
	silent = 0;
	printf(MSG_DEV_DISCONNECTED);
}

/**
 * @brief  callback of the device reset event
 * @param  None
 * @retval None
 */
void USBH_USR_ResetDevice(void) {
}

/**
 * @brief  Add the device speed message to the console
 * @param  Device speed
 * @retval None
 */
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed) {
	if (silent) return;
	if (DeviceSpeed == HPRT0_PRTSPD_HIGH_SPEED) {
		printf(MSG_DEV_HIGHSPEED);
	} else if (DeviceSpeed == HPRT0_PRTSPD_FULL_SPEED) {
		printf(MSG_DEV_FULLSPEED);
	} else if (DeviceSpeed == HPRT0_PRTSPD_LOW_SPEED) {
		printf(MSG_DEV_LOWSPEED);
	} else {
		printf(MSG_DEV_ERROR);
	}
}

/**
 * @brief  Add the USB device vendor and MFC Ids to the console
 * @param  device descriptor
 * @retval None
 */
void USBH_USR_Device_DescAvailable(void *DeviceDesc) {
	USBH_DevDesc_TypeDef *hs = DeviceDesc;

	if (silent) return;
	printf("[USBH] VID: %04Xh, PID: %04Xh\n",
			(int) ((*hs).idVendor), (int) ((*hs).idProduct));
}

/**
 * @brief  Device addressed event callbacak
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAddressAssigned(void) {
}

/**
 * @brief  Add the device class description to the console
 * @param  Configuration descriptor
 * @retval None
 */
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
		USBH_InterfaceDesc_TypeDef *itfDesc, USBH_EpDesc_TypeDef *epDesc) {
	USBH_InterfaceDesc_TypeDef *id;

	id = itfDesc;

	if (silent) return;
	if ((*id).bInterfaceClass == 0x08) {
		printf(MSG_MSC_CLASS);
	} else if ((*id).bInterfaceClass == 0x03) {
		printf(MSG_HID_CLASS);
	}
}

/**
 * @brief  Add the MFC String to the console
 * @param  Manufacturer String
 * @retval None
 */
void USBH_USR_Manufacturer_String(void *ManufacturerString) {
	if (silent) return;
	printf("[USBH] Manufacturer : %s\n", (char *) ManufacturerString);
}

/**
 * @brief  Add the Product String to the console
 * @param  Product String
 * @retval None
 */
void USBH_USR_Product_String(void *ProductString) {
	if (silent) return;
	printf("[USBH] Product : %s\n", (char *) ProductString);
}

/**
 * @brief  Add the Srial Number String to the console
 * @param  SerialNum_String
 * @retval None
 */
void USBH_USR_SerialNum_String(void *SerialNumString) {
	if (silent) return;
	printf("[USBH] Serial Number : %s\n", (char *) SerialNumString);
}

/**
 * @brief  Enumeration complete event callback
 * @param  None
 * @retval None
 */
void USBH_USR_EnumerationDone(void) {
	if (silent) return;
	printf(MSG_DEV_ENUMERATED);
}

/**
 * @brief  Device is not supported callback
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceNotSupported(void) {
	printf(MSG_DEV_NOT_SUPPORTED);
}

/**
 * @brief  User Action for application state entry callback
 * @param  None
 * @retval USBH_USR_Status : User response for key button
 */
USBH_USR_Status USBH_USR_UserInput(void) {
//	if (!silent) printf(MSG_USER_INPUT);
	return USBH_USR_RESP_OK;
}

/**
 * @brief  Over Current Detected on VBUS
 * @param  None
 * @retval Staus
 */
void USBH_USR_OverCurrentDetected(void) {
	printf(MSG_OVERCURRENT);
}

/**
 * @brief  Mass storage application main handler
 * @param  None
 * @retval Staus
 */
int USBH_USR_MSC_Application(void) {
	if (USB_Host_Application_Ready == 0) {
		if (silent) {
			printf("\n");
			silent = 0;
		}
		/* Initializes the File System*/
		if (f_mount(1, &USBH_fatfs) != FR_OK) {
			printf("[FS] Cannot mount FS on drive 1\n");
			USB_Host_Application_Ready = 1;
			return -1;
		}
		printf("[FS] FS on drive 1 mounted\n");
		printf("%s %d\n", MSG_DISK_SIZE,
				(int) ((USBH_MSC_Param.MSCapacity * USBH_MSC_Param.MSPageLength)
						/ 1024 / 1024));

		USB_Host_Application_Ready = 1;
	}
	return (0);
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
