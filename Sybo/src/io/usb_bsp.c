/*
 * usb_bsp.c
 *
 *  Created on: 2 Mar 2013
 *      Author: st
 */
#include "usb_bsp.h"
#include "usbd_conf.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "misc.h"
#include <stdio.h>

void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev) {

}

void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev, uint8_t state) {

}

/**
 * Initialise BSP configuration for the selected USB core
 */
void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev) {
	GPIO_InitTypeDef GPIO_InitStructure;
	USB_OTG_CORE_ID_TypeDef coreID = pdev->cfg.coreID;

	if (coreID == USB_OTG_FS_CORE_ID) {
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		// Configure VBUS, DM and DP Pins
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_12;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_OTG1_FS);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_OTG1_FS);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_OTG1_FS);
		RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_OTG_FS, ENABLE);
	} else if (coreID == USB_OTG_HS_CORE_ID) {
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		// Configure DM and DP pins
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_OTG2_FS);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_OTG2_FS);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_OTG_HS, ENABLE);
	} else {
		printf("USB_OTG_BSP_Init(): invalid pdev->cfg.coreID %d\n", coreID);
		return;
	}
}

/**
 * Enable USB Global interrupt for the selected USB core
 */
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev) {
	NVIC_InitTypeDef NVIC_InitStructure;
	USB_OTG_CORE_ID_TypeDef coreID = pdev->cfg.coreID;

	if (coreID == USB_OTG_FS_CORE_ID) {
		NVIC_InitStructure.NVIC_IRQChannel = OTG_FS_IRQn;
	} else if (coreID == USB_OTG_HS_CORE_ID) {
		NVIC_InitStructure.NVIC_IRQChannel = OTG_HS_IRQn;
	} else {
		printf("USB_OTG_BSP_EnableInterrupt(): invalid pdev->cfg.coreID %d\n", coreID);
		return;
	}
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief  USB_OTG_BSP_uDelay
 *         This function provides delay time in micro sec
 * @param  usec : Value of delay required in micro sec
 * @retval None
 */
void USB_OTG_BSP_uDelay(const uint32_t usec) {
	uint32_t count = 0;
	const uint32_t utime = (120 * usec / 7);
	do {
		if (++count > utime) {
			return;
		}
	} while (1);
}

/**
 * @brief  USB_OTG_BSP_mDelay
 *          This function provides delay time in milli sec
 * @param  msec : Value of delay required in milli sec
 * @retval None
 */
void USB_OTG_BSP_mDelay(const uint32_t msec) {
	USB_OTG_BSP_uDelay(msec * 1000);
}
