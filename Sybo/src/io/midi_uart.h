/*
 * midi_uart.h
 *
 *  Created on: 9 Feb 2013
 *      Author: st
 */

#ifndef MIDI_UART_H_
#define MIDI_UART_H_

#include <stm32f4xx_usart.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <misc.h>
#include "midi.h"
#include "synch.h"
#include <stdio.h>
#include <seq_clock.h>

#define USARTx_IRQHANDLER   		USART3_IRQHandler

#define MIDI_USART                 	USART3
#define MIDI_IRQn                  	USART3_IRQn
#define MIDI_CLK                   	RCC_APB1Periph_USART3
#define MIDI_TX_PIN                	GPIO_Pin_8
#define MIDI_TX_GPIO_PORT          	GPIOD
#define MIDI_TX_GPIO_CLK           	RCC_AHB1Periph_GPIOD
#define MIDI_TX_SOURCE             	GPIO_PinSource8
#define MIDI_TX_AF                 	GPIO_AF_USART3
#define MIDI_RX_PIN                	GPIO_Pin_9
#define MIDI_RX_GPIO_PORT          	GPIOD
#define MIDI_RX_GPIO_CLK           	RCC_AHB1Periph_GPIOD
#define MIDI_RX_SOURCE             	GPIO_PinSource9
#define MIDI_RX_AF                 	GPIO_AF_USART3

void Midi_UART_Init(void);
void Midi_OutputTickle(void);
void Midi_ThruEnable(int enable);

#if USE_PERF
void Midi_PrintData(void);

typedef struct {
	uint32_t nInBytes;
	uint32_t nThruBytes;
	uint32_t nInRTMsgs;				// real time
	uint32_t nInCVMsgs;				// channel voice
	uint32_t nInSCMsgs;				// system common
	uint32_t nInRSMsgs;				// running status
	uint32_t nInQPMsgs;				// produced to Queue (excluding RT)
	uint32_t chan;
} MidiPerformanceData;

#endif

#endif /* MIDI_UART_H_ */
