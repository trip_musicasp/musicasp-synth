/*
 * ser_uart.h
 *
 *  Created on: 11 Mar 2013
 *      Author: st
 */

#ifndef SER_UART_H_
#define SER_UART_H_

#include <stdint.h>

void SerUart_Init(void);
int SerUart_haschar(void);
int SerUart_getchar(uint8_t *buf);
int SerUart_putchar(uint8_t ch);
void SerUart_send_buffer(uint8_t *ptr, int len);
void SerUart_print(char *ptr);

#define DEBUG_USART 		USART1
#define USART_CLOCK 		RCC_APB2Periph_USART1
#define USART_AF			GPIO_AF_USART1

#define USART_GPIO			GPIOB
#define USART_GPIO_CLOCK	RCC_AHB1Periph_GPIOB
#define USART_TX_PIN		GPIO_Pin_6
#define USART_TX_PINSRC		GPIO_PinSource6
#define USART_RX_PIN		GPIO_Pin_7
#define USART_RX_PINSRC		GPIO_PinSource7

#endif /* SER_UART_H_ */
