/*
 * seq_clock.h
 *
 *  Created on: 21 Mar 2013
 *      Author: st
 */

#ifndef SEQ_CLOCK_H_
#define SEQ_CLOCK_H_

#include <debug_led.h>

#define SEQ_CLOCK_TIMER 		TIM3
#define SEQ_CLOCK_TIMER_RCC 	RCC_APB1Periph_TIM3
#define SEQ_CLOCK_IRQ			TIM3_IRQn
#define SEQ_CLOCK_IRQ_HANDLER	TIM3_IRQHandler

#define SEQ_CLOCK_PPQN 			48

void SeqClock_Init(void);

void SeqClock_setBPM(float bpm);
float SeqClock_getBPM(void);

void SeqClock_setExternalClock(int state);
int SeqClock_isExternalClock(void);

void SeqClock_ExternalTick(void);

int SeqClock_HandleTick(void);

#endif /* SEQ_CLOCK_H_ */
