/*
 * usbh_msc.c
 *
 *  Created on: 2 Mar 2013
 *      Author: st
 */

#include <usbh_msc_core.h>
#include <usbh_msc_bot.h>
#include <usb_hcd_int.h>
#include <stdio.h>

extern USBH_Usr_cb_TypeDef USBH_USR_cb;

USB_OTG_CORE_HANDLE USB_OTG_host;
USBH_HOST USB_Host;

void USB_MSC_Init(void) {
	USBH_Init(&USB_OTG_host,
			USB_OTG_HS_CORE_ID,
			&USB_Host,
			&USBH_MSC_cb,
			&USBH_USR_cb);
}

#if USE_USBH_MSC
void OTG_HS_IRQHandler(void) {
	USBH_OTG_ISR_Handler(&USB_OTG_host);
}
#endif

/**
 * This function needs to be called periodically to run the
 * USB Host state machine
 */
void USB_MSC_Process(void) {
	 USBH_Process(&USB_OTG_host, &USB_Host);
}

static char *hostState(void) {
	switch (USB_Host.gState) {
	case HOST_IDLE: return "Idle";
	case HOST_DEV_ATTACHED: return "Device Attached";
	case HOST_DEV_DISCONNECTED: return "Device Disconnected";
	case HOST_DETECT_DEVICE_SPEED: return "Detect Device Speed";
	case HOST_ENUMERATION: return "Enumeration";
	case HOST_CLASS_REQUEST: return "Class Request";
	case HOST_CLASS: return "Class";
	case HOST_CTRL_XFER: return "Control Transfer";
	case HOST_USR_INPUT : return "User Input";
	case HOST_SUSPENDED: return "Suspended";
	case HOST_ERROR_STATE: return "Error";
	default: return "Unknown";
	}
}

/*
static char *enumState(void) {
	switch (USB_Host.EnumState) {
	case ENUM_IDLE: return "Idle";
	case ENUM_GET_FULL_DEV_DESC: return "Get Full Device Desc";
	case ENUM_SET_ADDR: return "Set Address";
	case ENUM_GET_CFG_DESC: return "Get Config Desc";
	case ENUM_GET_FULL_CFG_DESC: return "Get Full Config Desc";
	case ENUM_GET_MFC_STRING_DESC: return "Get Manuf String Desc";
	case ENUM_GET_PRODUCT_STRING_DESC: return "Get Product String Desc";
	case ENUM_GET_SERIALNUM_STRING_DESC: return "Get Serial String Desc";
	case ENUM_SET_CONFIGURATION: return "Set Config";
	case ENUM_DEV_CONFIGURED: return "Configured";
	default: return "Unknown";
	}
}

static char *requestState(void) {
	switch (USB_Host.RequestState) {
	case CMD_IDLE: return "Idle";
	case CMD_SEND: return "Send";
	case CMD_WAIT: return "Wait";
	default: return "Unknown";
	}
}
*/

static char *controlState(void) {
	switch (USB_Host.Control.state) {
	case CTRL_IDLE: return "Idle";
	case CTRL_SETUP: return "Setup";
	case CTRL_SETUP_WAIT: return "Setup Wait";
	case CTRL_DATA_IN: return "Data In";
	case CTRL_DATA_IN_WAIT: return "Data In Wait";
	case CTRL_DATA_OUT: return "Data Out";
	case CTRL_DATA_OUT_WAIT: return "Data Out Wait";
	case CTRL_STATUS_IN: return "Status In";
	case CTRL_STATUS_IN_WAIT: return "Status In Wait";
	case CTRL_STATUS_OUT: return "Status Out";
	case CTRL_STATUS_OUT_WAIT: return "Status Out Wait";
	case CTRL_ERROR: return "Error";
	case CTRL_STALLED: return "Stalled";
	case CTRL_COMPLETE: return "Complete";
	default: return "Unknown";
	}
}

static char *mscState(MSCState state) {
	switch(state) {
	case USBH_MSC_BOT_INIT_STATE: return "Init";
	case USBH_MSC_BOT_RESET: return "Reset";
	case USBH_MSC_GET_MAX_LUN: return "Get Max LUN";
	case USBH_MSC_TEST_UNIT_READY: return "Test Unit Ready";
	case USBH_MSC_READ_CAPACITY10: return "Read Capacity 10";
	case USBH_MSC_MODE_SENSE6: return "Mode Sense 6";
	case USBH_MSC_REQUEST_SENSE: return "Request Sense";
	case USBH_MSC_BOT_USB_TRANSFERS: return "BOT Transfers";
	case USBH_MSC_DEFAULT_APPLI_STATE: return "App";
	case USBH_MSC_CTRL_ERROR_STATE: return "Control Error";
	case USBH_MSC_UNRECOVERED_STATE: return "Unrecovered Error";
	default: return "Unknown";
	}
}

static char *botState(void) {
	switch (USBH_MSC_BOTXferParam.BOTState) {
	case USBH_MSC_SEND_CBW: return "Send CBW";
	case USBH_MSC_SENT_CBW: return "Sent CBW";
	case USBH_MSC_BOT_DATAIN_STATE: return "Data In";
	case USBH_MSC_BOT_DATAOUT_STATE: return "Data Out";
	case USBH_MSC_RECEIVE_CSW_STATE: return "Receive CSW";
	case USBH_MSC_DECODE_CSW: return "Decode CSW";
	case USBH_MSC_BOT_ERROR_IN: return "Error In";
	case USBH_MSC_BOT_ERROR_OUT: return "Error Out";
	default: return "Unknown";
	}
}

static char *urbState(URB_STATE state) {
	switch (state) {
	case URB_IDLE: return "Idle";
	case URB_DONE: return "Done";
	case URB_NOTREADY: return "Not Ready";
	case URB_ERROR: return "Error";
	case URB_STALL: return "Stall";
	default: return "Unknown";
	}
}

extern URB_STATE MSC_DecodeCSW_URBState;

void USB_MSC_Debug(void) {
	printf("MSC: (H) %s, (C) %s, (M) %s, (MC) %s, (B) %s, (BU) %s\n",
			hostState(), controlState(),
			mscState(USBH_MSC_BOTXferParam.MSCState),
			mscState(USBH_MSC_BOTXferParam.MSCStateCurrent),
			botState(), urbState(MSC_DecodeCSW_URBState));
}
