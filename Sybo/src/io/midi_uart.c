/*
 * midi_uart.c
 *
 * Handle UART initalisation, thru and output queues and merging
 *
 *  Created on: 9 Feb 2013
 *      Author: st
 */

#include "midi_uart.h"

static int32_t thruEnable = 0;
static MidiByteQueue thruQueue;
static MidiByteQueue outputQueue;
static MidiByteQueue mergingQueue[2];
static uint32_t currentQueueIndex = 0;

#if USE_PERF
MidiPerformanceData perfMidi;
#endif

static void Midi_NVIC_Init(void) {
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = MIDI_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

static void Midi_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(MIDI_TX_GPIO_CLK | MIDI_RX_GPIO_CLK, ENABLE);

	/* Enable UART clock */
	RCC_APB1PeriphClockCmd(MIDI_CLK, ENABLE);

	/* Connect PXx to USARTx_Tx*/
	GPIO_PinAFConfig(MIDI_TX_GPIO_PORT, MIDI_TX_SOURCE, MIDI_TX_AF);

	/* Connect PXx to USARTx_Rx*/
	GPIO_PinAFConfig(MIDI_RX_GPIO_PORT, MIDI_RX_SOURCE, MIDI_RX_AF);

	/* Configure USART Tx as alternate function  */
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

	GPIO_InitStructure.GPIO_Pin = MIDI_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(MIDI_TX_GPIO_PORT, &GPIO_InitStructure);

	/* Configure USART Rx as alternate function  */
	GPIO_InitStructure.GPIO_Pin = MIDI_RX_PIN;
	GPIO_Init(MIDI_RX_GPIO_PORT, &GPIO_InitStructure);
}

void Midi_UART_Init(void) {
	USART_InitTypeDef USART_InitStructure;

	mergingQueue[0] = thruQueue;
	mergingQueue[1] = outputQueue;

	Midi_NVIC_Init();
	Midi_GPIO_Init();

	USART_InitStructure.USART_BaudRate = 31250;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(MIDI_USART, &USART_InitStructure);

	USART_Cmd(MIDI_USART, ENABLE);

	/* Enable the MIDI_USART Transmit interrupt: this interrupt is generated when the
	 MIDI_USART transmit data register is empty */
	// needs to be dynamically enabled/disabled when tx data is available TODO
//	USART_ITConfig(MIDI_USART, USART_IT_TXE, ENABLE);

	/* Enable the MIDI_USART Receive interrupt: this interrupt is generated when the
	 MIDI_USART receive data register is not empty */
	USART_ITConfig(MIDI_USART, USART_IT_RXNE, ENABLE);
}

#if USE_MIDI
static inline void produce(MidiMessage m) {
	Midi_Produce(m);
#	if USE_PERF
	perfMidi.nInQPMsgs++;
#	endif
}

static void Midi_Input(const uint8_t byte) {
	static MidiMessage current, realtime;
	static int remaining = 0;
	static int nDataBytes = 0;
	static int inSysex = 0;
	static MidiQueueByte thruByte;

#   if USE_PERF
	perfMidi.nInBytes++;
#   endif
	// process system real time immediately
	// because they may occur between bytes of other messages
	if (MIDI_IS_REAL_TIME_STATUS(byte)) {
#		if USE_SEQ_CLOCK
		if (byte == TIMING_CLOCK) {
			if (!SeqClock_isExternalClock()) {
				return; // don't even thru it to clash with int clk sent
			}
			SeqClock_ExternalTick();
		}
#		endif
		realtime.status = byte;
		Midi_Produce(realtime);
#       if USE_PERF
		perfMidi.nInRTMsgs++;
#       endif
	} else {
		if (MIDI_IS_CHANNEL_VOICE_STATUS(byte)) {
#			if USE_PERF
			perfMidi.nInCVMsgs++;
			perfMidi.chan = MIDI_CHANNEL(byte);
#			endif
			current.status = byte;
			uint8_t cmd = byte & 0xF0; 		// remove channel
			nDataBytes = remaining =
					(cmd == PROGRAM_CHANGE || cmd == CHANNEL_PRESSURE) ? 1 : 2;
		} else if (MIDI_IS_MESSAGE_STATUS(byte)) {
#			if USE_PERF
			perfMidi.nInSCMsgs++;
#			endif
			current.status = byte;
			remaining = 0;					// for TUNE_REQUEST, EOX
			if (byte == MTC_QUARTER_FRAME || byte == SONG_SELECT) {
				remaining = 1;
			} else if (byte == SONG_POSITION_PTR) {
				remaining = 2;
			} else if (byte == SYSEX) {
				inSysex = 1; 				// remaining is irrelevant
			} else if (byte == EOX) {
				inSysex = 0; 				// use init remaining of 0
			} else if (byte == TUNE_REQUEST) {
				produce(current);
			}
			nDataBytes = remaining;
		} else  if (!inSysex) {
			if (remaining == 0) {
				remaining = nDataBytes; 	// handle running status
#				if USE_PERF
				perfMidi.nInRSMsgs++;
#				endif
			}
			if (remaining > 0) {
				if (remaining == 2 || nDataBytes == 1) {
					current.val1 = byte; 	// 1st of 2 or only byte
				} else {
					current.val2 = byte; 	// 2nd of 2 bytes
				}
				remaining -= 1;
			}
			if (remaining == 0) {
				produce(current);
			}
		} else {
			// sysex data ends up here and is ignored
		}
	}
	// queue thru byte with last byte flag set if remaining == 0
	// system realtime won't set last byte flag if interrupting a message
	if (thruEnable) {
		thruByte.byte = byte;
		thruByte.last = (remaining == 0 && !inSysex) ? 1 : 0;
		Midi_ProduceByte(thruQueue, thruByte);
		Midi_OutputTickle(); 			// in case TX IRQ stalled
#		if USE_PERF
		perfMidi.nThruBytes++;
#		endif
	}
}

#define OTHER_QUEUE() 	1 - currentQueueIndex
#define CHANGE_QUEUE() 	currentQueueIndex = OTHER_QUEUE()

static void Midi_Output(void) {
	MidiByteQueue q = mergingQueue[currentQueueIndex];
	if (Midi_AvailableByte(q)) {
		MidiQueueByte b = Midi_ConsumeByte(q);
		USART_SendData(MIDI_USART, b.byte);
		if (b.last != 0) {
			CHANGE_QUEUE(); // change queue if last byte of message
		}
	} else { // nothing available from current queue so change queue
		CHANGE_QUEUE();
		q = mergingQueue[currentQueueIndex];
		if (Midi_AvailableByte(q)) {
			MidiQueueByte b = Midi_ConsumeByte(q);
			USART_SendData(MIDI_USART, b.byte);
			if (b.last != 0) {
				CHANGE_QUEUE(); // change queue if last byte of message
			}
		}
	}
}

/**
 * Handle USARTx global interrupt request.
 */
void USARTx_IRQHANDLER(void) {
	if (USART_GetITStatus(MIDI_USART, USART_IT_RXNE) != RESET) {
		Midi_Input(USART_ReceiveData(MIDI_USART) & 0xFF);
		// pending flag has been cleared by receiving data
	}
	if (USART_GetITStatus(MIDI_USART, USART_IT_TXE) != RESET) {
		Midi_Output();
		// pending flag may have been cleared if data was written
	}
}
#endif

#define TX_STALLED_MASK (USART_FLAG_TXE|USART_FLAG_TC)
#define MIDI_OUTPUT_STALLED() ((MIDI_USART->SR & TX_STALLED_MASK) == TX_STALLED_MASK)

// if TX IRQ has stalled we should send an available byte to restart it
void Midi_OutputTickle(void) {
	if (MIDI_OUTPUT_STALLED()) {
		unsigned int output_mutex = UNLOCKED;
		// Wait until the output mutex is acquired
		lock_mutex(&output_mutex);
		// check whether TX IRQ is still stalled
		// it may have been restarted if mutex was contended
		if (MIDI_OUTPUT_STALLED()) {
			Midi_Output();
		}
		// Leave critical section - release output mutex
		unlock_mutex(&output_mutex);
	}
}

void Midi_ThruEnable(int enable) {
	thruEnable = enable;
}

#if USE_PERF
void Midi_PrintData(void) {
	printf("MIDI bytes: %u in, %u thru\n",
			(unsigned)perfMidi.nInBytes,
			(unsigned)perfMidi.nThruBytes);
	printf("MIDI messages: %u RT, %u CV, %u SC, %u RS, %u P\n",
			(unsigned)perfMidi.nInRTMsgs,
			(unsigned)perfMidi.nInCVMsgs,
			(unsigned)perfMidi.nInSCMsgs,
			(unsigned)perfMidi.nInRSMsgs,
			(unsigned)perfMidi.nInQPMsgs);
	printf("MIDI received on channel: %u\n", (unsigned)perfMidi.chan);
}
#endif
