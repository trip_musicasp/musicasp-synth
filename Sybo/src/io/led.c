/*
 * led.c
 *
 * use of a large number of LEDs controlled by daisy-chained shift
 * registers such as 74hc595
 *
 *  Created on: 24 Feb 2013
 *      Author: st
 */

#include "led.h"

static int nLeds;
static int nBytes;
static uint8_t byte[N_BYTES];

/*
 * Initialise the LED system for n LEDs
 */
void LED_Init(int n) {
	GPIO_InitTypeDef  GPIO_InitStructure;

	assert_param((n > 0) && (n <= 8*N_BYTES));

	nLeds = n;					// store the number of leds
	nBytes = 1 + (nLeds % 8);	// work out the number of shift registers

	// initialise GPIO clock
	RCC_AHB1PeriphClockCmd(LED_GPIO_CLK, ENABLE);
	// Configure data and clock pins
	GPIO_InitStructure.GPIO_Pin = LED_DATA_PIN | LED_CLOCK_PIN | LED_REGISTER_PIN;
	GPIO_InitStructure.GPIO_Speed = LED_GPIO_SPEED;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = LED_GPIO_OUTPUT_TYPE;
	GPIO_InitStructure.GPIO_PuPd = LED_GPIO_PULL_TYPE;
	GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
}

/*
 * Select the state of a single LED to be set on the next call
 * to LED_Update().
 */
void LED_Set(int n, int state) {
	assert_param((n >= 0) && (n < nLeds));
	int nbyte = n >> 3;
	int nbit = n % 8;
	if (state) { // set
		byte[nbyte] |= (1 << nbit);
	} else { // clear
		byte[nbyte] &= ~(1 << nbit);
	}
}

/*
 * Call this once after you have called LED_Set as many times as you need to
 * set all the LED states.
 */
void LED_Update(void) {
	GPIO_WriteBit(LED_GPIO_PORT, LED_REGISTER_PIN, RESET);
	int i, j, bit;
	for (i = 0; i < nBytes; i++) {
		for (j = 0; j < 8; j++) {
			bit = (byte[i] & (1 << j)) ? SET : RESET;
			GPIO_WriteBit(LED_GPIO_PORT, LED_DATA_PIN, bit);
			GPIO_WriteBit(LED_GPIO_PORT, LED_CLOCK_PIN, SET);
			// clock a single data bit into the serial registers
			GPIO_WriteBit(LED_GPIO_PORT, LED_CLOCK_PIN, RESET);
		}
	}
	// clock the serial registers into the output registers
	GPIO_WriteBit(LED_GPIO_PORT, LED_REGISTER_PIN, SET);
}



