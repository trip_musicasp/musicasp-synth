/*
 * perf.c
 *
 * We use a 32 bit timer to implement a performance counter.
 *
 *  Created on: 1 Mar 2013
 *      Author: st
 */

#include "perf.h"
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h>

void Perf_Init(void) {
	TIM_TimeBaseInitTypeDef perfTimerStruct;

	RCC_APB1PeriphClockCmd(PERF_TIMER_RCC, ENABLE);
	TIM_TimeBaseStructInit(&perfTimerStruct);
	TIM_TimeBaseInit(PERF_TIMER, &perfTimerStruct);
	TIM_Cmd(PERF_TIMER, ENABLE);
}

inline int32_t Perf_Read(void) {
	return PERF_TIMER->CNT;
}
