/*
 * debug_led.h
 *
 *  Created on: 16 Mar 2013
 *      Author: st
 */

#ifndef DEBUG_LED_H_
#define DEBUG_LED_H_

#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>

void DebugLed_Init(void);
void DebugLed_set(int led, int val);

#endif /* DEBUG_LED_H_ */
