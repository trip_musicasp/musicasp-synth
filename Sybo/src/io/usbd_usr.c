/**
  ******************************************************************************
  * @file    usbd_usr.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    19-September-2011
  * @brief   This file includes the user application layer
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

#include "usbd_usr.h"
#include "usbd_ioreq.h"
#include <stdio.h>

USBD_Usr_cb_TypeDef USR_cb = {
  USBD_USR_Init,
  USBD_USR_DeviceReset,
  USBD_USR_DeviceConfigured,
  USBD_USR_DeviceSuspended,
  USBD_USR_DeviceResumed,
  
  USBD_USR_DeviceConnected,
  USBD_USR_DeviceDisconnected,
};

const char MSG_DEVICE_INIT[] = "[USBD] Device Init\n";
const char MSG_DEVICE_RESET[] = "[USBD] Device Reset\n";
const char MSG_DEVICE_CONFIGURED[] = "[USBD] Device Configured\n";
const char MSG_DEVICE_SUSPENDED[] = "[USBD] Device Suspended\n";
const char MSG_DEVICE_RESUMED[] = "[USBD] Device Resumed\n";
const char MSG_DEVICE_CONNECTED[] = "[USBD] Device Connected\n";
const char MSG_DEVICE_DISCONNECTED[] = "[USBD] Device Disconnected\n";

/**
* @brief  USBD_USR_Init 
* @param  None
* @retval None
*/
void USBD_USR_Init(void) {
	printf(MSG_DEVICE_INIT);
}

/**
 * @brief  USBD_USR_DeviceReset
 * @param  speed : device speed
 * @retval None
 */
void USBD_USR_DeviceReset(uint8_t speed) {
	printf(MSG_DEVICE_RESET);
}

/**
 * @brief  USBD_USR_DeviceConfigured
 * @param  None
 * @retval Staus
 */
void USBD_USR_DeviceConfigured(void) {
	printf(MSG_DEVICE_CONFIGURED);
}

/**
 * @brief  USBD_USR_DeviceConnected
 * @param  None
 * @retval Staus
 */
void USBD_USR_DeviceConnected(void) {
	printf(MSG_DEVICE_CONNECTED);
}

/**
 * @brief  USBD_USR_DeviceDisonnected
 * @param  None
 * @retval Staus
 */
void USBD_USR_DeviceDisconnected(void) {
	printf(MSG_DEVICE_DISCONNECTED);
}

/**
 * @brief  USBD_USR_DeviceSuspended
 * @param  None
 * @retval None
 */
void USBD_USR_DeviceSuspended(void) {
	printf(MSG_DEVICE_SUSPENDED);
	/* Users can do their application actions here for the USB-Reset */
}

/**
 * @brief  USBD_USR_DeviceResumed
 * @param  None
 * @retval None
 */
void USBD_USR_DeviceResumed(void) {
	printf(MSG_DEVICE_RESUMED);
	/* Users can do their application actions here for the USB-Reset */
}

