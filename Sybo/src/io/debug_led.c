/*
 * debug_led.c
 *
 * There are 2 LEDs on Sybo that may be used for debug purposes.
 * They are on PE0 and PE1
 *
 *  Created on: 16 Mar 2013
 *      Author: st
 */

#include "debug_led.h"

void DebugLed_Init(void) {
	GPIO_InitTypeDef  GPIO_InitStructure;
	// initialise GPIO clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	// Configure data and clock pins
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
}

void DebugLed_set(int led, int val) {
	GPIO_WriteBit(GPIOE, led ? GPIO_Pin_1 : GPIO_Pin_0, val ? SET : RESET);
}



