/*
 * audio_codec.c
 *
 * Handle Audio CODEC, I2S, DMA and IRQ initalisation and use
 *
 * We use one DMA stream from memory to peripheral for DAC
 * We use one DMA stream from peripheral to memory for ADC
 * Because ADC support is optional we call Audio_Process() on
 * the DAC DMA transfer complete IRQ handler which is always active.
 * Output to DAC is properly clipped to avoid wrapround.
 *
 */

#if USE_AUDIO
#include "audio_codec.h"
#include <stdlib.h>

static DMA_InitTypeDef DMA_OutInitStructure;
static I2S_InitTypeDef I2S_InitStructure;

#if USE_AUDIO_IN
static DMA_InitTypeDef DMA_InInitStructure;
static uint32_t *inputWords[2]; 			// interleaved left/right
AudioDMAIrqData audioInData;
#endif
static float *inputFloatPtrs[2];
static uint32_t *outputWords[2]; 			// interleaved left/right
static float *outputFloatPtrs[2];
AudioDMAIrqData audioOutData;
#if AUDIO_IRQ_ERROR_HANDLING
AudioI2SIrqData audioI2SData;
#endif

uint32_t nFrames = 32;

/**
 * Initializes the SPIx peripheral according to the specified parameters in
 * the I2S_InitStruct. This is an alternative to the standard I2S_Init()
 * function allowing dynamic modification of the external I2S clock source.
 *
 * @param  SPIx: where x can be  2 or 3 to select the SPI peripheral (configured in I2S mode).
 * @param  I2S_InitStruct: pointer to an I2S_InitTypeDef structure that
 *         contains the configuration information for the specified SPI peripheral
 *         configured in I2S mode.
 * @param  I2sSrcClk: value of the I2S external source clock in Hz (ie. 12288000).
 *
 * @note   The function calculates the optimal prescaler needed to obtain the most
 *         accurate audio frequency (depending on the I2S clock source, the PLL values
 *         and the product configuration). But in case the prescaler value is greater
 *         than 511, the default value (0x02) will be configured instead.
 */
uint32_t I2S_InitExt(SPI_TypeDef* SPIx, I2S_InitTypeDef* I2S_InitStruct,
		uint32_t I2sSrcClk) {
	uint16_t tmpreg = 0, i2sdiv = 2, i2sodd = 0;
	uint32_t tmp = 0;

	// ----------------------- SPIx I2SCFGR & I2SPR Configuration -----------
	// Clear I2SMOD, I2SE, I2SCFG, PCMSYNC, I2SSTD, CKPOL, DATLEN and CHLEN bits
	SPIx->I2SCFGR &= ((uint16_t) 0xF040);
	SPIx->I2SPR = 0x0002;

	// Get the I2SCFGR register value
	tmpreg = SPIx->I2SCFGR;

	// If the requested audio frequency is not the default, compute the prescaler
	// Compute the Real divider with a floating point
	tmp = (uint16_t) (((((I2sSrcClk / 256) * 10)
					/ I2S_InitStruct->I2S_AudioFreq)) + 5);

	// Remove the floating point
	tmp = tmp / 10;

	// Check the parity of the divider
	i2sodd = (uint16_t) (tmp & (uint16_t) 0x0001);

	// Compute the i2sdiv prescaler
	i2sdiv = (uint16_t) ((tmp - i2sodd) / 2);

	// see p.825 tech ref
	uint32_t actualRate =  I2sSrcClk / (256 * ((2 * i2sdiv) + i2sodd));

	// Get the Mask for the Odd bit (SPI_I2SPR[8]) register
	i2sodd = (uint16_t) (i2sodd << 8);

	// Test if the divider is 1 or 0 or greater than 0xFF
	if ((i2sdiv < 2) || (i2sdiv > 0xFF)) {
		// Set the default values
		i2sdiv = 2;
		i2sodd = 0;
	}

	// Write to SPIx I2SPR register the computed value
	SPIx->I2SPR = (uint16_t) ((uint16_t) i2sdiv
			| (uint16_t) (i2sodd | (uint16_t) I2S_InitStruct->I2S_MCLKOutput));

	// Configure the I2S with the SPI_InitStruct values
	tmpreg |= (uint16_t) ((uint16_t) SPI_I2SCFGR_I2SMOD
			| (uint16_t) (I2S_InitStruct->I2S_Mode
					| (uint16_t) (I2S_InitStruct->I2S_Standard
							| (uint16_t) (I2S_InitStruct->I2S_DataFormat
									| (uint16_t) I2S_InitStruct->I2S_CPOL))));

	// Write to SPIx I2SCFGR
	SPIx->I2SCFGR = tmpreg;

	return actualRate;
}

/**
 * Initializes the Audio Codec audio interface (I2S)
 * This function assumes that the dedicated PLLI2S_R is already configured
 * and ready to be used.
 */
uint32_t Codec_AudioInterface_Init(uint32_t sampleRate) {
	uint32_t plli2sN = 344, plli2sR = 2;
	uint32_t pllm = 1;
	uint32_t I2SSrcClkVal = 0;

	// Compute the PLL parameters
	pllm = (uint32_t) (RCC->PLLCFGR & RCC_PLLCFGR_PLLM);
	I2SSrcClkVal = (uint32_t) (((HSE_VALUE / pllm) * plli2sN) / plli2sR);

	// Enable the CODEC_I2S peripheral clock
	RCC_APB1PeriphClockCmd(CODEC_I2S_CLK, ENABLE);

	// Reset CODEC_I2S peripheral
	I2S_Cmd(CODEC_I2S, DISABLE);
	SPI_I2S_DeInit(CODEC_I2S);
#if USE_AUDIO_IN
	I2S_Cmd(CODEC_I2S_EXT, DISABLE);
	SPI_I2S_DeInit(CODEC_I2S_EXT);
#endif
	// codec is slave, HPF enabled, CKS0 controlw normal/double speed
	GPIO_WriteBit(CODEC_CONTROL_GPIO, CODEC_CONTROL_MASTER_PIN, RESET);
	// enable output HPF in slave mode
	GPIO_WriteBit(CODEC_CONTROL_GPIO, CODEC_CONTROL_HPF_PIN, RESET);
	// if sampleRate >52k we need double speed, otherwise normal speed
	int ns = sampleRate < 52000 ? 1 : 0;
	GPIO_WriteBit(CODEC_CONTROL_GPIO, CODEC_CONTROL_CKS0_PIN, ns);

	// Disable PLLI2S
	RCC->CFGR |= RCC_CFGR_I2SSRC;
	RCC->CR &= ~((uint32_t) RCC_CR_PLLI2SON);

	// PLLI2S clock used as I2S clock source
	RCC->CFGR &= ~RCC_CFGR_I2SSRC;

	// Configure PLLI2S
	RCC->PLLI2SCFGR = (plli2sN << 6) | (plli2sR << 28);

	// Enable PLLI2S
	RCC->CR |= ((uint32_t) RCC_CR_PLLI2SON);

	// Wait till PLLI2S is ready
	while ((RCC->CR & RCC_CR_PLLI2SRDY) == 0) {
	}

	// CODEC_I2S peripheral configuration
	I2S_InitStructure.I2S_AudioFreq = sampleRate;
	I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_24b;
	I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
	I2S_InitStructure.I2S_Mode = I2S_Mode_MasterTx;
	I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Enable;

	// Initialize the I2S peripheral with the structure above
	uint32_t actualRate = I2S_InitExt(CODEC_I2S, &I2S_InitStructure,
			(uint32_t) (I2SSrcClkVal));

	// Enable the I2S DMA TX request
	SPI_I2S_DMACmd(CODEC_I2S, SPI_I2S_DMAReq_Tx, ENABLE);

#if USE_AUDIO_IN
	// Enable the I2Sext DMA RX request
	SPI_I2S_DMACmd(CODEC_I2S_EXT, SPI_I2S_DMAReq_Rx, ENABLE);
	printf("Audio_Start(): I2SEXT DMA CR2=%X\n", (unsigned)CODEC_I2S_EXT->CR2);
#endif
	// The I2S peripheral will be enabled only in the Audio_Start() function

	return actualRate;
}

/**
 * Initialises IOs used by the Audio Codec control and audio interfaces.
 */
static void Codec_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStructure;

	// Enable I2S GPIO clocks
	RCC_AHB1PeriphClockCmd(CODEC_I2S_GPIO_CLOCK, ENABLE);

	// common settings for all i/o
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	// common settings for alternate function i/o
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;

	// CODEC_I2S pins configuration: WS, SCK and SD In and Out pins
	GPIO_InitStructure.GPIO_Pin = CODEC_I2S_WS_PIN;
	GPIO_Init(CODEC_I2S_WS_GPIO, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = CODEC_I2S_SD_OUT_PIN | CODEC_I2S_SCK_PIN |
			CODEC_I2S_SD_IN_PIN;
	GPIO_Init(CODEC_I2S_SD_SCK_GPIO, &GPIO_InitStructure);

	// Connect WS, SCK and SD In and Out pins to peripheral
	GPIO_PinAFConfig(CODEC_I2S_WS_GPIO, CODEC_I2S_WS_PINSRC, GPIO_AF_SPI3);
	GPIO_PinAFConfig(CODEC_I2S_SD_SCK_GPIO, CODEC_I2S_SD_OUT_PINSRC, GPIO_AF_SPI3);
	GPIO_PinAFConfig(CODEC_I2S_SD_SCK_GPIO, CODEC_I2S_SCK_PINSRC, GPIO_AF_SPI3);
	GPIO_PinAFConfig(CODEC_I2S_SD_SCK_GPIO, CODEC_I2S_SD_IN_PINSRC, GPIO_AF_I2S3ext);

	// CODEC_I2S pins configuration: MCK pin
	GPIO_InitStructure.GPIO_Pin = CODEC_I2S_MCK_PIN;
	GPIO_Init(CODEC_I2S_MCK_GPIO, &GPIO_InitStructure);

	// Connect MCK pin
	GPIO_PinAFConfig(CODEC_I2S_MCK_GPIO, CODEC_I2S_MCK_PINSRC, GPIO_AF_SPI3);

	// Enable Codec control GPIO clock
	RCC_AHB1PeriphClockCmd(CODEC_CONTROL_GPIO_CLOCK, ENABLE);

	// common settings for control outputs
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz; // low speed

	GPIO_InitStructure.GPIO_Pin = CODEC_CONTROL_MASTER_PIN |
			CODEC_CONTROL_CKS0_PIN | CODEC_CONTROL_HPF_PIN;
	GPIO_Init(CODEC_CONTROL_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CODEC_CONTROL_PDN_PIN;
	GPIO_Init(CODEC_CONTROL_GPIO, &GPIO_InitStructure);
}

/**
 * Initializes the audio codec and I2S interface
 */
uint32_t Codec_Init(uint32_t sampleRate) {
	Codec_GPIO_Init();
	return Codec_AudioInterface_Init(sampleRate);
}

/**
 * Starts playing audio stream from the audio Media.
 * @param  nFrames: Number of frames per block
 */
void Audio_Start(uint32_t nFrames) {
	/* Disable the I2S DMA Stream */
	DMA_Cmd(AUDIO_OUT_DMA_STREAM, DISABLE);

	// Clear the Interrupt flag
	DMA_ClearFlag(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_TC);
#if AUDIO_IRQ_ERROR_HANDLING
	DMA_ClearFlag(AUDIO_OUT_DMA_STREAM,
			AUDIO_OUT_DMA_FLAG_TE | AUDIO_OUT_DMA_FLAG_FE);
#endif


	// Configure the DMA transfer count
	// << 2, 1 for interleaved stereo frames, 1 for 16 bit peripheral
	// see 9.3.10, p223, RM0090
	DMA_OutInitStructure.DMA_BufferSize = (uint32_t)(nFrames << 2);

	// Configure the DMA Stream with the new parameters
	DMA_Init(AUDIO_OUT_DMA_STREAM, &DMA_OutInitStructure);

	// Enable the I2S DMA Stream
	DMA_Cmd(AUDIO_OUT_DMA_STREAM, ENABLE);
//	printf("Audio_Start(): DMA OUT CR=%X\n", (unsigned)AUDIO_OUT_DMA_STREAM->CR);

#if USE_AUDIO_IN
	/* Disable the I2S DMA Stream */
	DMA_Cmd(AUDIO_IN_DMA_STREAM, DISABLE);

	// Clear the Interrupt flag
	DMA_ClearFlag(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_TC);
#if AUDIO_IRQ_ERROR_HANDLING
	DMA_ClearFlag(AUDIO_IN_DMA_STREAM,
			AUDIO_IN_DMA_FLAG_TE | AUDIO_IN_DMA_FLAG_FE);
#endif

	// Configure the DMA transfer count
	// << 2, 1 for interleaved stereo frames, 1 for 16 bit peripheral
	// see 9.3.10, p223, RM0090
	DMA_InInitStructure.DMA_BufferSize = (uint32_t)(nFrames << 2);

	// Configure the DMA Stream with the new parameters
	DMA_Init(AUDIO_IN_DMA_STREAM, &DMA_InInitStructure);

	// Enable the I2S DMA Stream
	DMA_Cmd(AUDIO_IN_DMA_STREAM, ENABLE);
	printf("Audio_Start(): DMA IN CR=%X\n", (unsigned)AUDIO_IN_DMA_STREAM->CR);

	// Enable the I2S peripheral
	I2S_Cmd(CODEC_I2S_EXT, ENABLE);
	printf("Audio_Start(): I2SEXT CFGR=%X\n", (unsigned)CODEC_I2S_EXT->I2SCFGR);
#endif
	// Enable the I2S peripheral
	I2S_Cmd(CODEC_I2S, ENABLE);
//	printf("Audio_Start(): I2S CFGR=%X\n", (unsigned)CODEC_I2S->I2SCFGR);
//	printf("Audio_Start(): I2S DMA CR2=%X\n", (unsigned)CODEC_I2S->CR2);
	// when all clocks are present, power up codec
	GPIO_WriteBit(CODEC_CONTROL_GPIO, CODEC_CONTROL_PDN_PIN, SET);
}

/**
 * Initialises and prepares the Media to perform audio data transfer
 * from Media to the I2S peripheral. After ensuring the codec has initialised.
 */
void Audio_Init(AudioSettings *settings) {
	__set_FPSCR(__get_FPSCR() | FPU_FPDSCR_FZ_Msk); // Flush To Zero mode

	settings->actualSampleRate = Codec_Init(settings->sampleRate);

	// force a multiple of MIN_FRAMES within MIN_FRAMES .. MAX_FRAMES
	int n = settings->nFrames;
	if (n > MAX_FRAMES)	n = MAX_FRAMES;
	if (n < MIN_FRAMES)	n = MIN_FRAMES;
	n &= ~(MIN_FRAMES-1);					// TODO better
	settings->actualNFrames = nFrames = n;

	inputFloatPtrs[0] = (float *)malloc(nFrames * sizeof(float));
	inputFloatPtrs[1] = (float *)malloc(nFrames * sizeof(float));
	outputFloatPtrs[0] = (float *)malloc(nFrames * sizeof(float));
	outputFloatPtrs[1] = (float *)malloc(nFrames * sizeof(float));
	outputWords[0] = (uint32_t *)malloc(nFrames * 2 * sizeof(uint32_t));
	outputWords[1] = (uint32_t *)malloc(nFrames * 2 * sizeof(uint32_t));
#if USE_AUDIO_IN
	inputWords[0] = (uint32_t *)malloc(nFrames * 2 * sizeof(uint32_t));
	inputWords[1] = (uint32_t *)malloc(nFrames * 2 * sizeof(uint32_t));
#endif

	NVIC_InitTypeDef NVIC_InitStructure;

	// Enable the DMA clock
	RCC_AHB1PeriphClockCmd(AUDIO_DMA_CLOCK, ENABLE);

	// Configure the DMA Stream
	DMA_Cmd(AUDIO_OUT_DMA_STREAM, DISABLE);
	DMA_DeInit(AUDIO_OUT_DMA_STREAM);
	// Set the parameters to be configured
	DMA_OutInitStructure.DMA_Channel = AUDIO_OUT_DMA_CHANNEL;
	DMA_OutInitStructure.DMA_PeripheralBaseAddr = (uint32_t)CODEC_I2S_OUT_DR;
	DMA_OutInitStructure.DMA_Memory0BaseAddr = (uint32_t)outputWords[0];
	DMA_OutInitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_OutInitStructure.DMA_BufferSize = (uint32_t) 0xFFFE; // configured later
	DMA_OutInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_OutInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_OutInitStructure.DMA_PeripheralDataSize = AUDIO_DMA_PERIPH_DATA_SIZE;
	DMA_OutInitStructure.DMA_MemoryDataSize = AUDIO_DMA_MEM_DATA_SIZE;
	DMA_OutInitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_OutInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_OutInitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_OutInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_OutInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_OutInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(AUDIO_OUT_DMA_STREAM, &DMA_OutInitStructure);

	// Setup Double Buffering
	DMA_DoubleBufferModeConfig(AUDIO_OUT_DMA_STREAM, (uint32_t)outputWords[1],
			DMA_Memory_0);
	DMA_DoubleBufferModeCmd(AUDIO_OUT_DMA_STREAM, ENABLE);

	// Enable the selected DMA interrupts
	DMA_ITConfig(AUDIO_OUT_DMA_STREAM, DMA_IT_TC, ENABLE);
#if AUDIO_IRQ_ERROR_HANDLING
	DMA_ITConfig(AUDIO_OUT_DMA_STREAM, DMA_IT_TE | DMA_IT_FE, ENABLE);
	// enable the IS2 error interrupts
	SPI_I2S_ITConfig(CODEC_I2S, SPI_I2S_IT_ERR, ENABLE);
#endif

	// Enable the I2S DMA request
	SPI_I2S_DMACmd(CODEC_I2S, SPI_I2S_DMAReq_Tx, ENABLE);

#if USE_AUDIO_IN
	// Configure the DMA Stream
	DMA_Cmd(AUDIO_IN_DMA_STREAM, DISABLE);
	DMA_DeInit(AUDIO_IN_DMA_STREAM);
	// Set the parameters to be configured
	DMA_InInitStructure.DMA_Channel = AUDIO_IN_DMA_CHANNEL;
	DMA_InInitStructure.DMA_PeripheralBaseAddr = (uint32_t)CODEC_I2S_IN_DR;
	DMA_InInitStructure.DMA_Memory0BaseAddr = (uint32_t)inputWords[0];
	DMA_InInitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InInitStructure.DMA_BufferSize = (uint32_t) 0xFFFE; // configured later
	DMA_InInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InInitStructure.DMA_PeripheralDataSize = AUDIO_DMA_PERIPH_DATA_SIZE;
	DMA_InInitStructure.DMA_MemoryDataSize = AUDIO_DMA_MEM_DATA_SIZE;
	DMA_InInitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InInitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InInitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(AUDIO_IN_DMA_STREAM, &DMA_InInitStructure);

	// Setup Double Buffering
	DMA_DoubleBufferModeConfig(AUDIO_IN_DMA_STREAM, (uint32_t)inputWords[1],
			DMA_Memory_0);
	DMA_DoubleBufferModeCmd(AUDIO_IN_DMA_STREAM, ENABLE);

	// Enable the selected DMA interrupts
	DMA_ITConfig(AUDIO_IN_DMA_STREAM, DMA_IT_TC, ENABLE);
#if AUDIO_IRQ_ERROR_HANDLING
	DMA_ITConfig(AUDIO_IN_DMA_STREAM, DMA_IT_TE | DMA_IT_FE, ENABLE);
#endif

	// Enable the I2S DMA request
	SPI_I2S_DMACmd(CODEC_I2S_EXT, SPI_I2S_DMAReq_Rx, ENABLE);
#endif
	// I2S DMA IRQ Channel configuration
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_OUT_DMA_IRQ;
	NVIC_Init(&NVIC_InitStructure);
#if USE_AUDIO_IN
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_IN_DMA_IRQ;
	NVIC_Init(&NVIC_InitStructure);
#endif
#if AUDIO_IRQ_ERROR_HANDLING
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_SPI_IRQ;
	NVIC_Init(&NVIC_InitStructure);
#endif
}

void Audio_PrintData(void) {
	printf("Audio Out Completions: %u\n", (unsigned)audioOutData.nDMATC);
#if AUDIO_IRQ_ERROR_HANDLING
	printf("Audio Out Errors: %u/%u (Transmission/FIFO)\n",
			(unsigned)audioOutData.nDMATE, (unsigned)audioOutData.nDMAFE);
#endif
#if USE_AUDIO_IN
	printf("Audio In Completions: %u\n", (unsigned)audioInData.nDMATC);
#if AUDIO_IRQ_ERROR_HANDLING
	printf("Audio In Errors: %u/%u (Transmission/FIFO)\n",
			(unsigned)audioInData.nDMATE, (unsigned)audioInData.nDMAFE);
#endif
#endif
#if AUDIO_IRQ_ERROR_HANDLING
	printf("Audio I2S Errors: %u/%u/%u (Overrun/Underrun/Format)\n",
			(unsigned)audioI2SData.nI2SOVR,	(unsigned)audioI2SData.nI2SUDR,
			(unsigned)audioI2SData.nI2STIFRFE);
#endif
}

static inline uint32_t floatToWord(float f) {
	uint32_t x = (uint32_t) clip_q63_to_q31((q63_t) (f * 2147483648.0f));
	// swap LE DMA to BE I2S
	return (x & 0x0000FFFF) << 16 | (x & 0xFFFF0000) >> 16;
}

void AUDIO_OUT_IRQHandler(void) {
	// DMA Transfer complete interrupt
	if (DMA_GetFlagStatus(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_TC) != RESET) {
		Audio_Process(inputFloatPtrs, outputFloatPtrs, nFrames);
		// get the index of the memory target currently in use by the DMA Stream
		int n = DMA_GetCurrentMemoryTarget(AUDIO_OUT_DMA_STREAM);
		// use the other memory target (see p222/223 RM0090)
		uint32_t *pOut = outputWords[1 - n];
		int i;
		for (i = 0; i < nFrames;) {
			*pOut++ = floatToWord(outputFloatPtrs[0][i]);
			*pOut++ = floatToWord(outputFloatPtrs[1][i++]);
			*pOut++ = floatToWord(outputFloatPtrs[0][i]);
			*pOut++ = floatToWord(outputFloatPtrs[1][i++]);
		}
		audioOutData.nDMATC++;
		// Clear the Interrupt flag
		DMA_ClearFlag(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_TC);
	}
#if AUDIO_IRQ_ERROR_HANDLING
	if (DMA_GetFlagStatus(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_TE) != RESET) {
		audioOutData.nDMATE++;
		DMA_ClearFlag(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_TE);
	}
	if (DMA_GetFlagStatus(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_FE) != RESET) {
		audioOutData.nDMAFE++;
		DMA_ClearFlag(AUDIO_OUT_DMA_STREAM, AUDIO_OUT_DMA_FLAG_FE);
	}
#endif
}

// none of the SPI IRQ flags need to be cleared
#if AUDIO_IRQ_ERROR_HANDLING
void AUDIO_SPI_IRQHandler(void) {
	/* OVR (OverRun error) flag is cleared by software sequence: a read
	 * operation to SPI_DR register (SPI_I2S_ReceiveData()) followed by a read
	 * operation to SPI_SR register (SPI_I2S_GetFlagStatus()). */
   	if (SPI_I2S_GetFlagStatus(CODEC_I2S, SPI_I2S_FLAG_OVR) != RESET) {
		audioI2SData.nI2SOVR++;
	}
    /* UDR (UnderRun error) flag is cleared by a read operation to
     * SPI_SR register (SPI_I2S_GetFlagStatus()). */
	if (SPI_I2S_GetFlagStatus(CODEC_I2S, I2S_FLAG_UDR) != RESET) {
		audioI2SData.nI2SUDR++;
	}
	// Format error
	if (SPI_I2S_GetFlagStatus(CODEC_I2S, SPI_I2S_FLAG_TIFRFE) != RESET) {
		audioI2SData.nI2STIFRFE++;
	}
}
#endif

#if USE_AUDIO_IN
static inline float wordToFloat(uint32_t w) {
	// swap LE DMA to BE I2S
	w = (w & 0x0000FFFF) << 16 | (w & 0xFFFF0000) >> 16;
	return ((float)w / 2147483648.0f);
}

/**
 * @brief  This function handles main Media layer interrupt.
 */
void AUDIO_IN_IRQHandler(void) {
	// DMA Transfer complete interrupt
	if (DMA_GetFlagStatus(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_TC) != RESET) {
		// get the index of the memory target currently in use by the DMA Stream
		int n = DMA_GetCurrentMemoryTarget(AUDIO_IN_DMA_STREAM);
		// use the other memory target (see p222/223 RM0090)
		uint32_t *pIn = inputWords[1 - n];
		int i;
		for (i = 0; i < nFrames; ) {
			inputFloats[0][i] = wordToFloat(*pIn++);
			inputFloats[1][i++] = wordToFloat(*pIn++);
			inputFloats[0][i] = wordToFloat(*pIn++);
			inputFloats[1][i++] = wordToFloat(*pIn++);
		}
		audioInData.nDMATC++;
		// Clear the Interrupt flag
		DMA_ClearFlag(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_TC);
	}
#if AUDIO_IRQ_ERROR_HANDLING
	if (DMA_GetFlagStatus(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_TE) != RESET) {
		audioInData.nDMATE++;
		DMA_ClearFlag(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_TE);
	}
	if (DMA_GetFlagStatus(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_FE) != RESET) {
		audioInData.FE++;
		DMA_ClearFlag(AUDIO_IN_DMA_STREAM, AUDIO_IN_DMA_FLAG_FE);
	}
#endif // USE_AUDIO_IRQ_ERROR_HANDLING
}
#endif // USE_AUDIO_IN

#endif // USE_AUDIO
