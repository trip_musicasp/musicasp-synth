/*
 * adc.h
 *
 *  Created on: 22 Feb 2013
 *      Author: st
 */

#ifndef ADC_H_
#define ADC_H_

#define _PORT_SHIFT	16
#define _PORT_MASK	0x03
#define _PIN_SHIFT	8
#define _PIN_MASK	0x0F
#define _AIN_MASK 	0x0F

// private macro to help build the AnalogInput enum constants cleanly
#define _AIN(port,pin,ain) ((int)(((port-'A') << _PORT_SHIFT) | (pin << _PIN_SHIFT) | ain))

// public macros to help unbuild the AnalogInput enum constants
#define AIN_AIN(x) ((uint8_t)(x & _AIN_MASK))
#define _PORT(x) ((x >> _PORT_SHIFT) & _PORT_MASK)
#define AIN_PORT(x) (_PORT(x) == 0 ? GPIOA : (_PORT(x) == 1 ? GPIOB : GPIOC))
#define AIN_PIN(x) ((uint16_t)(1 << ((x >> _PIN_SHIFT) & _PIN_MASK)))

// public constants, enum and function declarations ------------------------

#if USE_DAC
#define N_ANALOG_IN	14
#else
#define N_ANALOG_IN 15
#endif

typedef enum {
	AIN_A0 = _AIN('A', 0, 0),
	AIN_A1 = _AIN('A', 1, 1),
	AIN_A2 = _AIN('A', 2, 2),
	AIN_A3 = _AIN('A', 3, 3),
//  AIN_A4 is unavailable because PA4 is used by I2S3_WS AF6
#if !USE_DAC
	AIN_A5 = _AIN('A', 5, 5),	// shares PA5 with DAC2_OUT
#endif
	AIN_A6 = _AIN('A', 6, 6),
	AIN_A7 = _AIN('A', 7, 7),
	AIN_B0 = _AIN('B', 0, 8),
	AIN_B1 = _AIN('B', 1, 9),
	AIN_C0 = _AIN('C', 0, 10),
	AIN_C1 = _AIN('C', 1, 11),
	AIN_C2 = _AIN('C', 2, 12),
	AIN_C3 = _AIN('C', 3, 13),
	AIN_C4 = _AIN('C', 4, 14),
	AIN_C5 = _AIN('C', 5, 15)
} AnalogInput;

void AnalogIn_Init(AnalogInput *ain, int nain);
void AnalogIn_StartScan(void);

extern void AnalogIn_Callback(float *raw, int nain);

#if USE_PERF
void ADC_printData(void);
#endif

// stuff to configure the hardware used by the source -------------------------

#define USE_ADC_DMA 			1

#if USE_ADC_DMA
#define ADC_DMA_CLOCK           RCC_AHB1Periph_DMA2
#define ADC_DMA_STREAM          DMA2_Stream0
#define ADC_DMA_CHANNEL         DMA_Channel_0
#define ADC_DMA_IRQ             DMA2_Stream0_IRQn
#define ADC_DMA_FLAG_TC         DMA_FLAG_TCIF0
#define ADC_DMA_FLAG_TE         DMA_FLAG_TEIF0
#define ADC_DMA_FLAG_FE         DMA_FLAG_FEIF0

#define ADC_DMA_IRQHandler      DMA2_Stream0_IRQHandler
#endif

#endif /* ADC_H_ */
