/*
 * serial.c
 *
 * A simple function to obtain a clean input line from the rather
 * crappy C input. Had to bypass newlib because it seemed to always
 * buffer stdin whether I called setbuf to disable buffering or setvbuf
 * to enable line buffering. So I call the _read syscall directly.
 *
 *  Created on: 21 Feb 2013
 *      Author: st
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

extern int _read(int file, char *ptr, int len);

#define LEN 80
static char line[LEN+1];

/*
 * Handle control characters (e.g. \b, terminating \n and \r)
 */
char *getcleanline(void) {
	int i = -1;
	char ch;
	do {
		_read(0, &line[++i], 1);
		ch = line[i];
		if (ch == '\b') i -= (i > 1) ? 2 : 1; // handle backspace
	} while (i < LEN && ch != '\r' && ch != '\n');
	line[i] = '\0';
	return line;
}

