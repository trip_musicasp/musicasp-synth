/*
 * seq_clock.c
 *
 * We use a 16 bit timer to implement a sequencer clock down to
 * approximately 19 BPM. We generate 48 PPQN, as was possible with
 * DIN-Sync, even when synced to an external 24 PPQN clock.
 * If the external clock fails we naturally jam-sync it.
 *
 *  Created on: 21 Mar 2013
 *      Author: st
 */

#include "seq_clock.h"
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h>
#include <misc.h>
#include <weak.h>

#define MAX_BPM 	290

static int MIN_BPM;
static int externalClock = 0;
static volatile int clearTicks = 0;
static volatile int ticksSinceExternalClock = 0;

static float BPMForPeriod(uint16_t uspp) {
	float spp = uspp * 1e-6f;
	float pps = 1.f / spp;
	float bps = pps / SEQ_CLOCK_PPQN;
	return bps * 60.f;
}

static uint16_t periodForBPM(float bpm) {
	float bps = bpm / 60.f;					// beats per second
	float pps = bps * SEQ_CLOCK_PPQN;		// pulses per second
	float spp = 1.f / pps;					// seconds per pulse
	float uspp = spp * 1e6f;				// microseconds per pulse
	return (uint16_t)uspp;
}

void SeqClock_Init(void) {
	MIN_BPM = BPMForPeriod(0xffff);

	TIM_TimeBaseInitTypeDef stepSeqTimerStruct;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(SEQ_CLOCK_TIMER_RCC, ENABLE);

	TIM_TimeBaseStructInit(&stepSeqTimerStruct);
	stepSeqTimerStruct.TIM_Prescaler = 84; // for 1MHz counter clock
	stepSeqTimerStruct.TIM_CounterMode = TIM_CounterMode_Up;
	stepSeqTimerStruct.TIM_Period = periodForBPM(120.f);
	TIM_TimeBaseInit(SEQ_CLOCK_TIMER, &stepSeqTimerStruct);

	NVIC_InitStructure.NVIC_IRQChannel = SEQ_CLOCK_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_ITConfig(SEQ_CLOCK_TIMER, TIM_IT_Update, ENABLE);
	TIM_Cmd(SEQ_CLOCK_TIMER, ENABLE);
}

void SeqClock_setBPM(float bpm) {
	if (externalClock) return;
	if (bpm < MIN_BPM) bpm = MIN_BPM;
	if (bpm > MAX_BPM) bpm = MAX_BPM;
	SEQ_CLOCK_TIMER->CNT = 0;						// TODO
	SEQ_CLOCK_TIMER->ARR = periodForBPM(bpm);
}

float SeqClock_getBPM(void) {
	return BPMForPeriod(SEQ_CLOCK_TIMER->ARR);
}

void SeqClock_setExternalClock(int state) {
	externalClock = state;
}

int SeqClock_isExternalClock(void) {
	return externalClock;
}

// called by the external clock
void SeqClock_ExternalTick(void) {
	int N = ticksSinceExternalClock;
	uint16_t X = SEQ_CLOCK_TIMER->CNT;
	uint16_t C = SEQ_CLOCK_TIMER->ARR;
	// in general for N, X, C
	// 2C' = NC + X
	// C' = (NC + X)/2
	clearTicks = 1;
	uint32_t newARR = (N*C + X) >> 1;
	if (newARR > 0xffff) return; 				// sanity check
	SEQ_CLOCK_TIMER->ARR = (uint16_t)newARR;
}

#if USE_SEQ_CLOCK

int WEAK SeqClock_HandleTick(void) { return SEQ_CLOCK_PPQN; }

static void SeqClock_Tick(void) {
	static int nticks = 1;
	if (--nticks > 0) return;
	nticks = SeqClock_HandleTick();
}

void SEQ_CLOCK_IRQ_HANDLER(void) {
	if (TIM_GetITStatus(SEQ_CLOCK_TIMER, TIM_IT_Update) != RESET) {
		if (externalClock) {
			if (clearTicks) {
				clearTicks = 0;
				ticksSinceExternalClock = 0;
			}
			ticksSinceExternalClock++;
		}
		SeqClock_Tick();
		// TODO send midi timing clock if !externalClock
		TIM_ClearITPendingBit(SEQ_CLOCK_TIMER, TIM_IT_Update);
	}
}
#endif

