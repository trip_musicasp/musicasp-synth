/*
 * perf.h
 *
 *  Created on: 1 Mar 2013
 *      Author: st
 */

#ifndef PERF_H_
#define PERF_H_

#include <stdint.h>

void Perf_Init(void);
int32_t Perf_Read(void);

#define PERF_TIMER 		TIM2
#define PERF_TIMER_RCC 	RCC_APB1Periph_TIM2

#endif /* PERF_H_ */
