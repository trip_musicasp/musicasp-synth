/*
 * ser_uart.c
 *
 *  Created on: 11 Mar 2013
 *      Author: st
 */

#include "ser_uart.h"
#include <stm32f4xx_usart.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stdio.h>
#include <string.h>

static int ser_init_done = 0;

/**
 * Configures USART GPIO.
 */
void SerUart_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(USART_GPIO_CLOCK, ENABLE);

	/* Enable UART clock */
	RCC_APB2PeriphClockCmd(USART_CLOCK, ENABLE);

	/* Connect PXx to USARTx_Tx*/
	GPIO_PinAFConfig(USART_GPIO, USART_TX_PINSRC, USART_AF);

	/* Connect PXx to USARTx_Rx*/
	GPIO_PinAFConfig(USART_GPIO, USART_RX_PINSRC, USART_AF);

	/* Configure USART Tx as alternate function  */
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

	GPIO_InitStructure.GPIO_Pin = USART_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* Configure USART Rx as alternate function  */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = USART_RX_PIN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

}

void SerUart_Init(void) {
	USART_InitTypeDef USART_InitStructure;

	/* USART configured as follow:
	 - BaudRate = 115200 baud
	 - Word Length = 8 Bits
	 - One Stop Bit
	 - No parity
	 - Hardware flow control disabled (RTS and CTS signals)
	 - Receive and transmit enabled
	 */
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	SerUart_GPIO_Init();

	/* USART configuration */
	USART_Init(DEBUG_USART, &USART_InitStructure);

	/* Enable USART */
	USART_Cmd(DEBUG_USART, ENABLE);
	ser_init_done = 1;
}

// is there a character available?
int SerUart_haschar(void) {
	return (DEBUG_USART->SR & USART_FLAG_RXNE) != 0;
}

int SerUart_getchar(uint8_t *buf) {
	if (!SerUart_haschar()) return 0;
	*buf = (uint8_t)DEBUG_USART->DR;
	return 1;
}

int SerUart_putchar(uint8_t ch) {
	if (!ser_init_done) return 0;
	if (ch == '\n') SerUart_putchar('\r');
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART */
	USART_SendData(DEBUG_USART, ch);

	/* Loop until the end of transmission */
	while (USART_GetFlagStatus(DEBUG_USART, USART_FLAG_TC) == RESET) {
	}

	return ch;
}

void SerUart_send_buffer(uint8_t *ptr, int len) {
	if (!ser_init_done) return;
	int i;
	for (i = 0; i < len; i++) {
		SerUart_putchar(*ptr++);
	}

}

void SerUart_print(char *ptr) {
	SerUart_send_buffer((uint8_t *)ptr, strlen(ptr));
}
