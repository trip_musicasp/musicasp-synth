/*
 * led.h
 *
 *  Created on: 24 Feb 2013
 *      Author: st
 */

#ifndef LED_H_
#define LED_H_

#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>

#define LED_GPIO_PORT			GPIOD
#define LED_GPIO_CLK			RCC_AHB1Periph_GPIOD
#define LED_DATA_PIN			GPIO_Pin_0
#define LED_CLOCK_PIN			GPIO_Pin_1
#define LED_REGISTER_PIN		GPIO_Pin_5
#define LED_GPIO_SPEED			GPIO_Speed_2MHz
#define LED_GPIO_OUTPUT_TYPE	GPIO_OType_PP
#define LED_GPIO_PULL_TYPE		GPIO_PuPd_NOPULL

#define N_BYTES					6

#endif /* LED_H_ */
