/*
 * step_seq.h
 *
 *  Created on: 22 Mar 2013
 *      Author: st
 */

#ifndef STEP_SEQ_H_
#define STEP_SEQ_H_

#include <stdint.h>
#include "midi.h"

typedef struct stepSeqLane {
	char *name;
	uint8_t burst;				// maximum notes to play at once
	uint8_t used;				// how many notes are available
	MidiMessage *messages;
	uint8_t (*prepare)(uint16_t bar, uint8_t teenth, struct stepSeqLane *lane);
	void (*record)(uint16_t bar, uint8_t teenth, struct stepSeqLane *lane, MidiMessage m);
} StepSeqLane;

void StepSeq_setShuffle(uint8_t amount); // 1 .. 7 (none .. full shuffle)
int StepSeq_Process(void);
void StepSeq_Play(void);
void StepSeq_Stop(void);
int StepSeq_isPlaying(void);
void StepSeq_addLane(StepSeqLane *l);

#endif /* STEP_SEQ_H_ */
