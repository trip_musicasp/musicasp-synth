/*
 * app.h
 *
 * This is the main constract for an application. see main.c
 *
 *  Created on: 11 Feb 2013
 *      Author: st
 */

#ifndef APP_H_
#define APP_H_

void App_Init(void);
void App_Background(void);

#endif /* APP_H_ */
