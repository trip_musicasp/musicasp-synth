/*
 * param.h
 *
 * Parameters are represented in 3 ways, for example, 440Hz in a 20Hz
 * to 20,000Hz range to the user, somewhere in the range 0..1 for the raw
 * value to Audio_SetParameter(), and normalised angular frequency to the dsp
 * code.
 *
 * A ControlLaw converts between user and raw values. It has a
 * ParamConverter to transparently convert, say, the log frequency user
 * values, to and from the linear raw values.
 *
 * When either the user or raw value changes, the other will be updated
 * accordingly, as will the dsp value.
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef PARAM_H_
#define PARAM_H_

#include <math.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <assertp.h>
#include <ff.h>

struct control_law;

typedef struct {
	void (*init)(struct control_law *law);
	float (*rawToUser)(struct control_law *law, float rawVal);
	float (*userToRaw)(struct control_law *law, float userVal);
	float (*userToUser)(struct control_law *law, float userVal);
} ParamConverter;

typedef struct control_law {
	float userMin;
	float userMax;
	const char *units;
	ParamConverter *convert;
	float (*userToDsp)(float userVal);
	const char *(*annotation)(unsigned n);
	float logMin;	// only used for log
	float logSpan;  // only used for log
} ControlLaw;

typedef struct {
	float rawVal;
	float userVal;
	float dspVal;
	ControlLaw *law;
	char *abbrev;
} Parameter;

void Param_Init(Parameter *p, char *abbrev, float userVal, ControlLaw *law);
void Param_Link(Parameter *p, uint32_t nparams);
void Param_Update(Parameter *p, float rawVal);
void Param_ParseUserVal(char *s);
void Param_PrintAll(void);
Parameter *Param_fromAbbreviation(char *s);

FRESULT Param_Save(char *partialAbrrev, char *path);
FRESULT Param_Load(char *path);

extern ParamConverter Param_LinConverter;
extern ParamConverter Param_LogConverter;
extern ParamConverter Param_DiscreteConverter;

#define PRESET_EXT			".preset"
#define PRESET_DIR			"0:/presets/"
#define PRESET_DEFAULT		PRESET_DIR"default"PRESET_EXT


#endif /* PARAM_H_ */
