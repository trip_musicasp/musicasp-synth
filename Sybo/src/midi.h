/*
 * midi.h
 *
 *  Created on: 9 Feb 2013
 *      Author: st
 *
 * A MIDI message is an instruction that controls some aspect of the
 * receiving device. A MIDI message consists of a status byte, which
 * indicates the type of the message, followed by up to two data bytes
 * that contain the parameters.[17] MIDI messages can be "channel messages",
 * which are sent on only one of the 16 channels and can be heard only by
 * devices receiving on that channel, or "system messages", which are heard
 * by all devices. Any data not relevant to a receiving device is ignored.
 *
 * There are five types of message: Channel Voice, Channel Mode, System Common,
 * System Real-Time, and System Exclusive.
 *
 * Channel Voice messages transmit real-time performance data over a single
 * channel. Examples include "note-on" messages which contain a MIDI note
 * number that specifies the note's pitch, a velocity value that indicates how
 * forcefully the note was played, and the channel number; "note-off" messages
 * that end a note; program change messages that change a device's patch;
 * and control changes that allow adjustment of an instrument's parameters.
 * http://www.indiana.edu/~emusic/etext/MIDI/chapter3_MIDI4.shtml
 *
 * Channel Mode messages include the Omni/mono/poly mode on and off messages,
 * as well as messages to reset all controllers to their default state or to
 * send "note-off" messages for all notes.
 *
 * System messages do not include
 * channel numbers, and are received by every device in the MIDI chain.
 *
 * MIDI time code is an example of a System Common message.
 *
 * System Real-Time messages provide for synchronization, and include MIDI clock
 * and Active Sensing.
 */

#ifndef MIDI_H_
#define MIDI_H_

#include <stdint.h>
#include "midi_uart.h"
#include <math.h>

#define BYTE_SIZE_MASK 0x3f				// TODO properly size
#define BYTE_SIZE (BYTE_SIZE_MASK+1)	// size a power of 2

#define NEXT_BYTE(x) ((x+1) & BYTE_SIZE_MASK)

typedef struct {
	uint8_t status;
	uint8_t val1;
	uint8_t val2;
	uint8_t spare;
} MidiMessage ;

typedef struct {
	uint8_t byte;
	uint8_t last : 1;
} MidiQueueByte;

typedef struct {
	int first;						// owned by Produce
	volatile int last, divider;		// shared by Produce and Consume
	MidiQueueByte fifo[BYTE_SIZE];
} MidiByteQueue;


typedef enum
{
	NOTE_OFF 			= 	0b10000000,	// key, velocity
	NOTE_ON 			= 	0b10010000, // key, velocity
	POLY_PRESSURE 		=	0b10100000,	// key, pressure
	CONTROL_CHANGE		=	0b10110000, // control, value
	PROGRAM_CHANGE		= 	0b11000000,	// preset, NONE
	CHANNEL_PRESSURE	=	0b11010000,	// pressure, NONE
	PITCH_BEND			=	0b11100000  // fine, coarse (14 bit resolution)
} MidiChannelVoice;						// LSB is channel


typedef enum
{
	SYSEX				=	0b11110000, // arbitrary length
	MTC_QUARTER_FRAME	=	0b11110001, // value, see http://www.electronics.dit.ie/staff/tscarff/Music_technology/midi/MTC.htm
	SONG_POSITION_PTR	= 	0b11110010, // lsb, msb
	SONG_SELECT			=	0b11110011,	// song number, NONE
//	reserved			=	0b11110100,
//	reserved			=	0b11110101,
	TUNE_REQUEST		=	0b11110110, // NONE, NONE
	EOX					=	0b11110111  // NONE, NONE
} MidiSystemCommon;

/*
 * no data bytes, may be sent at any time, even between the bytes of other
 * messages, so should be processed first
 */
typedef enum
{
	TIMING_CLOCK		=	0b11111000,
//	reserved			=	0b11111001,
	START				=	0b11111010,
	CONTINUE			=	0b11111011,
	STOP				=	0b11111110,
//	reserved			=	0b11111101,
	ACTIVE_SENSING		=	0b11111110,
	SYSTEM_RESET		=	0b11111111
} MidiSystemRealtime;

#define MIDI_IS_REAL_TIME_STATUS(x) 		((x & 0b11111000) == 0b11111000)
#define MIDI_IS_CHANNEL_VOICE_STATUS(x) 	((x & 0b10000000) && (x & 0b01110000) != 0b01110000)
#define MIDI_IS_MESSAGE_STATUS(x) 			(x & 0b10000000)
#define MIDI_CHANNEL(x) 					(x & 0x0f)

typedef enum
{
	BANK_SELECT = 0,
	MOD_WHEEL = 1,
	BREATH = 2,

	FOOT = 4,
	PORTAMENTO_TIME = 5,
	DATA_ENTRY_MSB = 6,
	MAIN_VOLUME = 7,
	BALANCE = 8,

	PAN = 10,

	EFFECT_1 = 12,
	EFFECT_2 = 13,

	EFFECT_1_DEPTH = 91,
	EFFECT_2_DEPTH = 92,
	EFFECT_3_DEPTH = 93,
	EFFECT_4_DEPTH = 94,
	EFFECT_5_DEPTH = 95,
	DATA_INCREMENT = 96,
	DATA_DECREMENT = 97,
	NRPN_LSB = 98,
	NRPN_MSB = 99,
	RPN_LSB = 100,
	RPN_MSB = 101,

	RESET_ALL_CONTROLLERS = 121,
	LOCAL_CONTROL = 122,
	ALL_NOTES_OFF = 123,
	OMNI_OFF = 124,
	OMNI_ON = 125,
	MONO_ON = 126,
	MONO_OFF = 127
} MidiControl;

void Midi_Init(void);

void Midi_Produce(MidiMessage msg);
int Midi_Available(void);
MidiMessage Midi_Consume(void);

void Midi_ProduceByte(MidiByteQueue q, MidiQueueByte b);
int Midi_AvailableByte(MidiByteQueue q);
MidiQueueByte Midi_ConsumeByte(MidiByteQueue q);

int Midi_ConsumeMessages(int max);
float Midi_Frequency(int key);

// function called by Midi_ConsumeMessages
int Midi_ChannelHandled(int channel);

void Midi_HandleMessage(MidiMessage m);

void Midi_HandleRealTimeMessage(MidiMessage m);
void Midi_HandleSystemCommonMessage(MidiMessage m);
void Midi_HandleChannelVoiceMessage(MidiMessage m);

// functions called by Midi_HandleRealTimeMessage
void Midi_HandleTimingClock(void);
void Midi_HandleStart(void);
void Midi_HandleContinue(void);
void Midi_HandleStop(void);
void Midi_HandleSystemReset(void);

// functions called by Midi_HandleSystemCommonMessage
void Midi_HandleMTCQuarterFrame(uint8_t val);
void Midi_HandleSongPositionPointer(uint8_t msb, uint8_t lsb);
void Midi_HandleSongSelect(uint8_t song);
void Midi_HandleTuneRequest(void);

// functions called by Midi_HandleChannelVoiceMessage
void Midi_HandleNoteOn(uint8_t channel, uint8_t key, uint8_t velocity);
void Midi_HandleNoteOff(uint8_t channel, uint8_t key, uint8_t velocity);
void Midi_HandlePolyPressure(uint8_t channel, uint8_t key, uint8_t pressure);
void Midi_HandleControlChange(uint8_t channel, uint8_t control, uint8_t value);
void Midi_HandleProgramChange(uint8_t channel, uint8_t program);
void Midi_HandleChannelPressure(uint8_t channel, uint8_t pressure);
void Midi_HandlePitchBend(uint8_t channel, uint8_t fine, uint8_t coarse);

void Midi_HandleAllNotesOff(void);

#endif /* MIDI_H_ */
