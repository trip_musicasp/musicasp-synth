/*
 * midi.c
 *
 * Provides a single producer single consumer wait-free FIFO
 * Produce will overwrite unconsumed items but this should never
 * happen due to the relative timings of the MIDI UART interrupt,
 * the Audio DMA interrupt and the limited speed of MIDI.
 *
 * Bassed on Herb Sutter's correction of Petru Marginean flawed FIFO
 * and obviously converted to an array of uint32_t for packed MIDI Messages.
 * http://www.drdobbs.com/parallel/writing-lock-free-code-a-corrected-queue/210604448?pgno=2
 * We use data memory barriers between modifying and publishing since the
 * ARM memory model is weakly ordered for normal memory.
 *
 *  Created on: 10 Feb 2013
 *      Author: st
 */

#include "midi.h"
#include "synch.h"
#include <weak.h>

#define SIZE_MASK 0x1f

#define NEXT(x) ((x+1) & SIZE_MASK)

static int first;					// owned by Produce
static volatile int last, divider;	// shared by Produce and Consume

static MidiMessage fifo[SIZE_MASK+1];	// size a power of 2

static float midiFrequency[128];

static void initFreqs(void) {
	int i;
	for (i = 0; i < 128; i++) {
		midiFrequency[i] = (float)(440.0 * pow(2.0, ((double)i - 69.0) / 12.0));
	}
}

void Midi_Init(void) {
	Midi_UART_Init();
	first = divider = last = 0;
	initFreqs();
}

float Midi_Frequency(int key) {
	return midiFrequency[key];
}

// --------------------------------------------------------------------------
// ---- The Midi Input Queue of MidiMessages --------------------------------
// --------------------------------------------------------------------------

// ? how does it really behave on fifo full
void Midi_Produce(MidiMessage msg) {
#if USE_AUDIO
	int tmp = NEXT(last);
    fifo[tmp] = msg;  				// add the new item
    DMB();
    last = tmp;						// publish it
    while( first != divider ) {		// trim
    	first = NEXT(first);
    }
#endif
}

int Midi_Available(void) {
	return (divider != last);
}

// call Midi_Available() first to ensure there's something to consume
// typically while (MidiAvailable()) msg = Midi_Consume();
MidiMessage Midi_Consume(void) {
	int tmp = NEXT(divider);
    MidiMessage msg = fifo[tmp];	// C: copy it back
    DMB();
    divider = tmp; 					// D: publish that we took it
    return msg;          			// and report success
}

// --------------------------------------------------------------------------
// ---- The Midi Output and Thru Queues of bytes (with flags) ---------------
// --------------------------------------------------------------------------

// ? how does it really behave on fifo full
void Midi_ProduceByte(MidiByteQueue q, MidiQueueByte b) {
	int tmp = NEXT_BYTE(q.last);
    q.fifo[tmp] = b;  				// add the new item
    DMB();
    q.last = tmp;						// publish it
    while( q.first != q.divider ) {		// trim
    	q.first = NEXT_BYTE(q.first);
    }
}

int Midi_AvailableByte(MidiByteQueue q) {
	return (q.divider != q.last);
}

// call Midi_AvailableByte() first to ensure there's something to consume
// typically while (MidiAvailableByte()) b = Midi_ConsumeByte(q);
MidiQueueByte Midi_ConsumeByte(MidiByteQueue q) {
	int tmp = NEXT_BYTE(q.divider);
    MidiQueueByte b = q.fifo[tmp];	// C: copy it back
    DMB();
    q.divider = tmp; 				// D: publish that we took it
    return b;          				// and report success
}

// --------------------------------------------------------------------------
// ---- Utility function for synths -----------------------------------------
// --------------------------------------------------------------------------

int WEAK Midi_ChannelHandled(int channel) { return 0; }

int Midi_ConsumeMessages(int max) {
	int consumed = 0;
	while (Midi_Available() && consumed < max) {
		Midi_HandleMessage(Midi_Consume());
		consumed += 1;
	}
	return consumed;
}

// --------------------------------------------------------------------------
// ---- Midi Message handling/Decoding --------------------------------------
// --------------------------------------------------------------------------

void Midi_HandleMessage(MidiMessage msg) {
	uint8_t status = msg.status;
	if (MIDI_IS_REAL_TIME_STATUS(status)) {
		Midi_HandleRealTimeMessage(msg);
	} else if (MIDI_IS_CHANNEL_VOICE_STATUS(status)) {
		if (Midi_ChannelHandled(MIDI_CHANNEL(status))) {
			Midi_HandleChannelVoiceMessage(msg);
		}
	} else {
		Midi_HandleSystemCommonMessage(msg);
	}
}

void WEAK Midi_HandleTimingClock(void) {}
void WEAK Midi_HandleStart(void) {}
void WEAK Midi_HandleContinue(void) {}
void WEAK Midi_HandleStop(void) {}
void WEAK Midi_HandleSystemReset(void) {}

/*
 * Handle System Real Time Messages with weak handlers so that
 * users only need to override whichever handlers they need.
 */
void Midi_HandleRealTimeMessage(MidiMessage m) {
	uint8_t status = m.status;
	if (status == TIMING_CLOCK) {
		Midi_HandleTimingClock();
	} else if (status == START) {
		Midi_HandleStart();
	} else if (status == CONTINUE) {
		Midi_HandleContinue();
	} else if (status == STOP) {
		Midi_HandleStop();
	} else if (status == SYSTEM_RESET) {
		Midi_HandleSystemReset();
	}
}

void WEAK Midi_HandleMTCQuarterFrame(uint8_t val) {}
void WEAK Midi_HandleSongPositionPointer(uint8_t msb, uint8_t lsb) {}
void WEAK Midi_HandleSongSelect(uint8_t song) {}
void WEAK Midi_HandleTuneRequest(void) {}

/*
 * Handle System Common Messages with weak handlers so that
 * users only need to ovderride whichever handlers they need.
 */
void Midi_HandleSystemCommonMessage(MidiMessage m) {
	uint8_t status = m.status;
	if (status == MTC_QUARTER_FRAME) {
		Midi_HandleMTCQuarterFrame(m.val1);
	} else if (status == SONG_POSITION_PTR) {
		Midi_HandleSongPositionPointer(m.val1, m.val2);
	} else if (status == SONG_SELECT) {
		Midi_HandleSongSelect(m.val1);
	} else if (status == TUNE_REQUEST) {
		Midi_HandleTuneRequest();
	}
}

void WEAK Midi_HandleNoteOn(uint8_t channel, uint8_t key, uint8_t velocity) {}
void WEAK Midi_HandleNoteOff(uint8_t channel, uint8_t key, uint8_t velocity) {}
void WEAK Midi_HandlePolyPressure(uint8_t channel, uint8_t key, uint8_t pressure) {}
void WEAK Midi_HandleControlChange(uint8_t channel, uint8_t control, uint8_t value) {}
void WEAK Midi_HandleProgramChange(uint8_t channel, uint8_t program) {}
void WEAK Midi_HandleChannelPressure(uint8_t channel, uint8_t pressure) {}
void WEAK Midi_HandlePitchBend(uint8_t channel, uint8_t fine, uint8_t coarse) {}

/*
 * Handle Channel Voice Messages with weak handlers so that
 * users only need to override whichever handlers they need.
 */
void Midi_HandleChannelVoiceMessage(MidiMessage m) {
	uint8_t ch = MIDI_CHANNEL(m.status);
	uint8_t status = m.status & 0xF0;
	if (status == NOTE_ON) {
		Midi_HandleNoteOn(ch, m.val1, m.val2);
	} else if (status == NOTE_OFF) {
		Midi_HandleNoteOff(ch, m.val1, m.val2);
	} else if (status == POLY_PRESSURE) {
		Midi_HandlePolyPressure(ch, m.val1, m.val2);
	} else if (status == CONTROL_CHANGE) {
		Midi_HandleControlChange(ch, m.val1, m.val2);
	} else if (status == PROGRAM_CHANGE) {
		Midi_HandleProgramChange(ch, m.val1);
	} else if (status == CHANNEL_PRESSURE) {
		Midi_HandleChannelPressure(ch, m.val1);
	} else if (status == PITCH_BEND) {
		Midi_HandlePitchBend(ch, m.val1, m.val2);
	}
}

