/*
 * fat.h
 *
 *  Created on: 16 Apr 2013
 *      Author: st
 */

#ifndef FAT_H_
#define FAT_H_

#include <ff.h>

void FAT_Init(void);
char *FAT_cwd(void);

void FAT_ls(char *path);
void FAT_du(char *path);
void FAT_cd(char *path);
void FAT_md(char *path);
void FAT_cat(char *path);
void FAT_rm(char *path);
void FAT_drv(BYTE drv);

void FAT_printError(FRESULT res);

#endif /* FAT_H_ */
