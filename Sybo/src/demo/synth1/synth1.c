/*
 * synth1.c 	A Synth to demonstrate use of Sybo software
 *
 *  Created on: 11 Feb 2013
 *      Author: st
 */

#include "synth1.h"
#include <unrolled.h>
#include <block.h>

Parameter param[N_PARAMS]; 					// array of synth parameters
static SynthVoice voice[N_SYNTH_VOICES];	// array of synth voices
static RomplerVoice drumVoice[N_DRUM_VOICES];
static RomplerSample drumSample[N_DRUM_SAMPLES];
static SynthVoice bass, lead;
static uint8_t bassKey = 242, leadKey = 242;
static const float ONE_OVER_128 = 1.f / 128;
static const float ONE_OVER_12 = 1.f / 12;
// fx send and return buffers and a buffer for 2x oversampled signals
static float *send[2], *ret[2], *buf2x;
// a buffer for 16 bit signals
static int16_t *s16words;
#if USE_ANALOG_IN
Parameter *paramMap[N_ANALOG_IN]; 			// map of AnalogInputs to Params
#endif
#if USE_PERF
PerformanceData perfData;
#endif
// 6 stages
static Phaser phaser = { 6 };

static float actualSamplePeriod;
static float F2W;

static RomplerSample *s4n(uint8_t note) {
	if (note < GM_LOWEST || note > GM_HIGHEST) return 0;
	return &drumSample[note - GM_LOWEST];
}

// overrides a weak function
void DrumKit_LoadSample(uint8_t note, char *path) {
	FRESULT res = Rompler_initSample(s4n(note), path, actualSamplePeriod,
										 note, DRUM_PARAM(note));
	if (res) {
		printf("'%s' - ", path);
		if (res == 21) {
			printf("Not a mono sample\n");
		} else if (res == 22) {
			printf("Insufficient heap\n");
		} else {
			FAT_printError(res);
		}
	}
}

// normalise to Nyquist
float normaliseFrequency(float Hz) {
	return Hz * actualSamplePeriod;
}

void Synth_Init(uint32_t sampleRate, uint32_t nframes) {
	send[0] = (float *)malloc(nframes * sizeof(float));
	send[1] = (float *)malloc(nframes * sizeof(float));
	ret[0] =  (float *)malloc(nframes * sizeof(float));
	ret[1] =  (float *)malloc(nframes * sizeof(float));
	s16words = (int16_t *)malloc(nframes * sizeof(int16_t));
	buf2x = (float *)malloc(nframes * 2 * sizeof(float));
	actualSamplePeriod = 1.f / sampleRate;
	F2W = normaliseFrequency(440.f);
	Reverb_Init(nframes);
	Phaser_Init(sampleRate);
	Filter_Setup(normaliseFrequency(50.f), normaliseFrequency(20000.f));
	DSFoscillatorW_Init(nframes);
	DrumKit_LoadDefault();
#if USE_ANALOG_IN
	paramMap[0] = &param[FILTER_CUTOFF];
	paramMap[1] = &param[FILTER_RES];
	paramMap[2] = &param[FILTER_ENV_DEPTH];
	paramMap[3] = &param[FILTER_ENV_DECAY];
#endif
}

/**
 * Called when a parameter changes.
 */
void Audio_SetParameter(int32_t index, float rawVal) {
	Param_Update(&param[index], rawVal);
}

/*
 * Called with latest smoothed analogue input values (0..1).
 * We should relate some or all of these to specific Parameters,
 * (based on the AnalogInput array we passed to AnalogIn_init())
 * detect which have changed significantly (to ignore noise) and
 * call Audio_SetParameter() accordingly.
 */
#if USE_ANALOG_IN
void AnalogIn_Callback(float *raw, int nain) {
	Parameter *p;
	int i;
	for (i = 0; i < nain; i++) {
		p = paramMap[i];
		if (!p) continue;
		if (fabs(p->rawVal - raw[i]) < 0.01)
			continue;
		Param_Update(p, raw[i]);
	}
#if USE_PERF
	perfData.nAInCallback++;
#endif
}
#endif

// voice output must be in temp0 or temp1, return the 'index' of the right one
static int processSynthVoice(SynthVoice *v,
		float *temp0, float *temp1, float *vout, uint32_t nframes) {
	const int pb = v->paramBase;
	switch (v->oscType) {
	case OSC_TRIPLE:
		// Triple OSC --------------------------------------------------------
		// vout <- osc1
		PolyBLEPoscillator_process(&v->osc.triple.osc1, vout, nframes);
		// temp0 <- osc2
		PolyBLEPoscillator_process(&v->osc.triple.osc2, temp0, nframes);
		// temp0 <- osc1 + osc2 * level2
		Unrolled_scale_add(temp0, vout, temp0,
				param[pb+OSC2_LEVEL].dspVal, nframes);
		// vout <- osc3
		PolyBLEPoscillator_process(&v->osc.triple.osc3, temp1, nframes);
		// vout <- osc1 + osc2 * level2 + osc3 * level3
		Unrolled_scale_add(vout, temp0, temp1,
				param[pb+OSC3_LEVEL].dspVal, nframes);
		break;
	case OSC_DSF:
		// DSF OSC -----------------------------------------------------------
		// vout <- osc
		DSFoscillatorW_process(&v->osc.dsf, vout, nframes,
				param[pb+OSC_ROLLOFF].dspVal);
		break;
	case OSC_SUPERSAW:
		// SuperSaw OSC ------------------------------------------------------
		// vout <- osc
		SuperSaw_process(&v->osc.supersaw, vout, nframes,
				param[pb+OSC_SS_DETUNE].dspVal,
				param[pb+OSC_SS_MIX].dspVal);
		break;
	}
	// FILTER ------------------------------------------------------------
	if (v->filterType != FILTER_NONE) {
		// temp0 <- filter cutoff env 0..+1
		ADSR_process(&v->filterEnv, temp0, nframes,
				param[pb+FILTER_ENV_ATTACK].dspVal,
				param[pb+FILTER_ENV_DECAY].dspVal,
				param[pb+FILTER_ENV_SUSTAIN].dspVal,
				param[pb+FILTER_ENV_RELEASE].dspVal);
		// temp1 <- temp0 scaled by env depth constant -96..+96
		Unrolled_scale(temp1, temp0,
				param[pb+FILTER_ENV_DEPTH].dspVal * ONE_OVER_12, nframes);
		// temp0 <- add fc constant (-96..+96) and key follow (-69..+58)
		float octave = ONE_OVER_12 * (
				param[pb+FILTER_CUTOFF].dspVal +
				param[pb+FILTER_KEY_FOLLOW].dspVal * (v->key - 69));
		Unrolled_offset(temp0, temp1, octave, nframes);
		// temp1 <- pow(2, temp0)
		Unrolled_pow2(temp1, temp0, nframes);
		// temp0 <- temp1 scaled to normalised frequency (0.5 = nyquist)
		Unrolled_scale(temp0, temp1, F2W, nframes);
		// vout <- in place filter using temp0 for cutoff
		switch(v->filterType) {
		case FILTER_MOOG:
			Moog_process(&v->filter.moog, vout, nframes, temp0,
					param[pb+FILTER_RES].dspVal);
			break;
		case FILTER_TEEMU_TR_LADDER_0:
			Tladder_process0(&v->filter.tladder, vout, nframes, temp0,
					param[pb+FILTER_RES].dspVal);
			break;
		case FILTER_TEEMU_TR_LADDER:
			Tladder_process(&v->filter.tladder, vout, nframes, temp0,
					param[pb+FILTER_RES].dspVal);
			break;
		case FILTER_ANDY_SVF:
			ASSVF_process(&v->filter.assvf, vout, nframes, temp0,
					param[pb+FILTER_RES].dspVal);
			break;
		}
	}
	// AMP ----------------------------------------------------------------
	v->amplitude += 0.1f * (v->amplitudeTarget - v->amplitude);
	// temp0 <- amp env
	ADSR_process(&v->ampEnv, temp0, nframes,
			param[pb+AMP_ENV_ATTACK].dspVal,
			param[pb+AMP_ENV_DECAY].dspVal,
			param[pb+AMP_ENV_SUSTAIN].dspVal,
			param[pb+AMP_ENV_RELEASE].dspVal);
	// temp1 <- vout * temp0 * K
	Unrolled_scale_mult(temp1, vout, temp0,
			param[pb+AMP_LEVEL].dspVal * v->amplitude, nframes);
	return 1;
}

static int processDrumVoice(RomplerVoice *v,
		float *temp0, float *temp1, float *vout, uint32_t nframes) {
	Rompler_processVoice(v, s16words, nframes);
	Unrolled_S16toF32(temp1, s16words, nframes);
	// temp0 <- temp1 scaled by level
	Unrolled_scale(temp0, temp1,
			param[v->paramBase+D_LEVEL].dspVal * v->amplitude, nframes);
	return 0;
}

#define ALTERNATE_SUM_AND_VOUT() vout = out[alt]; alt = 1-alt; sum = out[alt]

#define REVERB(sv) \
	if (useReverb && sv > 0.001f) { \
		Unrolled_scale_add(send[1-snd], send[snd], in[r], sv, nframes); \
		snd = 1 - snd; \
	}

/*
 * Process audio and MIDI
 */
void Audio_Process(float **in, float **out, uint32_t nframes) {
#if USE_PERF
	perfData.nAudioProcess += 1;
	perfData.tAP0 = Perf_Read();
#endif
#if USE_ANALOG_IN
	AnalogIn_StartScan(); // start scan for next time
#endif
#if USE_MIDI && !DEBUG_MIDI
#if USE_PERF
	perfData.nMidiIn +=
#endif
#if USE_SEQ_CLOCK
	StepSeq_Process();
#else
	Midi_ConsumeMessages(3);
#endif // USE_SEQ_CLOCK
#endif
	// Process audio
	float *vout = out[0], *sum = out[1];
	Unrolled_fill(out[0], 0, nframes); // clear initial buffer to sum voices into

	int i, alt = 1, r, snd = 0, vused = 0;
	int useReverb = param[REVERB_RETURN].dspVal > 0.0001f;

	// process poly synth voices
	for (i = 0; i < N_SYNTH_VOICES; i++) {
		if (voice[i].ampEnv.state == COMPLETE) continue;
		ALTERNATE_SUM_AND_VOUT();
		// build a voice using the 3 available buffers
		r = processSynthVoice(&voice[i], in[0], in[1], vout, nframes);
		// vout <- sum + in[r]
		Unrolled_add(vout, sum, in[r], nframes);
		vused++;
	}
	// phaser
	if (vused) {
		phaser.rate = param[PHASER_RATE].dspVal;
		phaser.depth = param[PHASER_DEPTH].dspVal;
		phaser.feedback = param[PHASER_FEEDBACK].dspVal;
		Phaser_Process(&phaser, vout, nframes);
		// reverb send[0] <- vout
		if (useReverb && param[REVERB_SEND].dspVal > 0.001f) {
			Unrolled_scale(send[snd], vout, param[REVERB_SEND].dspVal, nframes);
		} else {
			Unrolled_fill(send[snd], 0, nframes); // clear initial send buffer
		}
	} else {
		Unrolled_fill(send[snd], 0, nframes); // clear initial send buffer
	}

	if (bass.ampEnv.state != COMPLETE) {
		ALTERNATE_SUM_AND_VOUT();
		// build a voice using the 3 available buffers
		r = processSynthVoice(&bass, in[0], in[1], vout, nframes);
		// vout <- sum + in[r]
		Unrolled_add(vout, sum, in[r], nframes);
		// reverb send[1] <- send[0] + in[r]
		REVERB(param[BASS+REVERB_SEND].dspVal);
	}

	if (lead.ampEnv.state != COMPLETE) {
		ALTERNATE_SUM_AND_VOUT();
		// build a voice using the 3 available buffers
		r = processSynthVoice(&lead, in[0], in[1], vout, nframes);
		// vout <- sum + in[r]
		Unrolled_add(vout, sum, in[r], nframes);
		// reverb send[1] <- send[0] + in[r]
		REVERB(param[LEAD+REVERB_SEND].dspVal);
	}

	// process drum voices
	for (i = 0; i < N_DRUM_VOICES; i++) {
		if (!drumVoice[i].active) continue;
		RomplerVoice *v = &drumVoice[i];
		ALTERNATE_SUM_AND_VOUT();
		// build a voice using the 3 available buffers
		r = processDrumVoice(v, in[0], in[1], vout, nframes);
		// vout <- sum + in[r]
		Unrolled_add(vout, sum, in[r], nframes);
		// reverb send[1] <- send[0] + in[r]
		REVERB(param[v->paramBase+D_SEND].dspVal);
	}

	// reverb process
	if (useReverb) {
		Reverb_Process(ret, send[snd], nframes,
			param[REVERB_ALGO].dspVal,
			param[REVERB_PRE_DELAY].dspVal,
			param[REVERB_BANDWIDTH].dspVal,
			param[REVERB_DECAY_TIME].dspVal,
			param[REVERB_DAMPING_FILTER].dspVal);
		Unrolled_scale_add(out[0], vout, ret[0],
			param[REVERB_RETURN].dspVal, nframes);
		Unrolled_scale_add(out[1], vout, ret[1],
			param[REVERB_RETURN].dspVal, nframes);
	} else {
		// mono to stereo when reverb return hasn't done it for free
		Unrolled_copy(sum, vout, nframes);
	}
#if USE_PERF
	uint32_t te = Perf_Read();
	if (te > perfData.tAP0) {
		perfData.tAPdur = te - perfData.tAP0;
		if (perfData.tAPdur > perfData.tAPdurMax) {
			perfData.tAPdurMax = perfData.tAPdur;
		}
	}
	perfData.max = fmaxf(perfData.max, max_process(sum, nframes));
#endif
}

static void initSynthVoice(SynthVoice *v,
		uint8_t key, uint8_t velocity, uint16_t pb) {
	// we only reset state if we aren't stealing an audible voice
	int reset = v->ampEnv.value < 0.01f;
	float w1 = normaliseFrequency(Midi_Frequency(key));
	v->amplitudeTarget = (float)velocity * ONE_OVER_128;
	v->released = 0;
	v->key = key;
	v->paramBase = pb;
	v->oscType = (uint8_t)param[pb+OSC_TYPE].dspVal;
	switch (v->oscType) {
	case OSC_TRIPLE: {
		OscTriple *o = &v->osc.triple;
		PolyBLEPoscillator_init(&o->osc1, w1,
			param[pb+OSC1_SHAPE].dspVal, reset);
		PolyBLEPoscillator_init(&o->osc2, w1 *
			param[pb+OSC2_TUNE].dspVal,
			param[pb+OSC2_SHAPE].dspVal, reset);
		PolyBLEPoscillator_init(&o->osc3, w1 *
			param[pb+OSC2_TUNE].dspVal,
			param[pb+OSC2_SHAPE].dspVal, reset); }
		break;
	case OSC_DSF:
		DSFoscillatorW_init(&v->osc.dsf, w1,
			param[pb+OSC_N_PARTIALS].dspVal,
			param[pb+OSC_ROLLOFF].dspVal);
		break;
	case OSC_SUPERSAW:
		SuperSaw_init(&v->osc.supersaw, w1);
		break;
	}
	if (reset || v->filterType != (uint8_t)param[pb+FILTER_TYPE].dspVal) {
		v->filterType = (uint8_t)param[pb+FILTER_TYPE].dspVal;
		switch (v->filterType) {
		case FILTER_MOOG:
			Moog_init(&v->filter.moog);
			break;
		case FILTER_TEEMU_TR_LADDER_0:
		case FILTER_TEEMU_TR_LADDER:
			Tladder_init(&v->filter.tladder, param[pb+FILTER_MOOG_MODE].dspVal);
			break;
		case FILTER_ANDY_SVF:
			ASSVF_init(&v->filter.assvf, param[pb+FILTER_SVF_MODE].dspVal);
			break;
		}
	}
	v->filterEnv.state = ATTACK;
	v->ampEnv.state = ATTACK;
}

static void initDrumVoice(RomplerVoice *v, RomplerSample *d, uint8_t velocity) {
	v->amplitude = (float)velocity * ONE_OVER_128;
	Rompler_initVoice(v, d, param[d->paramBase+D_TUNE].dspVal);
}

// overrides a weak function in midi.c
void Midi_HandleNoteOn(uint8_t channel, uint8_t key, uint8_t velocity) {
	if (velocity == 0) {
		Midi_HandleNoteOff(channel, key, velocity);
		return;
	}
	if (channel == settings.midi.synthChannel) {
		int i;
		int best = 0;
		float env = 1.f;
		for (i = 0; i < N_SYNTH_VOICES; i++) {
			ADSR *e = &voice[i].ampEnv;
			if (e->state == COMPLETE) {
				best = i;
				break;		// use a free voice if available
			}
			if ((e->state > SUSTAIN) && (e->value < env)) {
				env = e->value;
				best = i;	// steal the quietest voice
			}
		}
		initSynthVoice(&voice[best], key, velocity, POLY);
	} else if (channel == settings.midi.bassSynthChannel) {
		bassKey = key;
		initSynthVoice(&bass, key, velocity, BASS);
	} else if (channel == settings.midi.leadSynthChannel) {
		leadKey = key;
		initSynthVoice(&lead, key, velocity, LEAD);
	} else if (channel == settings.midi.drumChannel) {
		RomplerSample *d = s4n(key);
		if (d && d->org) {
			int i;
			int best = -1;
			for (i = 0; i < N_DRUM_VOICES; i++) {
				if (drumVoice[i].key == key) {
					best = i;	// reuse already playing sample
					break;
				}
			}
			if (best < 0) {
				for (i = 0; i < N_DRUM_VOICES; i++) {
					if (!drumVoice[i].active) {
						best = i;
						break;
					}
				}
			}
			if (best < 0) best = 0; // TODO better stealing
			initDrumVoice(&drumVoice[best], d, velocity);
		}
	}
	//	printf("N: %u\n", (unsigned)key);
}

// overrides a weak function in midi.c
void Midi_HandleNoteOff(uint8_t channel, uint8_t key, uint8_t velocity) {
	if (channel == settings.midi.synthChannel) {
		int i;
		for (i = 0; i < N_SYNTH_VOICES; i++) {
			SynthVoice *v = &voice[i];
			if (v->key == key && !v->released) {
				v->ampEnv.state = v->filterEnv.state = RELEASE;
				v->released = 1;
				return;
			}
		}
	} else if (channel == settings.midi.bassSynthChannel) {
		if (key == bassKey) {
			bass.ampEnv.state = bass.filterEnv.state = RELEASE;
			bass.released = 1;
		}
	} else if (channel == settings.midi.leadSynthChannel) {
		if (key == leadKey) {
			lead.ampEnv.state = lead.filterEnv.state = RELEASE;
			lead.released = 1;
		}
	}
}

void Midi_HandleAllNotesOff(void) {
	int i;
	for (i = 0; i < N_SYNTH_VOICES; i++) {
		SynthVoice *v = &voice[i];
		if (v->ampEnv.state != COMPLETE) {
			v->ampEnv.state = RELEASE;
		}
	}
	if (bass.ampEnv.state != COMPLETE) {
		bass.ampEnv.state = RELEASE;
	}
	if (lead.ampEnv.state != COMPLETE) {
		lead.ampEnv.state = RELEASE;
	}
}

// overrides a weak function in midi.c
int Midi_ChannelHandled(int channel) {
	return channel == settings.midi.synthChannel ||
		   channel == settings.midi.bassSynthChannel ||
		   channel == settings.midi.leadSynthChannel ||
		   channel == settings.midi.drumChannel;
}

void cacheSamples(void) {
	int i;
	for (i = 0; i < N_DRUM_VOICES; i++) {
		if (!drumVoice[i].active) continue;
		Rompler_cacheVoice(&drumVoice[i]);
	}
}

void printVoiceData(void) {
	int i;
	printf("Poly Voices: ");
	for (i = 0; i < N_SYNTH_VOICES; i++) {
		SynthVoice *v = &voice[i];
		printf("%d=%c ", v->key, ADSR_STATE(v->ampEnv.state));
	}
	printf("\n");
	printf("Bass Voice: %d=%c\n", bass.key, ADSR_STATE(bass.ampEnv.state));
	printf("Lead Voice: %d=%c\n", lead.key, ADSR_STATE(lead.ampEnv.state));
}
