/*
 * background.c
 *
 * Provides App_Background() and all related background features.
 *
 *  Created on: 19 Mar 2013
 *      Author: st
 */

#include "synth1.h"
#include <core_cm4.h>	// for NVIC_SystemReset()
#include <debug_led.h>
#include <stm32f4xx_rtc.h>
#include <ctype.h>

extern char *getcleanline(void);
extern void cacheSamples(void);
#if USE_USBH_MSC
extern void USB_MSC_Process(void);
extern void USB_MSC_Debug(void);
#endif

#if USE_AUDIO
static void printAudioInitData(void) {
	AudioSettings *as = &settings.audio;
	printf("Audio: requested %uHz %u frames\n",
			(unsigned)as->sampleRate,
			(unsigned)as->nFrames);
	printf("Audio: actually %uHz %u frames\n",
			(unsigned)as->actualSampleRate,
			(unsigned)as->actualNFrames);
	printf("Audio: %f%% error in sample rate\n",
			100.f*(as->sampleRate -
				   as->actualSampleRate) /
				   as->sampleRate);
}

#define PEAK_MIN -200.f

static void printAudioPeakdB(void) {
	printf(" peak level %.2fdB\n", 20.f*log10f(perfData.max));
	perfData.max = 0.f;
}

#if USE_PERF
static void printAudioPerfData(void) {
	printf("Audio: %u blocks processed\n", (unsigned)perfData.nAudioProcess);
	printf("Audio: %u AIN callbacks\n", (unsigned)perfData.nAInCallback);
	if (perfData.nAudioProcess > 0) {
		printf("Audio: period %u, duration %u, load %.2f%%, peak load %u%%,",
			(unsigned)perfData.tAPper,
			(unsigned)perfData.tAPdur,
			(100.f*perfData.tAPdur/perfData.tAPper),
			(unsigned)(100.f*perfData.tAPdurMax/perfData.tAPper));
		perfData.tAPdurMax = 0;
		printAudioPeakdB();
		Reverb_printLevels();
	}
	Audio_PrintData();
}
#endif // USE_PERF
#endif // USE_AUDIO

#if USE_MIDI
#if USE_PERF
static void printMidiPerfData(void) {
	Midi_PrintData();
	printf("MIDI messages: %u Consumed\n", (unsigned)perfData.nMidiIn);
	printf("MIDI synth channel: %u\n", settings.midi.synthChannel);
	printf("MIDI bass synth channel: %u\n", settings.midi.bassSynthChannel);
	printf("MIDI lead synth channel: %u\n", settings.midi.leadSynthChannel);
	printf("MIDI drum channel: %u\n", settings.midi.drumChannel);
}
#endif // USE_PERF
#endif // USE_MIDI

static void printInitData(void) {
#if USE_AUDIO
	printAudioInitData();
#endif
}

static void printPerfData(void) {
#if USE_PERF
#if USE_AUDIO
	printAudioPerfData();
#endif
#if USE_MIDI
	printMidiPerfData();
#endif
#endif // USE_PERF
#if USE_ANALOG_IN
	ADC_printData();
#endif
#if USE_AUDIO
	printVoiceData();
#endif
}

static void printHelp(void) {
	printf("Commands:\n");
	printf("\t!\t\t\t\t(reset Sybo)\n");
#if USE_AUDIO
	printf("\t!<nframes> [<sampleRate>]\t(set <nframes> and perhaps <sampleRate> and reset Sybo)\n");
#endif
	printf("\t?\t\t\t\t(display all data)\n");
	printf("\t.\t\t\t\t(list all parameters)\n");
	printf("\t.<paramAbbrev>[=<value>]\t(get/set parameter value)\n");
	printf("\tpsave .<paramAbbrev> <file>\t(save parameters to file)\n");
	printf("\tpload <file>\t\t\t(load parameters from file)\n");
#if USE_ANALOG_IN
	printf("\tpmap <index> <paramAbbrev>|-\t(map control to parameter, or to nothing)\n");
#endif
	printf("\tkitload <file>\t\t\t(load a kit file\n");
	printf("\tkitpiece <note> <file>\t\t(load a sample for a note)\n");
#if USE_SEQ_CLOCK
	printf("\tplay\t\t\t\t(start playing)\n");
	printf("\tstop\t\t\t\t(start playing)\n");
	printf("\tbpm[=value]\t\t\t(get/set bpm)\n");
	printf("\tclk[=i|e]\t\t\t(get/set clock source internal/external)\n");
#endif
#if USE_FAT_FS
	printf("\tls [<directory>]\t\t(list the directory contents)\n");
	printf("\tdu [<directory>]\t\t(list disk usage recursively)\n");
	printf("\tcd <directory>\t\t\t(change to the directory)\n");
	printf("\tcat <file>\t\t\t(print file to standard output)\n");
	printf("\tmd [<directory>]\t\t(create a directory)\n");
	printf("\trm [<file>|<directory>]\t\t(delete the file or empty directory)\n");
#if USE_USBH_MSC
	printf("\tdrv 0|1\t\t\t\t(change drive)\n");
#endif
#endif
	printf("\ttime [hh:mm:ss]\t\t\t(get/set time)\n");
	printf("\tdate [yyyy/mm/dd]\t\t(get/set date)\n");
}

static void printTime(void) {
	RTC_TimeTypeDef RTC_TimeStruct;
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStruct);
	printf("%2d:%02d:%02d\n", RTC_TimeStruct.RTC_Hours,
			RTC_TimeStruct.RTC_Minutes, RTC_TimeStruct.RTC_Seconds);
}

static void printDate(void) {
	RTC_DateTypeDef RTC_DateStruct;
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	printf("20%d/%d/%d\n", RTC_DateStruct.RTC_Year,
			RTC_DateStruct.RTC_Month, RTC_DateStruct.RTC_Date);
}

static void setTime(char *t) {
	RTC_TimeTypeDef RTC_TimeStruct;
	unsigned h, m, s;
	sscanf(t, "%u:%u:%u", &h, &m, &s);
	if (h > 23) h = 23;
	if (m > 59) m = 59;
	if (s > 59) s = 59;
	RTC_TimeStruct.RTC_Hours = h;
	RTC_TimeStruct.RTC_Minutes = m;
	RTC_TimeStruct.RTC_Seconds = s;
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStruct);
	printTime();
}

// http://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week#Implementation-dependent_methods_of_Sakamoto.2C_Lachman.2C_Keith_and_Craver
static int dow(int y, int m, int d) {
	static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	y -= m < 3;
	int wd = (y + y/4 - y/100 + y/400 + t[m-1] + d) % 7;
	return wd ? wd : 7;
}

static void setDate(char *t) {
	RTC_DateTypeDef RTC_DateStruct;
	unsigned y, m, d;
	sscanf(t, "%u/%u/%u", &y, &m, &d);
	RTC_DateStruct.RTC_WeekDay = dow(y, m ,d);
	if (y > 2000) y -= 2000;
	if (y > 99) y = 13;
	if (m < 1 || m > 12) m = 4;
	if (d < 1 || d > 31) d = 24;
	RTC_DateStruct.RTC_Year = y;
	RTC_DateStruct.RTC_Month = m;
	RTC_DateStruct.RTC_Date = d;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
	printDate();
}

// remove leading and trailing spaces from e.g. filenames
static char *trim(char *c) {
	char *e = c + strlen(c) - 1;		// char prior to \0 termination
	while ((e > c) && isspace(*e)) {
		e--;
	}
	*++e = '\0';
	while ((c < e) && isspace(*c)) {
		c++;
	}
	return c;
}

static void SyboShell(void) {
	char *c;
#if USE_FAT_FS
	char sbuf[256];
#endif
	printInitData();
	printHelp();
	do {
#if USE_FAT_FS
		printf("%s", FAT_cwd());
		sbuf[0] = 0;
#endif
		printf(">");
		c = getcleanline();
		printf("\n");
		if (*c == '.') {
#if USE_AUDIO
			if (strlen(c) < 2) {
				Param_PrintAll();
			} else {
				Param_ParseUserVal(++c);
			}
#endif
		} else if (*c == '!') {
#if USE_AUDIO
			sscanf(c, "!%u %u",
					(unsigned *) &settings.audio.nFrames,
					(unsigned *) &settings.audio.sampleRate);
#endif
			printf("Resetting...\n");
			NVIC_SystemReset();
		} else if (*c == '?') {
			printPerfData();
		} else if (strncmp(c, "psave ", 6) == 0) {
			char abbrev[10], *sub = c + 6;
			if (sub[0] == '.' && isspace(sub[1])) {
				Param_Save("", trim(sub+2));
			} else if (sscanf(sub, ".%s", abbrev) == 1) {
				Param_Save(abbrev, trim(sub+1+strlen(abbrev)+1));
			}
		} else if (strncmp(c, "pload ", 6) == 0) {
			char *sub = c + 6;
			Param_Load(sub + 6 + 1);
#if USE_ANALOG_IN
		} else if (strncmp(c, "pmap ", 5) == 0) {
			int n;
			char abbrev[10];
			if (sscanf(c+5, "%d %s", &n, abbrev) == 2) {
				Parameter *p = abbrev[0] == '-' ? 0 : Param_fromAbbreviation(abbrev);
				if (n >= 0 && n < N_ANALOG_IN) {
					paramMap[n] = p;
					printf("mapped control %d to %s\n", n, abbrev);
				}
			}
#endif
		} else if (strncmp(c, "kit", 3) == 0) {
			char *sub = c + 3;
			if (strncmp(sub, "load", 4) == 0) {
				DrumKit_Load(trim(sub+4+1));
			} else if (strncmp(sub, "piece", 5) == 0) {
				DrumKit_ParseLine(trim(sub+5+1));
			}
#if USE_USBH_MSC
		} else if (*c == '#') {
			USB_MSC_Debug();
#endif
		} else if (strncmp(c, "time", 4) == 0) {
			if (strlen(c) > 5) setTime(c+5); else printTime();
		} else if (strncmp(c, "date", 4) == 0) {
			if (strlen(c) > 5) setDate(c+5); else printDate();
#if USE_SEQ_CLOCK
		} else if (strcmp(c, "play") == 0) {
			StepSeq_Play();
			printf("%s\n", StepSeq_isPlaying() ? "playing" : "stopped");
		} else if (strcmp(c, "stop") == 0) {
			StepSeq_Stop();
			printf("%s\n", StepSeq_isPlaying() ? "playing" : "stopped");
		} else if (strncmp(c, "bpm", 3) == 0) {
			char *e = strchr(c, '='); // null if accessor
			if (e) {
				float bpm;
				sscanf(++e, "%f", &bpm);
				SeqClock_setBPM(bpm);
			}
			printf("%.1f beats per minute\n", SeqClock_getBPM());
		} else if (strncmp(c, "clk", 3) == 0) {
			char *e = strchr(c, '='); // null if accessor
			if (e++) {
				SeqClock_setExternalClock(*e != 'i');
			}
			printf("%s clock\n",
					SeqClock_isExternalClock() ? "external" : "internal");
#endif
#if USE_FAT_FS
		} else if (strncmp(c, "ls", 2) == 0) {
			int n = sscanf(c, "ls %250s", sbuf);
			FAT_ls(n ? sbuf : 0);
		} else if (strncmp(c, "du", 2) == 0) {
			int n = sscanf(c, "du %250s", sbuf);
			FAT_du(n ? sbuf : 0);
		} else if (strncmp(c, "cd ", 3) == 0) {
			if (strlen(c) > 3) {
				FAT_cd(c+3);
			}
		} else if (strncmp(c, "md ", 3) == 0) {
			if (strlen(c) > 3) {
				FAT_md(c+3);
			}
		} else if (strncmp(c, "cat ", 4) == 0) {
			if (strlen(c) > 4) {
				FAT_cat(c+4);
			}
		} else if (strncmp(c, "rm ", 3) == 0) {
			if (strlen(c) > 3) {
				FAT_rm(c+3);
			}
		} else if (strncmp(c, "drv", 3) == 0) {
			int drv;
			if (sscanf(c, "drv %d", &drv)) {
				FAT_drv(drv);
			}
#endif
		} else {
			printf("Command not found\n");
		}
	} while (1);
}

#if DEBUG_MIDI
static void debugMidi(void) {
	// Consume MIDI Messages
	while (Midi_Available()) {
		MidiMessage msg = Midi_Consume();
		uint8_t status = msg.status;
		if (status == ACTIVE_SENSING) continue;
		printf("M: %X %d %d\n", status, msg.val1, msg.val2);
#if USE_PERF
		perfData.nMidiIn += 1;
#endif
		printMidiPerfData();
	}
}
#endif

static int idleCount = 0;
// override a weak function from the background busy loop
void Idle_Process(void) {
	cacheSamples();
#if USE_USBH_MSC
	USB_MSC_Process();
#endif
	idleCount++;
}

// flash led on/off with a period of 1 second
void SysTick_Handler(void) {
	static int lastIdleCount = -1;
	static int cnt = 50;
	static int on = 1;
	if (--cnt == 0) {
		DebugLed_set(1, on);
		on = !on;
		cnt = 50;
		if ((idleCount == lastIdleCount) && on) {
#if USE_USBH_MSC
			USB_MSC_Debug();	// once per second
#endif
		}
		lastIdleCount = idleCount;
	}
#if !USE_USB && !USE_SERIAL
	// ensure usb sticks recognised even without a shell
	Idle_Process();
#endif
}

/*
 * Application background code, called repeatedly if you return
 * e.g. user interface
 */
void App_Background(void) {
	SysTick_Config(SystemCoreClock / 100); // 10ms SysTick
#if DEBUG_MIDI
	debugMidi();
#elif USE_USB || USE_SERIAL
	SyboShell();
#endif
	// must not return!
}

#if USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line) {
	printf("Assertion failed: file %s line %d\r\n", (char *) file,
			(unsigned) line);
	while (1) {
	}
}
#endif

