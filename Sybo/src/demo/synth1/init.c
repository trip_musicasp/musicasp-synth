/*
 * init.c
 *
 * Provides App_Init() and all related initialisation features
 *
 *  Created on: 22 Mar 2013
 *      Author: st
 */

#include "synth1.h"
#include <backup.h>

#if USE_ANALOG_IN
static AnalogInput controls[N_ANALOG_IN]; 	// array of analog in configurations
#endif
#if USE_FAT_FS
void FAT_Init(void);
#endif
#if USE_USB
void USBD_CDC_Init(void);
#endif
#if USE_USBH_MSC
void USB_MSC_Init(void);
#endif

// we don't initialise this here because it is backed up by battery!
// by only using 1 NON_VOLATILE data structure, containing all other
// data structures that need to be non-volatile, we ensure that they
// remain at the same addresses.
Settings settings NON_VOLATILE;

static float samplesPerMilli;

static float linCoeff(float millis) {
	float ns = millis * samplesPerMilli;
	return 1.f / ns;
}

// return per sample coefficient for 99% in specified milliseconds
static float expCoeff(float millis) {
	float ns = millis * samplesPerMilli;
	float k = -4.60517018599f / ns; // k, per sample
	return 1.f - expf(k);
}

static const float FACTOR2 = 1.f / 20.0f;

static float dB2lin(float dLogarithmic) {
	return powf(10.0f, dLogarithmic * FACTOR2);
}

static float detuneCoeff(float x) {
	return x * x * x;					// TODO
}

#if 0
static unsigned midiChannel(float chan) {
	return (unsigned)chan - 1;
}
#endif

static ControlLaw oscTypeLaw = { 0.f, N_OSC_TYPES-1, 0,
		&Param_DiscreteConverter, 0, Osc_TypeName};
static ControlLaw oscShapeLaw = { 0.f, N_OSC_SHAPES-1, 0,
		&Param_DiscreteConverter, 0, Osc_ShapeName};
static ControlLaw filterTypeLaw = { 0.f, N_FILTER_TYPES-1, 0,
		&Param_DiscreteConverter, 0, Filter_TypeName};
static ControlLaw moogModeLaw = { 0.f, N_MOOG_MODES-1, 0,
		&Param_DiscreteConverter, 0, Moog_ModeName};
static ControlLaw svfModeLaw = { 0.f, N_SVF_MODES-1, 0,
		&Param_DiscreteConverter, 0, SVF_ModeName};
static ControlLaw algoLaw = { 0.f, N_ALGOS-1, 0,
		&Param_DiscreteConverter, 0, Reverb_AlgorithmName};
// define a lin control law from -96 to +96 semitones
static ControlLaw tuningLaw = { -96.f, 96.f, "st", &Param_LinConverter, 0 };
// define a lin control law from -99dB to 12dB TODO proper off
static ControlLaw gainLaw = { -99.f, 12.f, "dB", &Param_LinConverter, dB2lin };
// define a log control law from 1ms to 10000ms for exponential coefficients
static ControlLaw timeLaw = { 1.f, 10000.f, "ms", &Param_LogConverter, expCoeff };
// define a lin control law from 1ms to 10000ms for linear coefficients
static ControlLaw attackLaw = { 1.f, 10000.f, "ms", &Param_LogConverter, linCoeff };
// define a lin control law for the number of oscillator partials
// a null final parameter causes the dsp value to equal the user value
static ControlLaw npartialLaw = { 1.f, 1000.f, 0, &Param_DiscreteConverter, 0 };
// a pitch shift law
static ControlLaw pitchLaw = { 0.25f, 4.f, "", &Param_LinConverter, 0 };
static ControlLaw rateLaw = { 0.1f, 10.f, "Hz", &Param_LinConverter, 0 };
static ControlLaw detuneLaw = { 0.f, 1.f, "", &Param_LinConverter, detuneCoeff };
static ControlLaw keyFollowLaw = { -1.f, 1.f, "", &Param_LinConverter, 0 };
//static const ControlLaw midiChannelLaw = { 1.f, 16.f, "", &Param_LinConverter, midiChannel };

#define POT	OSC_DSF
#define PFT FILTER_MOOG

#define BOT	OSC_TRIPLE
#define BFT	FILTER_ANDY_SVF

#define LOT	OSC_TRIPLE
#define LFT	FILTER_TEEMU_TR_LADDER_0

// Initial abbrevation characters:
// P (Poly synth), B (Bass synth), L (Lead synth), RV (Reverb), F (Fx)
// FP (Fx Phaser)
static void initParams(void) {
	// ---- Synth Parameters ------------------------------------------------
	Param_Init(&param[POLY+OSC_TYPE],			"POT",    POT, &oscTypeLaw);
	Param_Init(&param[POLY+OSC1_SHAPE],			"P1S",    0.f, &oscShapeLaw);
	Param_Init(&param[POLY+OSC2_SHAPE],			"P2S",    0.f, &oscShapeLaw);
	Param_Init(&param[POLY+OSC2_TUNE],			"P2T",1.0075f, &pitchLaw);
	Param_Init(&param[POLY+OSC2_LEVEL],			"P2L",    0.f, &gainLaw);
	Param_Init(&param[POLY+OSC3_SHAPE],			"P3S",    0.f, &oscShapeLaw);
	Param_Init(&param[POLY+OSC3_TUNE],			"P3T",0.4968f, &pitchLaw);
	Param_Init(&param[POLY+OSC3_LEVEL],			"P3L",    0.f, &gainLaw);
	Param_Init(&param[POLY+OSC_SS_DETUNE],		"PSD",	 0.5f, &detuneLaw);
	// a null ControlLaw causes the raw, user and dsp values to all be 0..1
	Param_Init(&param[POLY+OSC_SS_MIX],			"PSM",	0.75f, 0);
	Param_Init(&param[POLY+OSC_N_PARTIALS], 	"PON",  250.f, &npartialLaw);
	Param_Init(&param[POLY+OSC_ROLLOFF],	 	"POR",  0.92f, 0);
	Param_Init(&param[POLY+FILTER_TYPE],		"PFT",	  PFT, &filterTypeLaw);
	Param_Init(&param[POLY+FILTER_MOOG_MODE],	"PFM",	  0.f, &moogModeLaw);
	Param_Init(&param[POLY+FILTER_SVF_MODE],	"PFV",	  0.f, &svfModeLaw);
	Param_Init(&param[POLY+FILTER_ENV_DEPTH],	"PFE",   48.f, &tuningLaw);
	Param_Init(&param[POLY+FILTER_ENV_ATTACK],	"PFA",   20.f, &attackLaw);
	Param_Init(&param[POLY+FILTER_ENV_DECAY], 	"PFD", 4000.f, &timeLaw);
	Param_Init(&param[POLY+FILTER_ENV_SUSTAIN],	"PFS",    0.f, 0);
	Param_Init(&param[POLY+FILTER_ENV_RELEASE],	"PFR", 1000.f, &timeLaw);
	Param_Init(&param[POLY+FILTER_CUTOFF],		"PFC", 	 -3.f, &tuningLaw);
	Param_Init(&param[POLY+FILTER_RES], 		"PFQ",   0.3f, 0);
	Param_Init(&param[POLY+FILTER_KEY_FOLLOW],	"PFK",	 0.7f, &keyFollowLaw);
	Param_Init(&param[POLY+AMP_ENV_ATTACK],	 	"PAA",   30.f, &attackLaw);
	Param_Init(&param[POLY+AMP_ENV_DECAY], 		"PAD",  200.f, &timeLaw);
	Param_Init(&param[POLY+AMP_ENV_SUSTAIN], 	"PAS",   0.5f, 0);
	Param_Init(&param[POLY+AMP_ENV_RELEASE], 	"PAR",  200.f, &timeLaw);
	Param_Init(&param[POLY+AMP_LEVEL], 			"PAL",  -18.f, &gainLaw);
	Param_Init(&param[PHASER_RATE],				"FPR",	0.33f, &rateLaw);
	Param_Init(&param[PHASER_DEPTH],			"FPD",	 0.3f, 0);
	Param_Init(&param[PHASER_FEEDBACK],			"FPF",	  0.f, 0);
	Param_Init(&param[POLY+REVERB_SEND],		"PRS",  -30.f, &gainLaw);
	// ---- Bass Synth Parameters -------------------------------------------
	Param_Init(&param[BASS+OSC_TYPE],			"BOT",    BOT, &oscTypeLaw);
	Param_Init(&param[BASS+OSC1_SHAPE],			"B1S",    0.f, &oscShapeLaw);
	Param_Init(&param[BASS+OSC2_SHAPE],			"B2S",    0.f, &oscShapeLaw);
	Param_Init(&param[BASS+OSC2_TUNE],			"B2T",1.0075f, &pitchLaw);
	Param_Init(&param[BASS+OSC2_LEVEL],			"B2L",    0.f, &gainLaw);
	Param_Init(&param[BASS+OSC3_SHAPE],			"B3S",    0.f, &oscShapeLaw);
	Param_Init(&param[BASS+OSC3_TUNE],			"B3T",0.4968f, &pitchLaw);
	Param_Init(&param[BASS+OSC3_LEVEL],			"B3L",    0.f, &gainLaw);
	Param_Init(&param[BASS+OSC_SS_DETUNE],		"BSD",	 0.5f, &detuneLaw);
	Param_Init(&param[BASS+OSC_SS_MIX],			"BSM",	  0.f, 0);
	Param_Init(&param[BASS+OSC_N_PARTIALS], 	"BON",  250.f, &npartialLaw);
	Param_Init(&param[BASS+OSC_ROLLOFF],	 	"BOR",   0.9f, 0);
	Param_Init(&param[BASS+FILTER_TYPE],		"BFT",	  BFT, &filterTypeLaw);
	Param_Init(&param[BASS+FILTER_MOOG_MODE],	"BFM",	  0.f, &moogModeLaw);
	Param_Init(&param[BASS+FILTER_SVF_MODE],	"BFV",	  0.f, &svfModeLaw);
	Param_Init(&param[BASS+FILTER_ENV_DEPTH], 	"BFE",   30.f, &tuningLaw);
	Param_Init(&param[BASS+FILTER_ENV_ATTACK], 	"BFA",   20.f, &attackLaw);
	Param_Init(&param[BASS+FILTER_ENV_DECAY], 	"BFD", 1000.f, &timeLaw);
	Param_Init(&param[BASS+FILTER_ENV_SUSTAIN],	"BFS",   0.1f, 0);
	Param_Init(&param[BASS+FILTER_ENV_RELEASE],	"BFR",  500.f, &timeLaw);
	Param_Init(&param[BASS+FILTER_CUTOFF], 		"BFC",  -12.f, &tuningLaw);
	Param_Init(&param[BASS+FILTER_RES],			"BFQ",   0.3f, 0);
	Param_Init(&param[BASS+FILTER_KEY_FOLLOW],	"BFK",	  0.f, &keyFollowLaw);
	Param_Init(&param[BASS+AMP_ENV_ATTACK], 	"BAA",    1.f, &attackLaw);
	Param_Init(&param[BASS+AMP_ENV_DECAY], 		"BAD",  200.f, &timeLaw);
	Param_Init(&param[BASS+AMP_ENV_SUSTAIN], 	"BAS",   1.0f, 0);
	Param_Init(&param[BASS+AMP_ENV_RELEASE], 	"BAR",  200.f, &timeLaw);
	Param_Init(&param[BASS+AMP_LEVEL], 			"BAL",  -15.f, &gainLaw);
	Param_Init(&param[BASS+REVERB_SEND],		"BRS",	-99.f, &gainLaw);
	// ---- Lead Synth Parameters -------------------------------------------
	Param_Init(&param[LEAD+OSC_TYPE],			"LOT",    LOT, &oscTypeLaw);
	Param_Init(&param[LEAD+OSC1_SHAPE],			"L1S",    0.f, &oscShapeLaw);
	Param_Init(&param[LEAD+OSC2_SHAPE],			"L2S",    0.f, &oscShapeLaw);
	Param_Init(&param[LEAD+OSC2_TUNE],			"L2T",1.0075f, &pitchLaw);
	Param_Init(&param[LEAD+OSC2_LEVEL],			"L2L",    0.f, &gainLaw);
	Param_Init(&param[LEAD+OSC3_SHAPE],			"L3S",    0.f, &oscShapeLaw);
	Param_Init(&param[LEAD+OSC3_TUNE],			"L3T",0.4968f, &pitchLaw);
	Param_Init(&param[LEAD+OSC3_LEVEL],			"L3L",    0.f, &gainLaw);
	Param_Init(&param[LEAD+OSC_SS_DETUNE],		"LSD",	 0.9f, &detuneLaw);
	Param_Init(&param[LEAD+OSC_SS_MIX],			"LSM",	  0.f, 0);
	Param_Init(&param[LEAD+OSC_N_PARTIALS], 	"LON",  250.f, &npartialLaw);
	Param_Init(&param[LEAD+OSC_ROLLOFF],	 	"LOR",   0.9f, 0);
	Param_Init(&param[LEAD+FILTER_TYPE],		"LFT",	 LFT, &filterTypeLaw);
	Param_Init(&param[LEAD+FILTER_MOOG_MODE],	"LFM",	  0.f, &moogModeLaw);
	Param_Init(&param[LEAD+FILTER_SVF_MODE],	"LFV",	  0.f, &svfModeLaw);
	Param_Init(&param[LEAD+FILTER_ENV_DEPTH], 	"LFE",   96.f, &tuningLaw);
	Param_Init(&param[LEAD+FILTER_ENV_ATTACK], 	"LFA",  700.f, &attackLaw);
	Param_Init(&param[LEAD+FILTER_ENV_DECAY], 	"LFD", 2000.f, &timeLaw);
	Param_Init(&param[LEAD+FILTER_ENV_SUSTAIN],	"LFS",   0.3f, 0);
	Param_Init(&param[LEAD+FILTER_ENV_RELEASE],	"LFR", 3000.f, &timeLaw);
	Param_Init(&param[LEAD+FILTER_CUTOFF], 		"LFC",  -12.f, &tuningLaw);
	Param_Init(&param[LEAD+FILTER_RES],			"LFQ",   0.2f, 0);
	Param_Init(&param[LEAD+FILTER_KEY_FOLLOW],	"LFK",	 0.0f, &keyFollowLaw);
	Param_Init(&param[LEAD+AMP_ENV_ATTACK], 	"LAA",   50.f, &attackLaw);
	Param_Init(&param[LEAD+AMP_ENV_DECAY], 		"LAD",  200.f, &timeLaw);
	Param_Init(&param[LEAD+AMP_ENV_SUSTAIN], 	"LAS",   0.5f, 0);
	Param_Init(&param[LEAD+AMP_ENV_RELEASE], 	"LAR",  500.f, &timeLaw);
	Param_Init(&param[LEAD+AMP_LEVEL], 			"LAL",   -9.f, &gainLaw);
	Param_Init(&param[LEAD+REVERB_SEND],		"LRS",	 -6.f, &gainLaw);
	// ---- Drum Parameters -------------------------------------------------
	int i;
	// malloc has 16 byte granularity plus overhead so subdivide one malloc
	// saves 684 bytes of heap over naive individual malloc(5)
	char *abb = malloc(15 * GM_RANGE);
	for (i = GM_LOWEST; i < GM_HIGHEST+1; i++) {
		unsigned pb = DRUM_PARAM(i);
		snprintf(abb, 5, "D%02dL", i);
		Param_Init(&param[pb+D_LEVEL],	 		  abb,   -6.f, &gainLaw);
		abb += 5;
		snprintf(abb, 5, "D%02dT", i);
		Param_Init(&param[pb+D_TUNE],			  abb,   1.0f, &pitchLaw);
		abb += 5;
		snprintf(abb, 5, "D%02dSS", i);
		Param_Init(&param[pb+D_SEND],			  abb,	-99.f, 	&gainLaw);
		abb += 5;
	}
	// ---- Reverb Parameters -----------------------------------------------
	Param_Init(&param[REVERB_ALGO],				"RVA",	  0.f, 	&algoLaw);
	Param_Init(&param[REVERB_PRE_DELAY],		"RVP",	 1.0f, 	0);
	Param_Init(&param[REVERB_BANDWIDTH],		"RVB",	 1.0f, 	0);
	Param_Init(&param[REVERB_DECAY_TIME],		"RVT",	 0.5f, 	0);
	Param_Init(&param[REVERB_DAMPING_FILTER],	"RVF",	0.20f, 	0);
	Param_Init(&param[REVERB_RETURN],			"RVR",	  0.f, 	&gainLaw);

	Param_Link(param, N_PARAMS);
}

// ---- Support for parameter names -----------------------------------------

#define _PHASER(c) 		(c == 'R') ? "Rate" : \
						(c == 'D') ? "Depth" : \
						(c == 'F') ? "Feedback" : "?"
#define _DRUM(c) 		(c == 'L') ? "Level" : \
						(c == 'T') ? "Tuning" : \
						(c == 'S') ? "Send" : "?"
#define _OSC(c) 		(c == 'S') ? "Shape" : \
						(c == 'T') ? "Tuning" : \
						(c == 'L') ? "Level" : "?"
#define _ENV_STAGE(c) 	(c == 'A') ? "Attack" : \
						(c == 'D') ? "Decay" : \
						(c == 'S') ? "Sustain" : \
						(c == 'R') ? "Release" : "?"
#define _SUPER_OSC(c) 	(c == 'D') ? "Detune" : \
						(c == 'M') ? "Mix" : "?"
#define _DSF_OSC(c) 	(c == 'N') ? "Number of partials" : \
						(c == 'R') ? "Rolloff of partials" : "?"
#define _FILTER(c) 		(c == 'C') ? "Cutoff" : \
						(c == 'Q') ? "Resonance" : \
						(c == 'E') ? "Envelope depth" : \
						(c == 'K') ? "Key follow" : \
						(c == 'T') ? "Type" : \
						(c == 'V') ? "SVF Mode" : \
						(c == 'M') ? "Moog Mode" : _ENV_STAGE(c)
#define _AMPLIFIER(c) 	(c == 'L') ? "Level" : _ENV_STAGE(c)
#define _REVERB(c) 		(c == 'R') ? "Return" : \
						(c == 'A') ? "Algorithm" : \
						(c == 'P') ? "Pre delay" : \
						(c == 'B') ? "Bandwidth" : \
						(c == 'T') ? "decay Time" : \
						(c == 'F') ? "damping Filter" : "?"

static void synthParam(char *buf, unsigned len, char b, char c) {
	unsigned l = strlen(buf);
	buf += l;
	len -= l;
	if (b == 'O' && c == 'T') {
		snprintf(buf, len, "Oscillator Type");
	} else if (b == 'R' && c == 'S') {
		snprintf(buf, len, "Reverb Send");
	} else if (b == 'M' && c == 'C') {
		snprintf(buf, len, "MIDI Channel");
	} else if (b == '1') {
		snprintf(buf, len, "(Triple) Oscillator 1 %s", _OSC(c));
	} else if (b == '2') {
		snprintf(buf, len, "(Triple) Oscillator 2 %s", _OSC(c));
	} else if (b == '3') {
		snprintf(buf, len, "(Triple) Oscillator 3 %s", _OSC(c));
	} else if (b == 'S') {
		snprintf(buf, len, "(Supersaw) Oscillator %s", _SUPER_OSC(c));
	} else if (b == 'O') {
		snprintf(buf, len, "(DSF) Oscillator %s", _DSF_OSC(c));
	} else if (b == 'F') {
		snprintf(buf, len, "Filter %s", _FILTER(c));
	} else if (b == 'A') {
		snprintf(buf, len, "Amplifier %s", _AMPLIFIER(c));
	}
}

// override a weak function
void Param_Name(Parameter *p, char *buf, unsigned len) {
	char *a = p->abbrev;
	if (strstr(a, "FP") == a) {
		snprintf(buf, len, "Fx Phaser %s", _PHASER(a[2]));
	} else if (strstr(a, "RV") == a) {
		snprintf(buf, len, "Reverb %s", _REVERB(a[2]));
	} else if (a[0] == 'D') {
		int num;
		sscanf(a, "D%d", &num);
		snprintf(buf, len, "Drum %d %s", num, _DRUM(a[3]));
	} else if (a[0] == 'P') {
		snprintf(buf, len, "Poly synth ");
		synthParam(buf, len, a[1], a[2]);
	} else if (a[0] == 'B') {
		snprintf(buf, len, "Bass synth ");
		synthParam(buf, len, a[1], a[2]);
	} else if (a[0] == 'L') {
		snprintf(buf, len, "Lead synth ");
		synthParam(buf, len, a[1], a[2]);
	}
	buf[len-1] = 0;
}

/**
 * Set non volatile settings first time or if invalid
 */
static void initSettings(void) {
#if USE_AUDIO
	AudioSettings *as = &settings.audio;
	if (as->sampleRate < 44100 || as->sampleRate > 96000) {
		as->sampleRate = 48000;
		printf("Audio: sample rate forced to %uHz\n",
				(unsigned)as->sampleRate);
	}
	if (as->nFrames < MIN_FRAMES || as->nFrames > MAX_FRAMES) {
		as->nFrames = MIN_FRAMES << 2;
		printf("Audio: number of frames forced to %u\n",
				(unsigned)as->nFrames);
	}
#endif
#if USE_MIDI
	MidiSettings *ms = &settings.midi;
	if (ms->synthChannel > 15) {
		ms->synthChannel = 0;
		printf("MIDI: synth channel forced to %u\n",
				(unsigned)ms->synthChannel);
	}
	if (ms->bassSynthChannel > 15 ||
			ms->bassSynthChannel == ms->synthChannel) {
		ms->bassSynthChannel = 1;
		printf("MIDI: bass synth channel forced to %u\n",
				(unsigned)ms->bassSynthChannel);
	}
	if (ms->leadSynthChannel > 15||
			ms->leadSynthChannel == ms->synthChannel||
			ms->leadSynthChannel == ms->bassSynthChannel) {
		ms->leadSynthChannel = 2;
		printf("MIDI: lead synth channel forced to %u\n",
				(unsigned)ms->leadSynthChannel);
	}
	if (ms->drumChannel > 15 ||
			ms->drumChannel == ms->synthChannel||
			ms->drumChannel == ms->bassSynthChannel||
			ms->drumChannel == ms->leadSynthChannel) {
		ms->drumChannel = 10;
		printf("MIDI: drum channel forced to %u\n",
				(unsigned)ms->drumChannel);
	}
#endif
}

void delayMillis(int millis) {
	volatile uint32_t a;
	while (millis--) {
		a = 14000;
		while (a--) {
		}
	}
}

extern void Fault_printData(void);

/*
 * Initialise the application
 */
void App_Init(void) {
	Backup_Init();		// initialise the backup domain to support NON_VOLATILE
	DebugLed_Init();
#if USE_USB
	USBD_CDC_Init(); 	// initialise USB serial port ASAP
	delayMillis(3000);
#endif
#if USE_SERIAL
	SerUart_Init();		// initialise USART serial port ASAP
#endif
	Fault_printData();
#if USE_FAT_FS
	FAT_Init();
#endif
#if USE_USBH_MSC
	USB_MSC_Init(); 	// initialise USB host mass storage class support
#endif
#if USE_PERF
	Perf_Init();		// initialise performance measuring
#endif
#if USE_ANALOG_IN
	controls[0] = AIN_A0;
	controls[1] = AIN_A1;
	controls[2] = AIN_A2;
	controls[3] = AIN_A3;
	AnalogIn_Init(&controls[0], 4);
#endif
	initSettings(); 				// perhaps initialise settings
#if USE_AUDIO
	// sampleRate is accurate enough and required to initParams before Audio_Init
	samplesPerMilli = settings.audio.sampleRate / 1000.f;
	initParams(); 					// initialise parameters
	Audio_Init(&settings.audio);
	Synth_Init(settings.audio.actualSampleRate, settings.audio.actualNFrames);
#if USE_PERF
	perfData.tAPper =
			((SystemCoreClock >> 1) /  settings.audio.actualSampleRate) *
			settings.audio.actualNFrames;
#endif
	Audio_Start(settings.audio.actualNFrames);
#endif
#if USE_MIDI
	Midi_Init(); 		// initialise midi system
	Midi_ThruEnable(1); // enable midi thru
#endif
#if USE_SEQ_CLOCK
	SeqClock_Init();	// enable 48 PPQN sequencer clock
	DemoSeq_Init();
#endif
}

