/*
 * demo_seq.c
 *
 *  Created on: 30 Mar 2013
 *      Author: st
 */

#include "demo_seq.h"

static uint8_t prepareSynth(uint16_t bar, uint8_t teenth, StepSeqLane *l);
static uint8_t prepareMetronome(uint16_t bar, uint8_t teenth, StepSeqLane *l);

#define PON NOTE_ON|0
static MidiMessage synthMsg[6][4] = {
		{{PON, 52, 100}, {PON, 71, 100}, {PON, 76, 100}, {PON, 81, 100}},
		{{PON, 52,   0}, {PON, 71,   0}, {PON, 76,   0}, {PON, 81,   0}},
		{{PON, 53, 100}, {PON, 69, 100}, {PON, 74, 100}, {PON, 79, 100}},
		{{PON, 53,   0}, {PON, 69,   0}, {PON, 74,   0}, {PON, 79,   0}},
		{{PON, 55, 100}, {PON, 67, 100}, {PON, 72, 100}, {PON, 77, 100}},
		{{PON, 55,   0}, {PON, 67,   0}, {PON, 72,   0}, {PON, 77,   0}}
};

#define DON NOTE_ON|10
static MidiMessage metronomeMsg[1] = {{ DON, GM_RIMSHOT, 100 }};

static StepSeqLane synthLane;
static StepSeqLane metronomeLane;

void DemoSeq_Init(void) {
	synthLane.name = "Pad";
	synthLane.burst = 1; // because voices are costly to create
	synthLane.messages = synthMsg[0];
	synthLane.prepare = &prepareSynth;
	StepSeq_addLane(&synthLane);

	synthLane.name = "Metronome";
	metronomeLane.burst = 1;
	metronomeLane.messages = metronomeMsg;
	metronomeLane.prepare = &prepareMetronome;
	StepSeq_addLane(&metronomeLane);
}

static uint8_t prepareMetronome(uint16_t bar, uint8_t teenth, StepSeqLane *l) {
	return ((teenth & 3) == 0) ? 1 : 0;
}

static uint8_t prepareSynth(uint16_t bar, uint8_t teenth, StepSeqLane *l) {
	bar &= 3;	// 4 bar loop
	switch (bar) {
	case 0:
		if (teenth == 0) {
			l->messages = synthMsg[0];
			return 4;
		}
		break;
	case 1:
		if (teenth == 15) {
			l->messages = synthMsg[1];
			return 4;
		}
		break;
	case 2:
		if (teenth == 0) {
			l->messages = synthMsg[2];
			return 4;
		}
		if (teenth == 15) {
			l->messages = synthMsg[3];
			return 4;
		}
		break;
	case 3:
		if (teenth == 0) {
			l->messages = synthMsg[4];
			return 4;
		}
		if (teenth == 15) {
			l->messages = synthMsg[5];
			return 4;
		}
		break;
	}
	return 0;
}
