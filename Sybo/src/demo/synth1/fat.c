/*
 * fat_test.c
 *
 *  Created on: 2 Mar 2013
 *      Author: st
 */
#include <ff.h>
#include <stdio.h>
#include <string.h>
#include "fat.h"
#include <unistd.h>

extern void Idle_Process(void);

static FATFS fsSD;

static char cwd[_MAX_LFN + 1];

static void printkOrM(long long unsigned bytes) {
	if (bytes < 999999) {
		printf("%6u \t", (unsigned)bytes);
		return;
	}
	unsigned k = bytes / 1024;
	if (k > 999999) {
		printf("%6uM\t", k / 1024);
	} else {
		printf("%6uk\t", k);
	}
}

FRESULT scan_files (
    char* path,        /* Start node to be scanned (also used as work area) */
    long long unsigned *pbytes		// pointer to the bytes value for this depth
)
{
    FRESULT res;
    FILINFO fno;
    DIR dir;
    int i;
    char *fn;   /* This function is assuming non-Unicode cfg. */
#if _USE_LFN
    static char lfn[_MAX_LFN + 1];
    fno.lfname = lfn;
    fno.lfsize = sizeof lfn;
#endif


    res = f_opendir(&dir, path);                       /* Open the directory */
    if (res == FR_OK) {
        i = strlen(path);
        for (;;) {
            res = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
            if (fno.fname[0] == '.') continue;             /* Ignore dot entry */
#if _USE_LFN
            fn = *fno.lfname ? fno.lfname : fno.fname;
#else
            fn = fno.fname;
#endif
            if (fno.fattrib & AM_DIR) {             // It is a directory
            	if (pbytes) {
            		long long unsigned rbytes = 0u;	// recursive bytes
            		sprintf(&path[i], "/%s", fn);
                	res = scan_files(path, &rbytes);
                	if (res != FR_OK) break;
                	*pbytes += rbytes;
                	printkOrM(rbytes);
                    printf("%s/\n", path);
                	path[i] = 0;
            	} else {
            		printf("\t%s/\n", fn);
            	}
            } else {                               	// It is a file
            	unsigned bytes = fno.fsize;
            	if (pbytes) {
               		*pbytes += bytes;
            	} else {
            		printkOrM(bytes);
               		printf("%s\n", fn);
             	}
            }
        	Idle_Process();
        }
    }

    return res;
}

void FAT_printError(FRESULT res) {
	char *c;
	switch (res) {
	case FR_DISK_ERR: c = "Unrecoverable disk error"; break;
	case FR_INT_ERR: c = "Internal Error"; break;
	case FR_NOT_READY: c = "disk_initialise() failed or medium removed"; break;
	case FR_NO_FILE: c = "Could not find the file"; break;
	case FR_NO_PATH: c = "Could not find the path"; break;
	case FR_INVALID_NAME: c = "The path name format is invalid"; break;
	case FR_DENIED: c = "Access denied due to prohibited access or directory full"; break;
	case FR_EXIST: c = "Access denied due to prohibited access"; break;
	case FR_INVALID_OBJECT: c = "The file/directory object is invalid"; break;
	case FR_WRITE_PROTECTED: c = "The physical drive is write protected"; break;
	case FR_INVALID_DRIVE: c = "The logical drive number is invalid"; break;
	case FR_NOT_ENABLED: c = "The volume has no work area"; break;
	case FR_NO_FILESYSTEM:	c = "There is no valid FAT volume"; break;
	case FR_MKFS_ABORTED: c = "The f_mkfs() aborted due to any parameter error"; break;
	case FR_TIMEOUT: c = "Could not get a grant to access the volume within defined period"; break;
	case FR_LOCKED: c = "The operation is rejected according to the file sharing policy"; break;
	case FR_NOT_ENOUGH_CORE: c = "LFN working buffer could not be allocated"; break;
	case FR_TOO_MANY_OPEN_FILES: c = "Number of open files > _FS_LOCK"; break;
	case FR_INVALID_PARAMETER: c = "Given parameter is invalid"; break;
	default: printf("Unknown Error %d\n", res); return;
	}
	printf("%s\n", c);
}

char *FAT_cwd(void) {
	return cwd;
}

static void get_cwd(void) {
	FRESULT res = f_getcwd(cwd, _MAX_LFN);
	if (res != FR_OK) {
		FAT_printError(res);
		cwd[0] = 0;
	}
}

void FAT_drv(BYTE drv) {
	FRESULT res = f_chdrive(drv);
	if (res != FR_OK) {
		FAT_printError(res);
	}
	get_cwd();
}

void FAT_cd(char *path) {
	FRESULT res = f_chdir(path);
	if (res != FR_OK) {
		FAT_printError(res);
	}
	get_cwd();
}

void FAT_ls(char *path) {
	FRESULT res = scan_files(path ? path : cwd, 0);
	if (res != FR_OK) {
		FAT_printError(res);
	}
}

void FAT_du(char *path) {
	long long unsigned topbytes = 0u;
	char *p = path ? path : cwd;
	FRESULT res = scan_files(p, &topbytes);
	if (res != FR_OK) {
		FAT_printError(res);
		return;
	}
	printkOrM(topbytes);
    printf("\n");
}

#define BUF_SIZE 80

void FAT_cat(char *path) {
	FRESULT res;
	FIL file;
	char buf[BUF_SIZE+1];

	res = f_open(&file, path, FA_OPEN_EXISTING | FA_READ);
	if (res != FR_OK) {
		FAT_printError(res);
		return;
	}

	while (f_gets(buf, BUF_SIZE, &file) == buf) {
		printf("%s", buf);
		Idle_Process();
	}
	if (f_error(&file)) printf("Error\n");
	f_close(&file);
}

void FAT_md(char *path) {
	FRESULT res = f_mkdir(path);
	if (res != FR_OK) {
		FAT_printError(res);
	}
}

void FAT_rm(char *path) {
	FRESULT res = f_unlink(path);
	if (res != FR_OK) {
		FAT_printError(res);
	}
}

static void init(char* vol) {
	char str[12];
	unsigned long serial;
	FRESULT res = f_getlabel(vol, str, &serial);
	if (res != FR_OK) {
		FAT_printError(res);
		return;
	}
	printf("%s volume label '%s', serial %lu\n", vol, str, serial);
	get_cwd();
}

void FAT_Init(void) {
	if (FR_OK == f_mount(0, &fsSD)) {
		init("0:");
	} else {
		printf("Drive 0: invalid (SD)\n");
	}
}

