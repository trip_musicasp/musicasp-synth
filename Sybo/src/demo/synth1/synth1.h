/*
 * synth1.h
 *
 *  Created on: 17 Feb 2013
 *      Author: st
 */

#ifndef SYNTH1_H_
#define SYNTH1_H_

#include <app.h>
#include <audio.h>
#include <midi.h>
#include <param.h>
#include <ain.h>
#include <perf.h>
#include <rompler.h>
#include <drumkit.h>
#include <osc.h>
#include <DSFosc.h>
#include <polyBLEP.h>
#include <supersaw.h>
#include <filter.h>
#include <adsr.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <s16math.h>
#include <reverb.h>
#include "phaser.h"
#include <step_seq.h>
#include "demo_seq.h"
#include <wav.h>
#include "fat.h"

// order of synth voice parameters relative to base
enum {
	OSC_TYPE,		// e.g Triple, DSF, Super
	OSC1_SHAPE,				// Triple
	OSC2_SHAPE,
	OSC2_TUNE,
	OSC2_LEVEL,
	OSC3_SHAPE,
	OSC3_TUNE,
	OSC3_LEVEL,
	OSC_SS_DETUNE,
	OSC_SS_MIX,
	OSC_N_PARTIALS,			// DSF
	OSC_ROLLOFF,
	FILTER_TYPE,
	FILTER_MOOG_MODE,
	FILTER_SVF_MODE,
	FILTER_ENV_ATTACK,
	FILTER_ENV_DECAY,
	FILTER_ENV_SUSTAIN,
	FILTER_ENV_RELEASE,
	FILTER_ENV_DEPTH,
	FILTER_CUTOFF,
	FILTER_RES,
	FILTER_KEY_FOLLOW,
	AMP_ENV_ATTACK,
	AMP_ENV_DECAY,
	AMP_ENV_SUSTAIN,
	AMP_ENV_RELEASE,
	AMP_LEVEL,
	REVERB_SEND,
//	MIDI_CHANNEL,
	N_VOICE_PARAMS  // must stay last in enum
};

// order of drum voice parameters relative to base
enum {
	D_LEVEL,
	D_TUNE,
	D_SEND,
	//D_PAN
	N_DRUM_PARAMS // must stay last in enum
};

// GM drum note allocations, http://www.midi.org/techspecs/gm1sound.php
enum {
	GM_KICK_A = 35,
	GM_KICK,
	GM_RIMSHOT,
	GM_SNARE_A,
	GM_CLAP,
	GM_SNARE,
	GM_LOW_FLOOR_TOM,
	GM_CLOSED_HAT,
	GM_HIGH_FLOOR_TOM,
	GM_PEDAL_HAT,
	GM_LOW_TOM,
	GM_OPEN_HAT,
	GM_LOW_MID_TOM,
	GM_HIGH_MID_TOM,
	GM_CRASH_CYMBAL,
	GM_HIGH_TOM,
	GM_RIDE_CYMBAL,
	GM_CHINESE_CYMBAL,
	GM_RIDE_BELL,
	GM_TAMBOURINE,
	GM_SPLASH_CYMBAL,
	GM_COWBELL,
	GM_CRASH_CYMBAL_2,
	GM_VIBRASLAP,
	GM_RIDE_CYMBAL_2,
	GM_HIGH_BONGO,
	GM_LOW_BONGO,
	GM_MUTE_HIGH_CONGA,
	GM_OPEN_HIGH_CONGA,
	GM_LOW_CONGA,
	GM_HIGH_TIMBALE,
	GM_LOW_TIMBALE,
	GM_HIGH_AGOGO,
	GM_LOW_AGOGO,
	GM_CABASA,
	GM_MARACAS,
	GM_SHORT_WHISTLE,
	GM_LONG_WHISTLE,
	GM_SHORT_GUIRO,
	GM_LONG_GUIRO,
	GM_CLAVES,
	GM_HIGH_WOODBLOCK,
	GM_LOW_WOODBLOCK,
	GM_MUTE_CUICA,
	GM_OPEN_CUICA,
	GM_MUTE_TRANGLE,
	GM_OPEN_TRIANGLE
};

// we don't have enough RAM to use all GM drums
#define GM_LOWEST	GM_KICK
#define GM_HIGHEST	GM_COWBELL
#define GM_RANGE	(1 + GM_HIGHEST - GM_LOWEST)

// synth, drum and effect parameter bases
enum {
	POLY = 0,
	PHASER_RATE = POLY + N_VOICE_PARAMS,
	PHASER_DEPTH,
	PHASER_FEEDBACK,
	BASS,
	LEAD = BASS + N_VOICE_PARAMS,
	DRUM = LEAD + N_VOICE_PARAMS,
	REVERB_ALGO = DRUM + N_DRUM_PARAMS * GM_RANGE,
	REVERB_PRE_DELAY,
	REVERB_BANDWIDTH,
	REVERB_DECAY_TIME,
	REVERB_DAMPING_FILTER,
	REVERB_RETURN,
	N_PARAMS // must stay last in enum
};

typedef struct {
	PolyBLEPoscillator osc1;
	PolyBLEPoscillator osc2;
	PolyBLEPoscillator osc3;
} OscTriple;

union SynthOscillator {
	OscTriple triple;
	SuperSawOscillator supersaw;
	DSFoscillator dsf;
};

union SynthFilter {
	Moog moog;
	Tladder tladder;
	ASSVF assvf;
};

typedef struct {
	uint8_t key;
	uint8_t oscType;
	uint8_t filterType;
	uint8_t paramBase;
	union SynthOscillator osc;
	union SynthFilter filter;
	ADSR filterEnv;
	ADSR ampEnv;
	float amplitude;
	float amplitudeTarget;
	uint8_t released; 				// for releasing correct keys
} SynthVoice;

typedef struct {
	uint32_t nAudioProcess;
	uint32_t nMidiIn;
	uint32_t nAInCallback;
	uint32_t tAP0;					// entry perf counter
	uint32_t tAPper;				// period perf count
	uint32_t tAPdur;				// duration perf count
	volatile uint32_t tAPdurMax;	// maximum duration
	volatile float max;
	volatile float maxReverbIn;		// prior to conversion to 16 bit
} PerformanceData;

#if USE_PERF
extern PerformanceData perfData;
#endif

#define N_SYNTH_VOICES	4
#define N_DRUM_VOICES	8
#define N_DRUM_SAMPLES	GM_RANGE

#define DRUM_PARAM(n)	(DRUM + (n - GM_LOWEST) * N_DRUM_PARAMS)

typedef struct {
	uint8_t synthChannel;			// N_SYNTH_VOICES
	uint8_t drumChannel;
	uint8_t leadSynthChannel;		// monophonic
	uint8_t bassSynthChannel;		// monophonic
} MidiSettings;

typedef struct {
	AudioSettings audio;
	MidiSettings midi;
} Settings;

extern Settings settings;
extern Parameter param[];
extern Parameter *paramMap[];

// setting this to 1 prints every midi message but gives no sound
#define DEBUG_MIDI 0

void Synth_Init(uint32_t sampleRate, uint32_t nframes);
void printVoiceData(void);

#endif /* SYNTH1_H_ */
