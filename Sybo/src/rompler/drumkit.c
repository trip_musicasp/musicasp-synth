/*
 * drumkit.c
 *
 * Kits cannot be saved because the filenames are not available because
 * they would use too much RAM to store.
 *
 *  Created on: 28 Apr 2013
 *      Author: st
 */

#include <stdint.h>
#include <weak.h>
#include "drumkit.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// should be weak to be overrideable by the app
// the app knows how to map notes to RomplerSamples
void WEAK DrumKit_LoadSample(uint8_t note, char *path) {}

void DrumKit_ParseLine(char *line) {
	unsigned n;
	if (!isdigit((int)*line)) return;
	if (!sscanf(line, "%d", &n)) return;
	DrumKit_LoadSample((uint8_t)n, line + 3);
}

FRESULT DrumKit_Load(char *path) {
	char line[256];
	FIL file;
	// open file
	FRESULT res = f_open(&file, path, FA_OPEN_EXISTING | FA_READ);
	if (res) return res;
	// parse each line to load sample
	while (f_gets(line, 256, &file) == line) {
		char *last = &line[strlen(line)-1];
		if (*last == '\n') {
			*last = '\0';	// remove trailing \n
		}
		DrumKit_ParseLine(line);
		line[0] = '\0';
	}
	// close file
	return f_close(&file);
}

FRESULT DrumKit_LoadDefault(void) {
	return DrumKit_Load(KIT_DEFAULT);
}

