/*
 * rompler.h
 *
 *  Created on: 25 Apr 2013
 *      Author: st
 */

#ifndef ROMPLER_H_
#define ROMPLER_H_

#include <stdint.h>
#include <ff.h>

// must be a power of 2!
#define NSECTORS			8
#define SECTOR_MASK			(NSECTORS-1)
#define SECTOR_WRAP(s)		((s) & SECTOR_MASK)

#define SECTOR_BYTES		512
#define SECTOR_SHORTS		(SECTOR_BYTES >> 1)
#define _OFFSET(pos)		((pos) & (SECTOR_SHORTS-1))
#define _SECTOR(pos)		(((pos) & (SECTOR_MASK << 8)) >> 8)
// that's shifting by 8 (rather than 9) because pos is an index into shorts

typedef struct {
	int16_t s16[SECTOR_SHORTS];
} Sector;

typedef struct {
	uint32_t length;
	float rateConversion;	// sample rate conversion ratio
	DWORD org;				// origin of contiguous sectors
	FIL file;				// the sample file
	Sector *first;			// the first of the contiguous sectors
	uint8_t key;
	uint8_t paramBase;
} RomplerSample;

typedef struct {
	uint32_t length;		// length in frames
	float position;			// current position in frames
	float increment;		// frame increment per sample
	float rIncrement;		// reciprocal of increment to save a realtime /
	float amplitude;
	uint8_t key;
	uint8_t paramBase;
	uint8_t drv;			// the drive the sample file is on
	uint8_t cacheSector;	// sector (of NSECTORS) to be cached next
	DWORD readSector;		// sector to be read next
	Sector sector[NSECTORS];// the sector cache
	volatile uint8_t free[NSECTORS];
	volatile uint8_t active;
	volatile int sjz;		// the sector which is playing
} RomplerVoice;

FRESULT Rompler_initSample(RomplerSample *s, char *path, float samplePeriod,
		uint8_t key, uint8_t paramBase);

void Rompler_initVoice(RomplerVoice *v, RomplerSample *s, float tuningRatio);

void Rompler_processVoice(RomplerVoice *v, int16_t *out, uint32_t nframes);

void Rompler_cacheVoice(RomplerVoice *v);

#endif /* ROMPLER_H_ */
