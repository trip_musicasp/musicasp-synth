/*
 * wav.c
 *
 *  Created on: 2 Apr 2013
 *      Author: st
 */

#include "wav.h"
#include <stdio.h>

extern char *TR909rim;

void WAV_printHeader(WAV_Header *h) {
	printf("Number of channels: %hu\n", h->NumOfChan);
	printf("Sample rate: %lu\n", h->SamplesPerSec);
	printf("Alignment: %hu\n", h->blockAlign);
	printf("Bits per sample: %hu\n", h->bitsPerSample);
	printf("Size: %lu\n", h->Subchunk2Size);
}

void WAV_test(void) {
	WAV_Header *h = (WAV_Header *)&TR909rim;
	printf("WAV pointers: %p %p\n", &TR909rim, h);
	WAV_printHeader(h);
}

