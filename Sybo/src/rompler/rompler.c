/*
 * rompler.c
 *
 * We read the wav header into the first stored sector to keep our
 * sectors aligned with the sd card for faster access.
 *
 *  Created on: 25 Apr 2013
 *      Author: st
 */

#include "rompler.h"
#include "wav.h"
#include <math.h>
#include <convert.h>
#include <string.h>
#include <diskio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*------------------------------------------------------------/
/ Check a file has contiguous sectors
/------------------------------------------------------------*/

/* Declarations of FatFs internal functions accessible from applications.
/  This is intended to be used by disk checking/fixing or dirty hacks :-) */
DWORD clust2sect (FATFS *fs, DWORD clst);
DWORD get_fat (FATFS *fs, DWORD clst);

static DWORD check_contiguous(    	/* Returns file start sector number */
    FIL* fp    						/* Pointer to the open file object */
)
{
    DWORD csz, tcl, ncl, ccl, cl;
#if _FATFS != 82786 /* Check if R0.09b */
#error This function may not be compatible with this revision of FatFs module.
#endif

    if (f_lseek(fp, 0))     		/* Check if the given parameters are valid */
    	return 0;
    csz = SECTOR_BYTES * fp->fs->csize;    /* Cluster size in unit of byte */
    DWORD len = fp->fsize;
    tcl = (len + csz - 1) / csz;    /* Total number of clusters required */
    len = tcl * csz;                /* Round-up file size to the cluster boundary */

    /* Check if the existing cluster chain is contiguous */
    ncl = 0; ccl = fp->sclust;
    do {
    	cl = get_fat(fp->fs, ccl);  /* Get the cluster status */
    	if (cl + 1 < 3) return 0;   /* Hard error? */
    	if (cl != ccl + 1 && cl < fp->fs->n_fatent) break;  /* Not contiguous? */
    	ccl = cl;
    } while (++ncl < tcl);
    if (ncl == tcl)             	/* Is the file contiguous? */
    	return clust2sect(fp->fs, fp->sclust);  /* Return file start sector */
    return 0;
}

void Rompler_clearSample(RomplerSample *s) {
	s->org = 0;
	if (s->file.fs) {
		f_close(&s->file);
	}
	if (s->first) {
		free(s->first);
	}
}

FRESULT Rompler_initSample(RomplerSample *s, char *path, float samplePeriod,
		uint8_t key, uint8_t paramBase) {
	if (!s) return FR_INVALID_PARAMETER;
	if (!path) return FR_INVALID_NAME;
	// if a file is already open, close it
	// this leaves any file currently playing unlocked but as long as it
	// isn't deleted or written while playing there won't be a problem
	Rompler_clearSample(s);
	// open file
    FRESULT res = f_open(&s->file, path, FA_OPEN_EXISTING | FA_READ);
    if (res) return res;
    // check the file sectors are contiguous
    s->org = check_contiguous(&s->file);
    if (!s->org) {
    	f_close(&s->file);
    	return FR_INVALID_OBJECT;
    }
   	s->first = (Sector *)malloc(sizeof(Sector));
    if (!s->first) {
    	s->org = 0;
    	f_close(&s->file);
    	return 22;
    }
    // read first sector including wav header
    res = disk_read(s->file.fs->drv, (BYTE *)s->first, s->org, 1);
    if (res) {
    	s->org = 0;
    	free(s->first);
    	f_close(&s->file);
    	return res;
    }
    WAV_Header *h = (WAV_Header *)s->first;
    if (h->NumOfChan != 1) {
    	s->org = 0;
    	free(s->first);
    	f_close(&s->file);
    	return 21;			// !!
    }
	s->rateConversion = (float)h->SamplesPerSec * samplePeriod;
	s->length = h->Subchunk2Size >> 1u;

	printf("Sample: note %d, '%s', %luHz, \n", key, path, h->SamplesPerSec);
	s->key = key;
	s->paramBase = paramBase;
    return res;
}

// to be called by Audio_Process
void Rompler_initVoice(RomplerVoice *v, RomplerSample *s, float tuningRatio) {
    memset((void *)&v->free[1], 1, NSECTORS-1);
	memcpy((void *)&v->sector[0], (void *)s->first, SECTOR_BYTES);
	v->free[0] = 0;							// mark sector[0] used
	v->key = s->key;
	v->paramBase = s->paramBase;
	v->drv = s->file.fs->drv;				// for disk_read
    v->readSector = s->org + 1;				// next sector to read
	v->length = s->length;					// including wav header
	v->position = sizeof(WAV_Header) >> 1u; // skip wav header
	v->increment = s->rateConversion * tuningRatio;
	v->rIncrement = 1.f / v->increment;		// avoids realtime division
	v->cacheSector = 1;						// next cache sector to fill
	v->sjz = 0;								// for marking mru sectors free
    v->active = 1;							// activate voice
}

// to be called by Audio_Process
void Rompler_processVoice(RomplerVoice *v, int16_t *out, uint32_t nframes) {
	Sector *s = &v->sector[0];
	uint32_t len = v->length;
	float pos = v->position;
	float incr = v->increment;
	float frac;
	DWORD i, j, k, sj, sk, sjz = v->sjz;
	uint32_t nf = nframes;

	// if the sample ends in this block, figure out how many frames to do
	if (ceilf(pos + nframes * incr) > len) {
		nf = (len - 1 - pos) * v->rIncrement;
	}

	for (i = 0; i < nf; i++) {
		k = ceilf(pos);						// index of sample after
		j = k - 1;							// index of sample before
		frac = pos - j;						// fractional position between
		sj = _SECTOR(j);					// sector for sample before
		sk = _SECTOR(k);					// sector for sample after
		out[i] = F32toS16(					// linear interpolation
					S16toF32(s[sj].s16[_OFFSET(j)]) * (1.f - frac) +
					S16toF32(s[sk].s16[_OFFSET(k)]) * frac);
		pos += incr;
		if (sjz != sj) {
			v->free[sjz] = 1;				// free mru sector
			sjz = sj;
		}
	}

	if (i < nframes) {						// if ended in this block
		for (; i < nframes; i++) {
			out[i] = 0;						// silence the rest of this block
		}
		v->active = 0;						// deactivate this voice
		return;
	}

	v->position = pos;						// remember for next block
	v->sjz = sjz;
}

void Rompler_cacheVoice(RomplerVoice *v) {
	if (!v->free[v->cacheSector]) return;	// nothing to do
	unsigned first = v->cacheSector;
	unsigned n = first + 1;
	while (n < NSECTORS && v->free[n]) {
		n++;
	}
	n -= first;
    disk_read(v->drv, (BYTE *)&v->sector[first], v->readSector, n);
    memset((void *)&v->free[first], '\0', n);
    v->readSector += n;
    v->cacheSector = SECTOR_WRAP(first + n);
}
