/*
 * drumkit.h
 *
 * A kit file is a text file which should have a note and a piece sample
 * path on each line. i.e. <note> <path>
 * e.g. 46 0:/samples/drum/machines/TR909/909Kick 01.wav
 * The path may contain spaces.
 *
 * The kit files should be in the KIT_DIR directory.
 * The kit files should have the KIT_EXT extension.
 *
 *  Created on: 28 Apr 2013
 *      Author: st
 */

#ifndef DRUMKIT_H_
#define DRUMKIT_H_

#include "rompler.h"
#include <ff.h>

#define KIT_EXT			".kit"
#define KIT_DIR			"0:/drumkits/"
#define KIT_DEFAULT		KIT_DIR"default"KIT_EXT

FRESULT DrumKit_Load(char *path);
FRESULT DrumKit_LoadDefault(void);
void DrumKit_ParseLine(char *line);

#endif /* DRUMKIT_H_ */
