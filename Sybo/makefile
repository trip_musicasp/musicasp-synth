#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!    
#
##############################################################################################
# 
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#

##############################################################################################
# Start of default section
#

TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp

MCU  = cortex-m4

# List all default C defines here, like -D_DEBUG=1
DDEFS = 

# List all default ASM defines here, like -D_DEBUG=1
DADEFS = 

# List all default directories to look for include files here
DINCDIR = 

# List the default directory to look for the libraries here
DLIBDIR =

# List all default libraries here
DLIBS = 

#
# End of default section
##############################################################################################

##############################################################################################
# Start of user section
#

# 
# Define project name and Ram/Flash mode here
PROJECT        = Synth1
RUN_FROM_FLASH = 1
USE_HARD_FPU   = 1
HEAP_SIZE      = 22000
STACK_SIZE     = 8192

#
# Define linker script file here
#
ifeq ($(RUN_FROM_FLASH), 0)
LDSCRIPT = ./prj/stm32f4xx_ram.ld
FULL_PRJ = $(PROJECT)_ram
else
LDSCRIPT = ./prj/stm32f4xxxg_flash.ld
FULL_PRJ = $(PROJECT)_rom
endif

#
# Define FPU settings here
#
ifeq ($(USE_HARD_FPU), 0)
FPU =
else
FPU = -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__FPU_USED=1
endif


# List all user C define here, like -D_DEBUG=1
UDEFS = -DUSE_STDPERIPH_DRIVER -DARM_MATH_CM4 -DUSE_FULL_ASSERT=1
UDEFS += -DUSE_PERF=1 -DUSE_MIDI=1 -DUSE_AUDIO=1 -DUSE_AUDIO_IN=0 \
		-DUSE_SEQ_CLOCK=1 \
		-DUSE_ANALOG_IN=1 -DUSE_FAT_FS=1 -DUSE_SD=1 -DUSE_USBH_MSC=0

# swap these lines to use serial uart debug		
UDEFS += -DUSE_USB=1 -DUSE_SERIAL=0
#UDEFS += -DUSE_USB=0 -DUSE_SERIAL=1
		

# Define ASM defines here
UADEFS = 

# List C source files here
SRC  = ./src/cmsis/device/system_stm32f4xx.c \
	   $(wildcard ./src/demo/synth1/*.c) \
       $(wildcard ./src/*.c) \
       $(wildcard ./src/ll/*.c) \
       $(wildcard ./src/io/*.c) \
       $(wildcard ./src/stmicro/std/src/*.c) \
       $(wildcard ./src/fat_fs/*.c) \
       $(wildcard ./src/fat_fs/option/*.c) \
       $(wildcard ./src/stmicro/usbotg/src/*.c) \
       $(wildcard ./src/stmicro/usbd/Core/src/*.c) \
       $(wildcard ./src/stmicro/usbd/Class/cdc/src/*.c) \
       $(wildcard ./src/stmicro/usbh/Core/src/*.c) \
       $(wildcard ./src/stmicro/usbh/Class/MSC/src/*.c) \
       $(wildcard ./src/parts/*.c) \
       $(wildcard ./src/parts/osc/*.c) \
       $(wildcard ./src/parts/filt/*.c) \
       $(wildcard ./src/parts/env/*.c) \
       $(wildcard ./src/parts/fx/*.c) \
       $(wildcard ./src/parts/reverb/*.c) \
       $(wildcard ./src/parts/reverb/algorithms/*.c) \
       $(wildcard ./src/rompler/*.c)
       

# List ASM source files here
ASRC = ./src/cmsis/device/startup_stm32f4xx.s \
	   ./src/ll/mutex.s

# List all user directories here
UINCDIR = ./src \
		  ./src/io \
		  ./src/ll \
		  ./src/fat_fs \
		  ./src/demo/synth1 \
          ./src/cmsis/device \
          ./src/cmsis/core \
          ./src/stmicro/std/inc \
          ./src/stmicro/usbotg/inc \
          ./src/stmicro/usbd/Core/inc \
          ./src/stmicro/usbd/Class/cdc/inc \
          ./src/stmicro/usbh/Core/inc \
          ./src/stmicro/usbh/Class/MSC/inc \
          ./src/parts \
          ./src/parts/osc \
          ./src/parts/filt \
          ./src/parts/env \
          ./src/parts/fx \
          ./src/parts/reverb \
          ./src/parts/reverb/algorithms \
          ./src/rompler

# List the user directory to look for the libraries here
ULIBDIR = ./lib

# List all user libraries here
# not sure why they don't work without ./lib prefix (given ULIBDIR above)
ULIBS = ./lib/arm_cortexM4lf_math.lib

# Define optimisation level here
#OPT = -O0
OPT = -Ofast
#OPT = -O2 -falign-functions=16 -fno-inline -fomit-frame-pointer

#
# End of user defines
##############################################################################################


INCDIR  = $(patsubst %,-I%,$(DINCDIR) $(UINCDIR))
LIBDIR  = $(patsubst %,-L%,$(DLIBDIR) $(ULIBDIR))

ifeq ($(RUN_FROM_FLASH), 0)
DEFS    = $(DDEFS) $(UDEFS) -DRUN_FROM_FLASH=0 -DVECT_TAB_SRAM
else
DEFS    = $(DDEFS) $(UDEFS) -DRUN_FROM_FLASH=1
endif

ADEFS   = $(DADEFS) $(UADEFS) -D__HEAP_SIZE=$(HEAP_SIZE) -D__STACK_SIZE=$(STACK_SIZE)
OBJS    = $(ASRC:.s=.o) $(SRC:.c=.o)
LIBS    = $(DLIBS) $(ULIBS)
MCFLAGS = -mthumb -mcpu=$(MCU) $(FPU)

ASFLAGS  = $(MCFLAGS) $(OPT) -g -gdwarf-2 -Wa,-amhls=$(<:.s=.lst) $(ADEFS)

CPFLAGS  = $(MCFLAGS) $(OPT) -gdwarf-2 -Wall -Wstrict-prototypes -Winline -fverbose-asm 
CPFLAGS += -ffunction-sections -fdata-sections -Wa,-ahlms=$(<:.c=.lst) $(DEFS)

LDFLAGS  = $(MCFLAGS) -nostartfiles -T$(LDSCRIPT) -Wl,-Map=$(FULL_PRJ).map,--cref,--gc-sections,--no-warn-mismatch $(LIBDIR)

HEX_FLAGS = -O ihex 
BIN_FLAGS = -j .text -j .ARM.extab -j .ARM.exidx -j .data -O binary
DFU_FLAGS = -v --device 0483:df11 --alt 0 -s 0x08000000:leave -D

# Generate dependency information
DEPDIR = .dep
CPFLAGS += -MD -MP -MF $(DEPDIR)/$(@F).d


#
# makefile rules
#

all: $(OBJS) $(FULL_PRJ).elf $(FULL_PRJ).hex $(FULL_PRJ).bin


%.o : %.c
	$(CC) -c $(CPFLAGS) -I . $(INCDIR) $< -o $@

%.o : %.s
	$(AS) -c $(ASFLAGS) $< -o $@

%elf: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $@
  
%hex: %elf
	$(CP) $< $(HEX_FLAGS) $@
	
%bin: %elf
	$(CP) $< $(BIN_FLAGS) $@

$(OBJS) : | $(DEPDIR)

$(DEPDIR) :
	mkdir $(DEPDIR)

.PHONY : clean
clean:
	-rm -f $(OBJS)
	-rm -f $(FULL_PRJ).elf
	-rm -f $(FULL_PRJ).map
	-rm -f $(FULL_PRJ).hex
	-rm -f $(FULL_PRJ).bin
	-rm -f $(SRC:.c=.c.bak)
	-rm -f $(SRC:.c=.lst)
	-rm -f $(ASRC:.s=.s.bak)
	-rm -f $(ASRC:.s=.lst)
	-rm -fR $(DEPDIR)

.PHONY : flash
flash:
	dfu-util $(DFU_FLAGS) $(FULL_PRJ).bin
	
# 
# Include the dependency files, should be the last of the makefile
#
-include $(wildcard .dep/*)

# *** EOF ***